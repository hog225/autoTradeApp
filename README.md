Python Version: Python3.6 32bit

[Project Setting]
window cmd 에서 기본 환경 세팅
    cd ~\autoTradeApp
	c:\Python36-32\python -m venv atvenv
    run activeVenv.bat
	python -m pip install --upgrade pip

requirements.txt 만들기
    pip freeze > requirements.txt

Package Install
    pip install -r requirements.txt
    TA-Lib
        - https://www.lfd.uci.edu/~gohlke/pythonlibs/#ta-lib
        - package/Window/TA... 32 이용
        - autoTrade/README.txt 참조

[GIT]
Branch 생성 
    1. Gitlab에서 우선 Branch를 생성 후 CheckOut
    2. 현제 폴더를 Branch에 Commit 하려면 우클릭 > TortoiseGit > Switch/Checkout Click

    