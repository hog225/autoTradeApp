# coding=utf-8
import pandas_datareader.data as web
import pandas as pd
import numpy as np
import datetime
import matplotlib.pyplot as plt
import talib as TA
import os
from .toolEX.chartLib import *
import pprint
import requests
from io import BytesIO
from . import toolEX.chartLib as chart
from . import toolEX.log as log


#--- pandas

# 리니어 리그레이션 같은데 나중에 분석해 보길 ..
# https://www.emilkhatib.com/analyzing-trends-in-data-with-pandas/

pdLog = log.LogWrapper('pandasLib')
pdLog.setLogOption([1, 1, 1, 1, 1, 1, 0]) # only exception enabled

def trend(df):
    ser_df = df
    df = df.to_frame()
    df = df.copy().sort_index()
    dates = df.index.to_julian_date().values[:, None]
    x = np.concatenate([np.ones_like(dates), dates], axis=1)
    y = df.values


    result_df = pd.DataFrame(np.linalg.pinv(x.T.dot(x)).dot(x.T).dot(y).T,
                        df.columns, ['Constant', 'Trend'])

    constant = float(result_df['Constant'])
    trend = float(result_df['Trend'])

    print(constant)
    print(trend)
    k = [(trend * x + 1) for x in range(len(df))]
    print(k)
    plt.plot(ser_df.tolist())
    plt.plot(k)

    plt.show()

def linReg2(candle_df, ma_ser):

    p = 10
    #data_to_anal = candle_df.close
    data_to_anal = ma_ser

    if len(data_to_anal) < p:
        return [0]

    selected = data_to_anal[-10:]

    coefficients, residuals, _, _, _ = np.polyfit(list(range(len(selected.index))), selected, 1, full=True)
    #print coefficients
    #print residuals
    #mse = residuals[0] / (len(selected.index))
    #nrmse = np.sqrt(mse) / (selected.max() - selected.min())
    #print('Slope ' + str(coefficients[0]))
    #print('NRMSE: ' + str(nrmse))

    if 1:
        return coefficients
    # 그래프 그릴 때
    else:
        print(coefficients)
        coe_list = [coefficients[0] * x + coefficients[1] for x in range(len(selected))]
        print(coe_list)
        coe_series = pd.Series(index=selected.index, data= coe_list)


        plt.plot(selected.tolist())
        plt.plot(coe_list)

        #plt.plot(selected)
        #plt.plot(coe_series)

        plt.show()


def linReg(in_list):
    xi = np.arange(len(in_list))
    #A = np.vstack([xi, np.ones(len(xi))]).T
    y = np.array(in_list)
    # linearly generated sequence
    #m, c = np.linalg.lstsq(A, y, 3)[0]  # obtaining the parameters
    m = (((xi.mean()*y.mean()) - (xi*y).mean()) /
         ((xi.mean()**2) - (xi**2).mean()))
    c = y.mean()
    #print (m, c)

    return m

def getNonzeroIndexFromList(_list):
    arr = np.array(_list)
    arr_1de = arr.nonzero()
    if len(arr_1de) == 1:
        return arr_1de[0]
    else:
        return 0

def getSlope(x1, x2, y1, y2):
    if x2 - x1 == 0:
        return 0
    m = (y2 - y1) / float(x2 - x1)

    return m

def normalSlop(in_list):
    xi = np.arange(len(in_list))
    y = np.array(in_list)

    slop = (y[-1] - y[0]) / (xi[-1] - xi[0])
    return slop

def linRegshow(in_list, m, c):
    xi = np.arange(len(in_list))
    y = np.array(in_list)

    plt.plot(xi, y, 'o', label='Original data', markersize=10)
    plt.plot(xi, m * xi + c, 'r', label='Fitted line')
    plt.legend()
    plt.show()


def DfToPickle(df, file_name):
    file_name = file_name + '.pickle'
    cur_dir = os.path.dirname(os.path.abspath(__file__))
    pickle_path = os.path.join(cur_dir, 'toolEX', 'pickle_file', file_name)
    pickle_dir = os.path.join(cur_dir, 'toolEX', 'pickle_file')
    if not os.path.isdir(pickle_dir):
        print('Create Pickle Dir %s' % pickle_dir)
        os.mkdir(pickle_dir)

    print('\x1b[1;34mPICKLE_PATH= %s\x1b[1;m' % pickle_path)
    df.to_pickle(pickle_path)



def getCoinChartFromWeb(coin_name, range):
    try:
        start_time = datetime.date.today() - datetime.timedelta(range)
        yahoo_strs = coin_name.upper()
        yahoo_strs = yahoo_strs + "-USD"
        data = web.DataReader(yahoo_strs, "yahoo", start =start_time)
    except:
        return 0

    return data

def changeDFforziplineOrder(df, start=None, end=None):

    df.index = df['Date'].apply(pd.to_datetime) # Date 의 Dtype을 Datetime 형식으로 변경 해야함
    ch_df = df.loc[:, 'Open':]
    ch_df.columns = ['open', 'high', 'low', 'close', 'AAPL', 'volume']
    ch_df = ch_df.tz_localize("UTC")
    print(ch_df[start:end].size)
    return ch_df[start:end]

def read_apple_stock():
    start = datetime.datetime(2010, 1, 1)
    end = datetime.datetime(2016, 3, 19)
    data = web.DataReader("AAPL", "yahoo", start, end)
    print(data.index)
    data.to_csv('apple.csv') # DataFrame 을 CSV 에 씀
    return data

def read_btc_coin():
    data = web.DataReader("BTC-KRW", "yahoo")
    data.to_csv('btc_krw.csv')
    return data

def getFinancialStatments():
    # 네이버에서 재무재표 얻어오기
    # 좀더 보완이 필요 하다
    url_tmpl = 'https://companyinfo.stock.naver.com/v1/company/ajax/cF1001.aspx?cmp_cd=%s&fin_typ=%s&freq_typ=%s&encparam=%s&id=%s'
    encparam = 'U3d2QUtpRzNISDFMVUtSS0ZibUpPZz09'
    id='TDdCYnFkT0'
    url = url_tmpl % ('005930', '0', 'Y', encparam, id)  # 삼성전자, 4(IFRS 연결), Y:년 단위

    #url = 'https://companyinfo.stock.naver.com/v1/company/c1010001.aspx?cmp_cd=005930&fin_typ=4&freq_typ=Y'

    dfs = pd.read_html(url)
    df = dfs[1]

    #df = df.set_index('주요재무정보')
    print(df)

def getStockCodeFromWeb(mode):
    # 참고자료 https://woosa7.github.io/krx_stock_master/
    # http://kind.krx.co.kr/corpgeneral 접속
    # Excel 버튼을 누를시의 동작을 파악하기 위해 우클릭 검사 -> Network -> Name -> Header 에서 Post 및에 From Data의 값을 조회
    # df = pd.read_html('http://kind.krx.co.kr/corpgeneral/corpList.do?method=download&searchType=13', header=0)[0]
    url = 'http://kind.krx.co.kr/corpgeneral/corpList.do'
    if mode == 0: # KOSPI
        data = {
            'method': 'download',
            'orderMode': '1',  # 정렬컬럼
            'orderStat': 'D',  # 정렬 내림차순
            'marketType': 'stockMkt',  # 코스피냐 코스닥이냐
            'searchType': '13',  # 검색유형: 상장법인
            'fiscalYearEnd': 'all',  # 결산월: 전체
            'location': 'all',  # 지역: 전체
        }
        file_str = '%s_Yahoo.csv' % 'KOSPI'
        tag = '.KS'

    elif mode == 1: # KOSDAQ
        data = {
            'method': 'download',
            'orderMode': '1',  # 정렬컬럼
            'orderStat': 'D',  # 정렬 내림차순
            'marketType': 'kosdaqMkt',  # 코스피냐 코스닥이냐
            'searchType': '13',  # 검색유형: 상장법인
            'fiscalYearEnd': 'all',  # 결산월: 전체
            'location': 'all',  # 지역: 전체
        }
        file_str = '%s_Yahoo.csv' % 'KOSDAQ'
        tag = '.KQ'
    else:
        return 0

    r = requests.post(url, data=data)
    f = BytesIO(r.content)
    dfs = pd.read_html(f, header=0, parse_dates=['상장일'])
    df = dfs[0].copy()

    # 숫자를 앞자리가 0인 6자리 문자열로 변환
    df['종목코드'] = df['종목코드'].astype(np.str)
    df['종목코드'] = df['종목코드'].str.zfill(6) + tag

    try:
        path_str = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'toolEX', 'csv_file', file_str)
        df.to_csv(path_str, encoding='utf-8')
    except Exception as e:
        print('path add fail \n', e)

    return df
    #df[u"종목코드"] = df[u"종목코드"].map('{:06d}'.format)

def readKOSPICode_csv():
    file_name = 'KOSPI_Yahoo.csv'
    path_str = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'toolEX', 'csv_file', file_name)
    df = pd.read_csv(path_str, encoding='utf-8')
    return df

def readKOSDAQCode_csv():
    file_name = 'KOSDAQ_Yahoo.csv'
    path_str = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'toolEX', 'csv_file', file_name)
    df = pd.read_csv(path_str, encoding='utf-8')
    return df

def readStockFromWeb(stock, file_name=''):
    try:
        data = web.DataReader(stock, "yahoo")
    except Exception as e:
        print('Stock Data Maby not exsist code %s name %s' % (stock, file_name))
        print(e)
        return


    if file_name == '':
        file_name = stock
    else:
        pos = stock.find('.')
        if pos >= 0:
            tag = stock[pos:]
            file_name = file_name + tag


    file_str = file_name + '.csv'
    path_str = ''


    #data.index = data['Date'].apply(pd.to_datetime)
    # 원래코드 pandas datareader 업그레이드시 Open Column 의 위치가 변경되는 듯 ...
    ch_df = data.loc[:, 'Open':]
    ch_df.columns = ['open', 'high', 'low', 'close', 'adj_close', 'volume']

    #ch_df = data.loc[:, 'High':]
    #ch_df.columns = ['high', 'low', 'open', 'close', 'adj_close', 'volume']
    #ch_df = ch_df.tz_localize("UTC")
    data = ch_df

    #print data.head()

    #pickle_name = stock + '_price'
    #DfToPickle(data, pickle_name)

    try:
        path_str = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'toolEX', 'csv_file', 'daily', file_str)
        data.to_csv(path_str)
    except:
        print('path add fail ')

    print(path_str)

    return data

def draw_gragh(pandas_data):
    # GUI 환경에서만
    plt.plot(pandas_data.index, pandas_data["Adj Close"])  # 그래프 생성
    plt.show()  # 그래프 출력

def draw_gragh_from_csv(file_name):
    # GUI 환경에서만
    pandas_data = pd.read_csv(file_name)
    plt.plot(pandas_data.index, pandas_data["Adj Close"])  # 그래프 생성
    plt.show()  # 그래프 출력

def pd_objectCreation():
    s = pd.Series([1, 3, 4,np.nan, 6, 8])
    print(s)

    dates = pd.date_range('20130101', periods= 6)
    print(dates)

    df = pd.DataFrame(np.random.rand(6,4), index= dates, columns=list('ABCD'))
    print(df)
    pass

def makeTestFrame():
    dates = pd.date_range('20160101', periods=6)
    df = pd.DataFrame(np.random.rand(6,4), index=dates, columns=list('ABCD'))
    return df

def makeTestFrame2():
    df2 = pd.DataFrame({'A': 1.,
                        'B': pd.Timestamp('20130102'),
                        'C': pd.Series(1, index=list(range(4)), dtype='float32'),
                        'D': np.array([3] * 4, dtype='int32'),
                        'E': pd.Categorical(["test", "train", "test", "train"]),
                        'F': 'foo'})
    return df2


def makeTS(date_str):
    """creates a Pandas DT object from a string"""
    return pd.Timestamp(date_str, tz='utc')

def getCloseDF(df, start=None, end=None):

    df.index = df['Date'].apply(pd.to_datetime) # Date 의 Dtype을 Datetime 형식으로 변경 해야함
    ch_df = df[['Adj Close']]
    ch_df.columns = ["price"]
    ch_df = ch_df.tz_localize("UTC")
    #print ch_df[start:end].size
    return ch_df[start:end]

def getDateIndexDF(df, start=None, end=None):

    df.index = df['Date'].apply(pd.to_datetime) # Date 의 Dtype을 Datetime 형식으로 변경 해야함
    new_col = df.columns.drop('Date')
    ch_df = df[new_col]
    ch_df = ch_df.tz_localize("UTC")
    #print ch_df[start:end].size
    return ch_df[start:end]

def add_df():
    df2 = pd.DataFrame(20.3434, index=[pd.Timestamp('20100123')], columns=['price'])

def order_buy(index, price, pro_df):
    cur_stock_price = price
    prev_balance = pro_df['balance'][-1]
    prev_stock_amt = pro_df['amt'][-1]

    can_buy_stock_amt = prev_balance // cur_stock_price
    cur_stock_amt = int(can_buy_stock_amt) + prev_stock_amt
    cur_balance = prev_balance - can_buy_stock_amt * cur_stock_price
    cur_trade = 'buy'
    add_df = pd.DataFrame([[cur_balance, cur_trade, cur_stock_price, cur_stock_amt]]
                          , index=[index]
                          , columns=['balance', 'trade', 'price', 'amt'])
    pro_df = pro_df.append(add_df)
    return pro_df

def order_sell(index, price, pro_df):
    cur_stock_price = price
    prev_balance = pro_df['balance'][-1]
    prev_stock_amt = pro_df['amt'][-1]

    can_sell_stock_amt = prev_stock_amt
    cur_stock_amt = prev_stock_amt - can_sell_stock_amt
    cur_balance = prev_balance + can_sell_stock_amt * cur_stock_price
    cur_trade = 'sell'
    add_df = pd.DataFrame([[cur_balance, cur_trade, cur_stock_price, cur_stock_amt]]
                          , index=[index]
                          , columns=['balance', 'trade', 'price', 'amt'])
    pro_df = pro_df.append(add_df)
    return pro_df

def makeOHLCDataFrame(df_price, df_open_price, df_high_price, df_low_price, len=None):

    OHLC_dat = pd.DataFrame(index=df_price.index[len:])
    OHLC_dat['close'] = df_price
    OHLC_dat['open'] = df_open_price
    OHLC_dat['high'] = df_high_price
    OHLC_dat['low'] = df_low_price
    return OHLC_dat

def getRaisePercentage(close_befor, close_now):
    return ((close_now - close_befor) / close_befor) * 100

def checkGapAndPercentage(candle_df):
    close_befor = candle_df.close[-2]
    today_open = candle_df.open[-1]

    return ((today_open - close_befor) / close_befor) * 100

def checkPriceBand(ref_price, comp_price, pct):

    high_p = ref_price + (ref_price * pct) / 100
    low_p = ref_price - (ref_price * pct) / 100

    if (low_p <= comp_price <= high_p):
        return True
    else:
        return False


# 수정 필요
def getSerPatternUpperShadow(open, high, low, close):
    # if open > close:
    #     return (high - open) / (open * 1.0)
    # else: # open <= close
    #     return (high - close) / (close * 1.0)


    upperShadow = np.where(open > close, (high - open) / (open * 1.0), (high - close) / (close * 1.0))
    serUpperShadow = pd.Series(upperShadow, index=open.index)

    return serUpperShadow

def getPatternUpperShadow(open, high, low, close):
    if open > close:
        return (high - open) / (open * 1.0)
    else: # open <= close
        return (high - close) / (close * 1.0)


def getPatternBody(open, high, low, close):
    # - 음봉 + 양봉
    return (close - open) / (open * 1.0)

def getPatternLowShadow(open, high, low, close):
    if open > close:
        return (close - low) / (low * 1.0)
    else: # open <= close
        return (close - high) / (high * 1.0)


def customEveningStar(open, high, low, close):
    if len(open) < 3:
        return False

    if getRaisePercentage(open[-3], close[-3]) > 1.7:
        if getRaisePercentage(close[-3], open[-2]) > 0.3:
            result = TA.CDLLONGLINE(open, high, low, close)
            if result[-1] < 0:
                pdLog.info('eveningData', getRaisePercentage(open[-3], close[-3]), getRaisePercentage(close[-3], open[-2]))
                return True

    return False

def customHangingMan(open, high, low, close):

    if getPatternUpperShadow(open, high, low, close) == 0:

        body = close - open
        whole = high - low

        if abs(body/(whole*1.0)) < 0.3:
            return True

    return False

def getWholePeriodCustomPattern(open, high, low, close, func):

    result = []
    for index in range(len(open)):
        result.append(func(open[index], high[index], low[index], close[index]))
        #df = pd.DataFrame(data = [[open, high, low, close]], index= open.index)
        #print checkGapAndPercentage(df)

    serResult = pd.Series(result, index = open.index)
    return serResult


HAMMER = 'hammer'
INHAMMER = 'inverse_hammer'
TWOCROWS = 'twocrows'
CDL3INSIDE = 'threeInsideUpDown'
CDLDOJI = 'doji'
CDLLONGLINE ='jangdae_bong'
CDLLONGLEGGEDDOJI = 'longlegdogi'
CDLDRAGONFLYDOJI = 'dragonflydoji' # 상승 신호라고 함
CDL2CROWS = 'two_crows'
CDLHARAMI = 'CDLHARAMI'
CDLHARAMICROSS = 'CDLHARAMICROSS'
CDLKICKING = 'CDLKICKING'
CDLKICKINGBYLENGTH = 'CDLKICKINGBYLENGTH'
CDLGRAVESTONEDOJI = 'CDLGRAVESTONEDOJI'
CDLSHORTLINE = 'CDLSHORTLINE'
CDL3WHITESOLDIERS = 'advandingWhiteSoldiers' # 상승 적삼형 잘안나오는 패턴
CDLKICKINGBYLENGTH = 'kicking' # 잘 안나오는 패턴
Bullish_Engulfing = 'Bullish_Engulfing' # 상승 장악형
Bearish_Engulfing = 'Bearish_Engulfing' # 상승 장악형
CDLMORNINGSTAR = 'CDLMORNINGSTAR' # 샛별형
CDLDRAGONFLYDOJI = 'CDLDRAGONFLYDOJI' # 잠자리 도지
CDLHANGINGMAN = 'CDLHANGINGMAN' # 교수 형 하락 패턴
CDLDOJISTAR = 'CDLDOJISTAR' # 십자형
CDLEVENINGSTAR = 'CDLEVENINGSTAR' # 이브닝 스타
CDLBELTHOLD = 'CDLBELTHOLD'
SIMPLE_TEST = 'simple_test'
CDLTASUKIGAP = 'CDLTASUKIGAP'
DARKCLOUD = 'DarkCloudCover'
CDLEVENINGDOJISTAR = 'eveningDojiStar'
C_CDLEVENINGSTAR = 'Custom' + CDLEVENINGSTAR

custom = 'customCandle'

PATTERN_TO_USE = custom
CUSTOM_FUNC = customHangingMan

def getPatternRecogDate(df_candle, pattern, pluse_candel = 1, str_sdate = 0, str_enddate = None):
    # 패턴별로 해당 패턴이 나오고 나서 주가가 하락하는지 상승하는지 여부를 확인하는 모듈이 필요 할 듯
    # 기냥 다음날 종가(시가)와 특정 기간동안의 최저가 최고가 종가를 출력하기

    if str_sdate == 0 or str_enddate == None:
        n_df_candle = df_candle
    else:
        n_df_candle = df_candle.loc[str_sdate:str_enddate]


    if pattern == HAMMER:
        s_result = TA.CDLHAMMER(n_df_candle.open, n_df_candle.high, n_df_candle.low, n_df_candle.close)
    elif pattern == TWOCROWS:
        s_result = TA.CDL3BLACKCROWS(n_df_candle.open, n_df_candle.high, n_df_candle.low, n_df_candle.close)
    elif pattern == CDL3INSIDE:
        s_result = TA.CDL3INSIDE(n_df_candle.open, n_df_candle.high, n_df_candle.low, n_df_candle.close)
    elif pattern == CDLDOJI:
        s_result = TA.CDLDOJI(n_df_candle.open, n_df_candle.high, n_df_candle.low, n_df_candle.close)
    elif pattern == CDLLONGLINE:
        s_result = TA.CDLLONGLINE(n_df_candle.open, n_df_candle.high, n_df_candle.low, n_df_candle.close)
    elif pattern == CDLLONGLEGGEDDOJI:
        s_result = TA.CDLLONGLEGGEDDOJI(n_df_candle.open, n_df_candle.high, n_df_candle.low, n_df_candle.close)
    elif pattern == CDLDRAGONFLYDOJI:
        s_result = TA.CDLDRAGONFLYDOJI(n_df_candle.open, n_df_candle.high, n_df_candle.low, n_df_candle.close)
    elif pattern == CDL2CROWS:
        s_result = TA.CDL2CROWS(n_df_candle.open, n_df_candle.high, n_df_candle.low, n_df_candle.close)
    elif pattern == CDLHARAMI:
        s_result = TA.CDLHARAMI(n_df_candle.open, n_df_candle.high, n_df_candle.low, n_df_candle.close)

    elif pattern == CDLHARAMICROSS:
        s_result = TA.CDLHARAMICROSS(n_df_candle.open, n_df_candle.high, n_df_candle.low, n_df_candle.close)
    elif pattern == CDLKICKINGBYLENGTH:
        s_result = TA.CDLKICKINGBYLENGTH(n_df_candle.open, n_df_candle.high, n_df_candle.low, n_df_candle.close)
    elif pattern == CDLGRAVESTONEDOJI:
        s_result = TA.CDLGRAVESTONEDOJI(n_df_candle.open, n_df_candle.high, n_df_candle.low, n_df_candle.close)
    elif pattern == CDLSHORTLINE:
        s_result = TA.CDLSHORTLINE(n_df_candle.open, n_df_candle.high, n_df_candle.low, n_df_candle.close)
    elif pattern == INHAMMER:
        s_result = TA.CDLINVERTEDHAMMER(n_df_candle.open, n_df_candle.high, n_df_candle.low, n_df_candle.close)
    elif pattern == CDL3WHITESOLDIERS:
        s_result = TA.CDL3WHITESOLDIERS(n_df_candle.open, n_df_candle.high, n_df_candle.low, n_df_candle.close)
    elif pattern == CDLKICKINGBYLENGTH:
        s_result = TA.CDL3WHITESOLDIERS(n_df_candle.open, n_df_candle.high, n_df_candle.low, n_df_candle.close)

    elif pattern == Bullish_Engulfing:
        s_result = TA.CDLENGULFING(n_df_candle.open, n_df_candle.high, n_df_candle.low, n_df_candle.close)
    elif pattern == CDLMORNINGSTAR:
        s_result = TA.CDLMORNINGSTAR(n_df_candle.open, n_df_candle.high, n_df_candle.low, n_df_candle.close)
    elif pattern == CDLDRAGONFLYDOJI:
        s_result = TA.CDLDRAGONFLYDOJI(n_df_candle.open, n_df_candle.high, n_df_candle.low, n_df_candle.close)
    elif pattern == CDLHANGINGMAN:
        s_result = TA.CDLHANGINGMAN(n_df_candle.open, n_df_candle.high, n_df_candle.low, n_df_candle.close)

    elif pattern == CDLDOJISTAR:
        s_result = TA.CDLDOJISTAR(n_df_candle.open, n_df_candle.high, n_df_candle.low, n_df_candle.close)
    elif pattern == CDLEVENINGSTAR:
        # 잘 안맞는듯 횡보 구간에서만 나왔음
        s_result = TA.CDLEVENINGSTAR(n_df_candle.open, n_df_candle.high, n_df_candle.low, n_df_candle.close)

    elif pattern == SIMPLE_TEST:
        s_result = TA.CDLLONGLEGGEDDOJI(n_df_candle.open, n_df_candle.high, n_df_candle.low, n_df_candle.close)
    elif pattern == CDLTASUKIGAP:
        s_result = TA.CDLTASUKIGAP(n_df_candle.open, n_df_candle.high, n_df_candle.low, n_df_candle.close)
    elif pattern == custom:
        s_result = getWholePeriodCustomPattern(n_df_candle.open, n_df_candle.high, n_df_candle.low, n_df_candle.close, CUSTOM_FUNC)

    else:
        return []



    k = 0
    list_index = []
    list_val = []
    for result in s_result:
        if result < 0:
            list_index.append(s_result.index[k])
            list_val.append(s_result[k])
        elif result > 0:
            list_index.append(s_result.index[k])
            list_val.append(s_result[k])

        k = k+1
    result_df = pd.DataFrame(index=list(range(len(list_index))))
    result_df['date'] = list_index
    result_df['value'] = list_val



    return result_df, s_result


def DI_Strategy(df_candle, str_sdate = 0, str_enddate = None,  show_plt=1):
    if str_sdate == 0 or str_enddate == None:
        n_df_candle = df_candle
    else:
        n_df_candle = df_candle.loc[str_sdate:str_enddate]

    mdi_result = TA.MINUS_DI(n_df_candle.high, n_df_candle.low, n_df_candle.close, timeperiod=14)
    pdi_result = TA.PLUS_DI(n_df_candle.high, n_df_candle.low, n_df_candle.close, timeperiod=14)
    adx_result = TA.ADX(n_df_candle.high, n_df_candle.low, n_df_candle.close, timeperiod=5)
    adxr_result = TA.ADXR(n_df_candle.high, n_df_candle.low, n_df_candle.close, timeperiod=5)
    apo_result = TA.APO(n_df_candle.close, fastperiod=12, slowperiod=26, matype=0)
    rsi_result = TA.RSI(n_df_candle.close, timeperiod=14)

    ma_5_vol = TA.MA(n_df_candle.volume, timeperiod=5)

    rsi_result = rsi_result.rolling(window=3).mean()
    n_df_candle['MDI'] = mdi_result
    n_df_candle['PDI'] = pdi_result
    n_df_candle['ADX'] = adx_result

    sep_tech_list = [rsi_result]
    if show_plt == 1:
        plot_candles(n_df_candle, volume_bars=True, volume_tech=[ma_5_vol], sep_technicals=sep_tech_list)

    return n_df_candle



def checkYangBong(ohlc_dat):
    bong_df = pd.Series(index = ohlc_dat.index, data = np.where(ohlc_dat['open'] <= ohlc_dat['close'], 1, 0))
    return bong_df

def checkOneYangbong(open, close):
    if close > open:
        return True
    return False

def shortBodyCandle(ohlc_dat):

    short_val = 0.005
    percentage = ((ohlc_dat['close'] - ohlc_dat['open'])/ohlc_dat['open'])
    yang_bong_ser = pd.Series(index=ohlc_dat.index, data=np.where((0.0 <= percentage) & (percentage<= short_val), 100, 0))
    um_bong_ser = pd.Series(index=ohlc_dat.index, data=np.where((-short_val <= percentage) & (percentage < 0.0), -100, 0))

    return yang_bong_ser + um_bong_ser

YANGBONG = 0
SHORTCANDLE =1
def getPatternRecogCustom(df_candle, pattern, str_sdate = 0, str_enddate = None):
    if str_sdate == 0 or str_enddate == None:
        n_df_candle = df_candle
    else:
        n_df_candle = df_candle.loc[str_sdate:str_enddate]



    if pattern == YANGBONG:
        s_result = checkYangBong(n_df_candle)
    elif pattern == SHORTCANDLE:
        s_result = shortBodyCandle(n_df_candle)
    else:
        return []


    k = 0
    list_index = []
    list_val = []
    for result in s_result:
        if result < 0:
            list_index.append(s_result.index[k])
            list_val.append(s_result[k])
        elif result > 0:
            list_index.append(s_result.index[k])
            list_val.append(s_result[k])

        k = k+1

    result_df = pd.DataFrame(index=list(range(len(list_index))))
    result_df['date'] = list_index
    result_df['value'] = list_val
    return result_df

def csvToDataFrame(file_name, path_from=0):

    #if path_from == 0:
    path = os.path.join(os.path.dirname(__file__), 'toolEX', 'csv_file', 'daily', file_name)

    df = pd.read_csv(path)

    df.index = df['Date'].apply(pd.to_datetime) # Date 의 Dtype을 Datetime 형식으로 변경 해야함
    ch_df = df.loc[:, 'open':]
    ch_df.columns = ['open', 'high', 'low', 'close', 'adj_close', 'volume']
    ch_df = ch_df.tz_localize("UTC")

    return ch_df



if __name__=="__main__":
    # 웹에서 Stock 데이터 얻어오기 번들데이터 만들려면 makeCustomBundle.py 파일 돌려
    # readStockFromWeb("005385.KS")
    # readStockFromWeb('TSLA')
    # readStockFromWeb('AMD')


    TEST_CODE = 2

    if TEST_CODE == 1:
        # 지표 분석 Test ----------------------------------------------------
        test_df = csvToDataFrame('GOOG.csv')
        anal_df = test_df['20160111':'20170120']
        #DI_Strategy(test_df, str_sdate= '20150111', str_enddate= '20170920')

        ser = getWholePeriodCustomPattern(anal_df.open, anal_df.high, anal_df.low, anal_df.close, customHangingMan)
        print(ser)

        exit()
        # -----------------------------지표 분석 Test END -----------------------
    elif TEST_CODE == 2:

        # 봉 차트 분석 Test ----------------------------------------------------

        test_df = csvToDataFrame('신세계.KS.csv')
        anal_df = test_df['20160111':'20170120']
        #print getPatternUpperShadow(anal_df.open, anal_df.high, anal_df.low, anal_df.close)


        #result_df = getPatternRecogDate(test_df, PATTERN_TO_USE, str_sdate= '20150111', str_enddate= '20170920')
        result_df, originResult = getPatternRecogDate(anal_df, PATTERN_TO_USE)

        for i in range(len(result_df)):
            print(result_df.date[i], result_df.value[i])


        ma_ser = TA.MA(anal_df['close'], timeperiod=5)
        ma_list = [ma_ser]


        pluse = np.where(np.array(originResult.tolist()) > 0 , 1, 0)
        minus = np.where(np.array(originResult.tolist()) < 0, 1, 0)



        marker_list = [pluse, minus]


        plot_candles(anal_df, technicals=ma_list, marker=marker_list)
        exit()


        # -----------------------------봉 차트 분석 Test END -----------------------

    elif TEST_CODE == 3:

        linReg([1, 9, -2])
        exit()

    elif TEST_CODE == 4:
        # 아래 내용 시험 하기
        stock = csvToDataFrame('AAPL.csv')
        price = stock['close'][70:75]
        print(type(price))
        linReg2(price)
        exit()

    elif TEST_CODE == 5:
        path_str = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'toolEX', 'pickle_file', 'GOOG_stoc.pickle')
        stock = pd.read_pickle(path_str)
        stock.index = stock['Date'].apply(pd.to_datetime)
        stock = stock.tz_localize("UTC")

        price = stock.stock_price[200:205]

        linReg2(price)
        exit()
    elif TEST_CODE == 6:
        start = datetime.datetime(2019, 1, 1)
        end = datetime.datetime(2018, 9, 21)
        data = web.DataReader('AAPL', "google", start, end)
        #print data

    elif TEST_CODE == 7:
        #getFinancialStatments()
        pass

    elif TEST_CODE == 8:


        test_df = csvToDataFrame('GOOG.csv')
        # test_df = csvToDataFrame(u'삼성전자.KS.csv', path_from=1)
        # anal_df = test_df['20160111':'20160920']
        anal_df = test_df['20160111':'20170120']

        ma_ser = TA.MA(anal_df['close'], timeperiod=5)
        ma_25ser = TA.MA(anal_df['close'], timeperiod=7)
        ma_5_vol = TA.MA(anal_df['volume'], timeperiod=5)
        marker_list = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 743.50201400000003, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 701.79399420000004, 0, 0, 0, 0, 0, 0, 0, 711.74799839999991, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 739.25600599999984, 0, 0, 0, 0, 0, 0, 747.09000239999989, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 757.08199459999958, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 715.1020019999994, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 731.40999739999938, 0, 0, 0, 0, 0, 0, 0, 722.5800047999993, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 783.75399199999947, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 775.19801019999943, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 782.57801519999953, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 781.68400859999963, 0, 0, 0, 0, 0, 0, 0, 0, 803.23798819999979, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 776.58400919999963, 0, 0, 0, 0, 0, 0, 0, 766.7439939999997, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 795.26799319999952, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 806.71799299999975, 0, 0, 0]]

        chart.plot_candles(anal_df, marker=marker_list, technicals=[ma_ser])



