# -*- coding: utf-8 -*-
from .make_candle_timer import *
from .getcoinchart import *
from autoTradeApp.mqtt_task import *
from autoTradeApp.db_trade_lib import *
from gevent.pool import Pool
import gevent.monkey
gevent.monkey.patch_all()


if __name__ == "__main__":
    #Currency_monitor DB 에 있는 거 선택하여 모니터링

    print('\x1b[1;35m------Get Coin Chart Task Start------\x1b[1;m')

    create_init_db()

    while True:
        try:
            gevent.joinall([
                gevent.spawn(task_control_chart),
                gevent.spawn(task_writeCandle),
                gevent.spawn(mqtt_pub_thread),
                gevent.spawn(mqtt_subs_thread, MSG_URL_CHART),
                gevent.spawn(init_process)
            ])

        except (KeyboardInterrupt, SystemExit):
            print('Service End')
            exit()

'''
#monkey patching test
    def test_response(n):
        coin = ['btc','iota', 'xrb', 'etc','eth']
        payload = {'currency':'%s'%coin[n]}
        j_dat = get_response_val_by_key(ORDER_BOOK,'GET',params=payload)
        print "가장 높은 판매가격 " + str(j_dat["ask"][-1])

    pool = Pool(5)
    pool.map(test_response,xrange(5))
'''