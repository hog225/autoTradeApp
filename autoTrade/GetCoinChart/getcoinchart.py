# -*- coding: utf-8 -*-
from autoTradeApp.api_func import *
from autoTradeApp.db_chart_lib import *
from autoTradeApp.mqtt_task import *
import gevent

def get_coinchart_api(currency, _chart_api_q):
    print('\x1b[1;34m%s GET COIN CHART START\x1b[1;m' % currency)
    TEST_ORDER_ID = 0
    start_time = 0
    while True:
        gevent.sleep(0)
        try:
            data = _chart_api_q.get_nowait()
            option, para1, para2, data_list = data
            m1_trigger, timer_age = data_list
            #print '\x1b[1;34mget Coin chart %s, %d, %d, %s \x1b[1;m' % (str(option), para1, para2, str(data_list))
            duration = time.time() - start_time
            start_time = time.time()
            print('\x1b[1;34m     %s API Request %0.5f option %d \x1b[1;m' % (currency, duration, option))

            if option == GET_ORDER_BOOK:
                currency_params = {'currency': '%s' % currency}
                json_dat = get_response_val_by_key(ORDER_BOOK, 'GET', params=currency_params)
                insertOrderBook(json_dat)

            elif option == GET_TICKER:
                currency_params = {'currency': '%s' % currency}
                json_dat = get_response_val_by_key(TICKER, 'GET', params=currency_params)
                insertTicker(json_dat)
                mqtt_q.put_nowait(get_msg_for_q(MQ_CHART_NOTI_TICK, para2= MSG_URL_STRATEGY + '/' + MSG_URL_CHART, data_list=json_dat))
                if m1_trigger == True:
                    chart_candle_q.put_nowait(get_msg_for_q(M_1_CANDLE, para1=currency, data_list=[timer_age]))


                #if json_dat != {}:
                #    stra_q.put_nowait(get_msg_for_q(option,0,0,json_dat))

        except NQueue.Empty:
            pass



if __name__ == "__main__":
    #json_dat = get_response_val_by_key(TICKER, 'GET', params=currency_params)
    #insertTicker(json_dat)
    delete_coin_chart('btc')
    delete_coin_chart('iota')



