#!/bin/bash

PROCESS='python chart_main.py'
PIDSTR=`ps -ef | grep "$PROCESS" | grep -v grep|awk '{printf $2 " "}'`;
PID=`echo $PIDSTR | cut -d" " -f1`

echo $PIDSTR

if [ "$PID" == "" ];then
	echo No live $PROCESS
else
	while [ "$PIDSTR" != "" ];
	do
		echo kill $PROCESS
		kill -9 $PIDSTR
		sleep 1;
		PIDSTR=`ps -ef | grep "$PROCESS" | grep -v grep|awk '{printf $2 " "}'`;
	done
fi
