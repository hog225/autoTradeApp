# -*- coding: utf-8 -*-
from autoTradeApp.db_chart_lib import *
from autoTradeApp.timer_task import *
from .getcoinchart import *
from gevent import Greenlet
from multiprocessing import Process, Queue, Pool

CHART_TASK_DAT = []
'''
CHART_TASK_DAT[
    {
        'currency':'currency'
        'chart_q':q,
        'chart_tmr':tmr,
        'chart_thread':thread
    }
]
'''

def get_candle_name(currency):
    new_candle_list = []
    for i in CANDLE_LIST:
        currency_prefix = '%s_'%currency
        new_candle_name = currency_prefix+i
        new_candle_list.append(new_candle_name)
    return new_candle_list

def collecting_chart_start(timeout, currency):
    #check there are data in CHART_TASK_DAT
    for chart_dat in CHART_TASK_DAT:
        if chart_dat['currency'] == currency:
            return False

    chart_task_dat = {}
    chart_task_dat['currency'] = currency

    _chart_api_q = NQueue.Queue()
    chart_task_dat['chart_q'] = _chart_api_q

    tmr = eventTimer(timeout, api_call_timer_per_10_sec, currency,_chart_api_q)
    tmr_name = currency+'_timer'
    tmr.setName(tmr_name)
    tmr.start()


    chart_task_dat['chart_tmr'] = tmr

    chart_thread = Greenlet.spawn(get_coinchart_api, currency, _chart_api_q)
    chart_thread.start()
    chart_task_dat['chart_thread'] = chart_thread
    CHART_TASK_DAT.append(chart_task_dat)
    return True

def collecting_chart_stop( currency):
    #check there are data in CHART_TASK_DAT
    i=0
    for chart_dat in CHART_TASK_DAT:
        if chart_dat['currency'] == currency:
            chart_dat['chart_tmr'].kill()
            chart_dat['chart_thread'].kill()
            CHART_TASK_DAT.pop(i)
            return True
        i += 1
    return False

def collecting_chart_print():
    chart_dat_dic = {
        'currency':[],
        'tmr':{
            'tmr_name':[],
            'tmr_enable':[]
        },
        'thread':[]
    }
    for chart_dat in CHART_TASK_DAT:
        chart_dat_dic['currency'].append(chart_dat['currency'])
        chart_dat_dic['tmr']['tmr_enable'].append(chart_dat['chart_tmr'].getName())
        chart_dat_dic['tmr']['tmr_name'].append(chart_dat['chart_tmr'].isAlive())
        chart_dat_dic['thread'].append(chart_dat['chart_thread'].started)
    return chart_dat_dic


def task_writeCandle():
    print(C_BOLD + '**WRITE CANDLE START**' + C_END)
    TIMER_COUNT_1M = 0

    while True:
        gevent.sleep(0)
        try:
            option, para1, para2, data_list = chart_candle_q.get_nowait()
            currency = para1
            timer_age_1min = data_list[0]
            #print '\x1b[1;35mtask_writeCandle %s, %d, %d, %s \x1b[1;m' % (str(option), para1, para2, str(data_list))

            if option == M_1_CANDLE:
                m1, m5, m30, h1, h6, h12, d1 = get_candle_name(currency)
                print('\x1b[1;35m       %s M_1_CANDLE WRITE \x1b[1;m' % (currency))

                candle_write(currency, M_1_CANDLE*60/10)

                if timer_age_1min % 5 == 0:
                    candle_write_using_candle_db(currency, m1, m5, 5, 60)

                if timer_age_1min % 30 == 0:
                    candle_write_using_candle_db(currency, m5, m30, 6, 950)
                if timer_age_1min % 60 == 0:
                    candle_write_using_candle_db(currency, m30, h1, 2, 1800)

                if timer_age_1min % 360 == 0:
                    candle_write_using_candle_db(currency, h1, h6, 6, 3600)
                if timer_age_1min % 720 == 0:
                    candle_write_using_candle_db(currency, h6, h12, 2, 21600)
                if timer_age_1min % 1440 == 0:
                    candle_write_using_candle_db(currency, h12, d1, 2, 43200)

        except NQueue.Empty:
            pass




def task_control_chart():
    print(C_BOLD + '**CONTROL CHART START**' + C_END)
    tof = ['Fail', 'Success']
    frame_info = getframeinfo(currentframe())
    while True:
        gevent.sleep(0)
        try:
            option, para1, para2, data_list = chart_control_q.get_nowait()
            str_dat = '%s : option : %s, para1 : %s, para2 : %s, info : %s' \
                % (frame_info.function, str(option), str(para1), str(para2), str(data_list))
            clog(str_dat, CONCh)

            opt = getOptionFromMQ(data_list)
            cur_list = getCurrencyFromOption(opt)
            currency = cur_list[0]

            if currency == '' and len(currency) > 8:
                p_str = 'invalid Coin'
                clog(p_str, CONCh)


            if option == MQ_CHART_DAT_COLLECT_START:
                # TODO Currency_monitor DB 에 넣기

                res = collecting_chart_start(10, currency)
                p_str = 'Collecting %s Coin Start %s ' % (currency, tof[res])
                if res == True:
                    create_coin_chart(currency)
                    insertChartMonList(currency)
                clog(p_str, CONCh)


            elif option == MQ_CHART_DAT_COLLECT_STOP:
                #Currency_monitor DB에서 빼기
                res = collecting_chart_stop(currency)
                p_str = 'Collecting %s Coin stop %s ' % (currency, tof[res])
                clog(p_str, CONCh)

            elif option == MQ_CHART_DAT_DELETE:
                res = collecting_chart_stop(currency)
                p_str = 'Collecting %s Coin delete %s ' % (currency, tof[res])
                delete_coin_chart(currency)
                clog(p_str, CONCh)


            elif option == MQ_CHART_GETTESTDB:
                pass

            elif option == MQ_CHART_GETTASK:
                #Task Info 보여주기
                task_chart_list = collecting_chart_print()
                dic_dump(task_chart_list)
                #clog(str(task_chart_list), CONCh)
                #mqtt_q.put_nowait(get_msg_for_q(MQ_CHART_GETTASK,0,0,str(task_chart_list)))
                pass

        except NQueue.Empty:
            pass


def init_process():
    gevent.sleep(1)
    db_list = db_select_all(CURRENCY_MON_LIST)

    print(C_BOLD + '**INIT_PROCESS**' + C_END)
    for db in db_list:
        mon_start_currency = str(db[2])
        print('Mon Start ' + mon_start_currency)
        dat_list = '-c' + ' ' + mon_start_currency
        chart_control_q.put(get_msg_for_q(MQ_CHART_DAT_COLLECT_START, data_list=dat_list))


def api_call_timer_per_10_sec(currency, _chart_api_q, timer_age, m1_triger):
    _chart_api_q.put_nowait(get_msg_for_q(GET_TICKER, para1=currency, data_list=[m1_triger, timer_age]))
    print('\x1b[1;33m10 Sec Timer\x1b[1;m')
    #if m1_triger == True:
    #    chart_candle_q.put_nowait(get_msg_for_q(M_1_CANDLE,para1=currency,data_list = [timer_age]))



if __name__ == "__main__":
    #insertChartMonList('btc')
    init_process()
    '''
    chart_job = []
    coin_list = ['btc','iota','ada','power','super','nem','xrb','xrp','sdf']
    for i in coin_list:
        process_name = 'chart_proc_%s' % i
        p = Process(target=chart_proc, name=process_name,
                    args=(10,i))
        chart_job.append(p)
        p.daemon = True
        p.start()
        print p.name,' ','started '


    while True:
        time.sleep(100)
    '''




