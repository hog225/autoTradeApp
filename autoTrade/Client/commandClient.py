# -*- coding: utf-8 -*-
import paho.mqtt.client as mqtt
import time
import sys
import gevent
import queue
import json
import os
#sys.path.insert(0,'/home/IOT_Server/engine/IOT_engine/client')
#from Event_Timer import *
from multiprocessing import Process
from define import *

#buy testbuy

STIME = 120
TIMER = 0


MQTT_SERVER = "127.0.0.1"
MY_IP = "192.168.0.18"
PROMPT = 'Yeonggi'

MSG_URL_ROOT ='autotrade/'
MSG_ROOT = 'autotrade'
TOPIC_TRADING = MSG_URL_ROOT+ 'trading'
TOPIC_STRATEGY = MSG_URL_ROOT+'strategy'
TOPIC_CHART = MSG_URL_ROOT+'chart'
TOPIC_CLIENT = MSG_URL_ROOT+'client'
my_topic = TOPIC_CLIENT + '/#'
MSG_URL_CLIENT = 'client'

def getstrbykey(key, val):
    return_str = 'ERROR'
    if key == 'way':
        strs = ['BUY', 'SELL', 'CANCEL']
        try:
            return_str = str(key) + ' : ' + strs[int(val)]
        except:
            return_str = str(key) + ' : ' + str(val) + 'ERROR'

    elif key == 'real':
        strs = ['REAL', 'TEST']
        try:
            return_str = str(key) + ' : ' + strs[int(val)]
        except:
            return_str = str(key) + ' : ' + str(val) + 'ERROR'

    elif key == 'err_code':

        try:
            return_str = str(key) + ' : ' + CMN_ERR_CODE[int(val)]
        except:
            return_str = str(key) + ' : ' + str(val) + 'ERROR'

    else:
        return_str = str(key) + ' : ' + str(val)

    return return_str

def prompt():
    sys.stdout.write('[%s] : ' % PROMPT)
    sys.stdout.flush()


def on_connect(client, userdata, flags, rc):
    print(("Connected with result code "+str(rc)))
    client.subscribe(my_topic)

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    para_list = msg.payload.split(',')
    print(('\n[PATH] : %s' % (msg.topic)))
    str_para = ''
    for i in para_list:
        i = i.strip()
        try:
            key, val = i.split('=')
            str_para += getstrbykey(key, val)
            str_para += ' '
        except:
            str_para += 'E_ ' + str(i)
    print(('[PARA] : %s' % str_para))




def mqtt_subs_thread():
    print('mqtt sub thread start ')
    global TIMER
    while True:
        client = mqtt.Client()

        client.on_connect = on_connect
        client.on_message = on_message
        client.connect(MQTT_SERVER, 1883)

        print('start subscribe ')
        client.loop_forever(timeout=60)
        print('looping')



def user_thread():
    APP_ORDER_ID = 0
    while True:
        gevent.sleep(0)
        message = sys.stdin.readline()
        sys.stdin.flush()
        APP_ORDER_ID += 1
        data = message.strip()
        if data == 'help':
            print('-------------------------------------')
            print('매개변수 사용 안할 시 Default 임')
            print('** -o    [app_order_id]  Must need for Cancle')
            print('** -c    [Currency]      Default: btc')
            print('** -p    [price]         Default: last price')
            print('** -qp   [1, 2, 3]       25%, 50%, 75%')
            print('** -q    [qty]           Default: trade all possible amount')
            print('** [...]      -  ')
            print(' ')
            print('trade_buy_test [currency], [option]')
            print('trade_sell_test [currency], [option]')
            print('trade_cancel_test [app_order_id] [currency]')
            print('trade_margine_update [currency]')
            print('trade_margine_update_test [currency]')
            print('trade_show_process')
            print(' ')
            print('chart_collect_start [currency]')
            print('chart_collect_stop [currency]')
            print('chart_gettestdb [currency] ,[option]')
            print('chart_get_testinfo')
            print('chart_delete_db [currency]')
            print('--------------------------------------')
        elif data:

            input_list = data.split()
            key = input_list.pop(0)

            if data.find('cancel') < 0: #Cancel 은 사용자가 Order ID 입력 함으로
                order_id = '-o ' + str(APP_ORDER_ID)
                input_list.append(order_id)
                print("Order ID : " + str(APP_ORDER_ID))
            else:
                print("Cancel Order User App ID will be used ")

            send_str = ' '.join(input_list)



            try:
                if key.find('chart') == 0:
                    send_topic = os.path.join(TOPIC_CHART, MSG_URL_CLIENT, key)
                elif key.find('trade') == 0:
                    send_topic = os.path.join(TOPIC_TRADING, MSG_URL_CLIENT, key)
                elif key.find('strat') == 0:
                    send_topic = os.path.join(TOPIC_STRATEGY, MSG_URL_CLIENT, key)
                else:
                    send_topic = ''

                mqttc = mqtt.Client()
                mqttc.connect(MQTT_SERVER, 1883, 60)
                mqttc.publish(send_topic, send_str)
            except:
                print('invalid topic')
        prompt()

if __name__=="__main__":

    mqtt_sub = Process(target=mqtt_subs_thread, name='mqtt_sub')
    mqtt_sub.daemon = True
    mqtt_sub.start()

    while True:
        try:
            gevent.joinall([
                gevent.spawn(user_thread),
            ])

        except (KeyboardInterrupt,SystemExit):
            print('Service End')
            exit()
