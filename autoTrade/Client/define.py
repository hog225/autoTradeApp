# -*- coding: utf-8 -*-
CMN_SUCCESS = 0
CMN_ERR_INVALID_COIN_PRICE = 1
CMN_ERR_NOT_ENOUGH_MONEY = 2
CMN_ERR_INVALID_FORMAT = 3
CMN_ERR_INVALID_COIN = 4
CMN_ERR_REQ_FAIL = 5
CMN_ERR_DB_SELECT_ERROR = 6
CMN_ERR_DB_UPDATE_ERROR = 7
CMN_ERR_DB_INSERT_ERROR = 8
CMN_ERR_DB_CREATE_ERROR = 9
CMN_ERR_DB_DELETE_ERROR = 10
CMN_ERR_NO_APP_ID = 11
CMN_ERR_INVALID_ORDER_WAY = 12
CMN_ERR_TOO_MANY_TIMER = 13
CMN_ERR_ASSIGN_FAIL = 14
CMN_ERR_RESULT_FAIL = 15

CMN_ERR_CODE = {
    1 : 'Invalid Coin Price',
    2 : 'Not enough Money',
    3 : 'Invalid Format',
    4 : 'Invalid Coin',
    5 : 'Request Fail',
    6 : 'DB Select Error',
    7 : 'DB Update Error',
    8 : 'DB Insert Error',
    9 : 'DB Create Error',
    10 : 'DB Delete Error',
    11: 'No APP ID',
    12: 'Invalid Order way',
    13: 'Too Many Timer',
    14: 'Assign Fail',
    15: 'Result Fail',
}

MQ_TEST_BUY = 'trade_buy_test'
MQ_TR_ACK = 'trade_order_ack'
MQ_TR_NAK = 'trade_order_nak'
MQ_TR_COM = 'trade_order_com'
MQ_TR_FAIL = 'trade_order_fail'

MQ_BUY_ACK = 'trade_buy_test_success'
MQ_BUY_NAK = 'trade_buy_test_fail'
MQ_TEST_SELL = 'trade_sell_test'
MQ_SELL_ACK = 'trade_sell_test_success'
MQ_SELL_NAK = 'trade_sell_test_fail'
MQ_TEST_CANCEL = 'trade_cancel_test'
MQ_CANCEL_ACK = 'trade_cancel_success'
MQ_CANCEL_NAK = 'trade_cancel_fail'

MQ_BUY_COM = 'trade_buy_filled'
MQ_SELL_COM = 'trade_sell_filled'
MQ_TEST_BUY_COM = 'trade_buy_test_filled'
MQ_TEST_SELL_COM = 'trade_sell_test_filled'
MQ_BUY_FAIL = 'trade_buy_fail'
MQ_SELL_FAIL = 'trade_sell_fail'
MQ_TEST_BUY_FAIL = 'trade_buy_test_fail'
MQ_TEST_SELL_FAIL = 'trade_sell_test_fail'