
# coding=utf-8

from autoTradeApp.pandasLib import *
from .chartLib import *
from scipy import signal as sci_sig
from autoTradeApp import common_func
from .log import *
import locale


# trend
UP_TREND = 10
UP_ADJ_TREND = 9
UP_TO_DOWN_READY = 8

LIE_TREND = 5 # 횡보
DOWN_RALLY_TREND = 3
DOWN_TO_UP_READY = 2


DOWN_TREND = 1
SUDEN_DOWN_TREND = 2


# 봉 변화
NO_TREND = 0

# 중요 패턴
IMPORT_PATTERN = 5


TREND_STATE = 0
PREV_TREND_STATE = 0
LOCK_TREND = 0

TR_BUY = 2
TR_SELL = 1


LONG_TREND_UP = 50
LONG_TREND_DOWN = 51
LONG_TREND_LIE = 52

CHART_DOUBLE_BOTTOM = 100
CHART_DOUBLE_TOP = 101
CHART_BOX = 102
CHART_FLAG = 103
CHART_HEAD_AND_SHOLDER = 104
CHART_REV_HEAD_AND_SHOLDER = 105



defaultLossCut = -6
variationLossCut = 0

gap_line_index = [0]
taLog = LogWrapper(TA_STRA)
taLog.setLogOption([1, 1, 1, 1, 1, 1, 0]) # only exception enabled

def getFullTrendByLine(n_df_candle, serMa, position):

    if position < 0:
        pos = len(n_df_candle) - position
    elif position >= len(n_df_candle):
        pos = len(n_df_candle) - 1
    else:
        pos = position

    serMa = serMa[:pos+1]

    edgePoint = getHighAndLowLineMA5(n_df_candle, serMa, position, 1) # None Fix
    arrNonzero = edgePoint.nonzero()[0] # 값이 2면 low point


    if len(arrNonzero) < 2:
        return 0


    lowPointIdxs = np.where(edgePoint == 2)
    highPointIdxs = np.where(edgePoint == -2)

    lowPointIdxs = lowPointIdxs[0]
    highPointIdxs = highPointIdxs[0]

    if len(highPointIdxs) < 2 and len(lowPointIdxs) < 2:
        return 0
    #
    # if (serMa.max() - serMa.min()) > 0:
    #     normSerMa = (serMa - serMa.min()) / (serMa.max() - serMa.min())
    # else:
    #     normSerMa = (serMa - serMa.min())

    normSerMa = serMa

    highPricePoints = np.array(normSerMa[highPointIdxs].tolist(), dtype=float)
    lowPricePoints = np.array(normSerMa[lowPointIdxs].tolist(), dtype=float)

    highPctChg = np.diff(highPricePoints) / highPricePoints[:-1] * 100
    lowPctChg = np.diff(lowPricePoints) / lowPricePoints[:-1] * 100

    #taLog.critical('getFullTrendByLine', '[', serMa.index[pos], ']', highPctChg, '\n\n',lowPctChg)

    return lowPointIdxs, lowPricePoints, lowPctChg, highPointIdxs, highPricePoints, highPctChg

def getHighLowPctChg(n_df_candle, serMa, position):

    if position < 0:
        pos = len(n_df_candle) - position
    elif position >= len(n_df_candle):
        pos = len(n_df_candle) - 1
    else:
        pos = position

    res = getFullTrendByLine(n_df_candle, serMa, pos)
    if res == 0:
        return 0
    else:
        lowPointIdxs, lowPricePoints, lowPctChg, highPointIdxs, highPricePoints, highPctChg = res

    lowPointIdxs = np.append(lowPointIdxs, pos)[-3:]
    highPointIdxs = np.append(highPointIdxs, pos)[-3:]
    lowPricePoints = np.append(lowPricePoints, serMa[pos])[-3:]
    highPricePoints = np.append(highPricePoints, serMa[pos])[-3:]


    highPctChg = np.diff(highPricePoints) / highPricePoints[:-1] * 100
    lowPctChg = np.diff(lowPricePoints) / lowPricePoints[:-1] * 100

    highPeriodChg = np.diff(highPointIdxs)
    lowPeriodChg = np.diff(lowPointIdxs)

    taLog.critical('LongTrend', '[', serMa.index[pos], ']', highPctChg, lowPctChg, '\n', highPeriodChg, lowPeriodChg)

    return lowPctChg, lowPeriodChg, highPctChg, highPeriodChg

def checkChartPattern(n_df_candle, serMa, position):
    res = getFullTrendByLine(n_df_candle, serMa, position)
    if res == 0:
        return 0
    else:
        lowPointIdxs, lowPricePoints, lowPctChg, highPointIdxs, highPricePoints, highPctChg = res

    if (-1 < highPctChg[-1] < 1) :

        h1 = highPointIdxs[-1]
        h2 = highPointIdxs[-2]

        idxs = np.where((lowPointIdxs > h2) & (lowPointIdxs < h1))[0]
        if len(idxs) > 0:

            lowPrice = lowPricePoints[idxs[0]]
            highLowPct = getRaisePercentage(highPricePoints[-2], lowPrice)
            if highLowPct < -7:
                if (20 < h1 - h2 < 30):
                    taLog.critical('DoubleTOP ~~~~~~~~~~~~')
                    return True

    return 0







def isHighestPosIn6Month(n_df_candle):

    close = n_df_candle.close
    delta_time = datetime.timedelta(days=180)

    start_idx = close.index[-1] - delta_time
    interval = datetime.timedelta(days=10)

    #print start_idx
    #print close.index[-1]
    #print 'MAX idx', close[start_idx:].idxmax()
    #print len(close[start_idx:])

    max_date = close[start_idx:].idxmax()
    cur_date = close.index[-1]

    delt_time = cur_date - max_date
    #taLog.info(delt_time)

    if delt_time < interval:
        return True
    else:
        return False

def getLossCutPoint(dfCandle, buyPrice):
    global variationLossCut
    curPrice = dfCandle.close[-1]
    yesPrice = dfCandle.close[-2]
    rop = getRaisePercentage(buyPrice, curPrice)

    todayRop = getRaisePercentage(yesPrice, curPrice)
    if (buyPrice < curPrice) and (todayRop > 0):
        variationLossCut += todayRop/2


    if rop < variationLossCut:
        taLog.critical('손절매 loss cut point: ', variationLossCut)

        return TR_SELL

    return 0





def getLastGapLine(n_df_candle):
    global gap_line_index
    gap = checkGapAndPercentage(n_df_candle)

    if gap < -3.5:
        gap_line_index.append(len(n_df_candle)-1)

    elif gap > 3.5:
        gap_line_index.append(len(n_df_candle)-1)

    return gap_line_index[-1]

def checkSlopTrend(n_df_candle, ref_index, period, tr_flag, ser_volume):

    # 매도 혹은 매수 후 단기전인 추세를 보기 위한 함수
    cur_index = len(n_df_candle)
    close_ser = n_df_candle.close

    ma_vol = TA.MA(ser_volume, timeperiod=5)

    if (cur_index - ref_index) > period:
        return 0
    else:
        slop_ser = close_ser.pct_change() * 100

        x = slop_ser[-3]  # 4일전 종가와 3일전 종가 사이의 기울기
        y = slop_ser[-2]  # 전전일 종가와 전일 종가 사이의 기울기
        z = slop_ser[-1]  # 전일 종가와 금일 종가 사이의 기울기

        if tr_flag == TR_BUY:
            if z < -5:
                taLog.info('Slop Down Trend')
                return DOWN_TREND
            else:
                return UP_TREND
            #elif x < 0 and y < 0 and z < 0:
                #return DOWN_TREND

        elif tr_flag == TR_SELL:
            #taLog.info('SLOP', z, ma_vol[-1], ser_volume[-1])
            if (z > 3.5) and (ma_vol[-1] < ser_volume[-1]):
                taLog.info('Slop Up Trend')
                return UP_TREND


            return DOWN_TREND

            #elif y < 0 and z < -CP_YZ:

                #return DOWN_TREND

            #elif x > 0 and y > -10 and z > 0:
                #return UP_TREND

            #elif x < 0 and y < 0 and z > 0:
                #return UP_TREND




def slopTrend(n_df_candle, prev_trend):

    CP_Z = -5 # z 의 하락 판단 임계값
    CP_YZ = 17 # y - z 의 임계값

    anal_range = 6
    close_ser = n_df_candle.close
    current_trend = prev_trend[0]

    if len(close_ser) < 0:
        return NO_TREND

    slop_ser = close_ser.diff()

    x = slop_ser[-3] # 4일전 종가와 3일전 종가 사이의 기울기
    y = slop_ser[-2] # 전전일 종가와 전일 종가 사이의 기울기
    z = slop_ser[-1] # 전일 종가와 금일 종가 사이의 기울기

    if prev_trend[0] > LIE_TREND or prev_trend[0] == NO_TREND: # 상승장
        if z < CP_Z and y - z > CP_YZ:
            #print n_df_candle.index[-1], 'slop Down'
            current_trend= DOWN_TREND
        elif x < 0 and y < 0 and z < 0:
            #print n_df_candle.index[-1], 'slop Down'
            current_trend = DOWN_TREND
        elif y < 0 and z < -CP_YZ:
            #print n_df_candle.index[-1], 'slop Down'
            current_trend = DOWN_TREND


        # elif x > 0 and y > 0 and z < 0:
        #     current_trend = UP_ADJ_TREND
        #     print n_df_candle.index[-1], 'slop UP_ADJ_TREND'
        # elif x > 0 and y < 0 and z < 0:
        #     current_trend = UP_TO_DOWN_READY
        #     print n_df_candle.index[-1], 'slop UP_TO_DOWN_READY'


    elif prev_trend[0] < LIE_TREND or prev_trend[0] == NO_TREND: # 하락장
        if x > 0 and y > -10 and z > 0:
            #print n_df_candle.index[-1] , x, y, z, n_df_candle[-4:]
            #print n_df_candle.index[-1], 'slop Up'
            current_trend = UP_TREND

        elif x < 0 and y < 0 and z > 0:
            current_trend = DOWN_RALLY_TREND
            #print n_df_candle.index[-1], 'slop DOWN_RALLY_TREND'
        # elif x < 0 and y > 0 and z > 0:
        #     current_trend = DOWN_TO_UP_READY
        #     print n_df_candle.index[-1], 'slop DOWN_TO_UP_READY'

    if prev_trend[0] == current_trend:
        return NO_TREND
    else:
        prev_trend[0] = current_trend
        return current_trend


def getHighAndLowLineMA5(dfCandle, serMa, curIndex, noneFix=0):

    filter_window = 13
    checkLen = 4

    ma_arr = np.array(serMa)
    ma_filt = sci_sig.savgol_filter(serMa, filter_window, 2)
    ma_filt_slop_arr = np.array(np.diff(ma_filt))


    #non_nan_dat = ma_filt[~np.isnan(ma_filt)] nan 이 아닌 Array 얻기 ***

    serSignedDat = np.sign(ma_filt_slop_arr)
    serSignedDat = np.where(np.isnan(serSignedDat) == True, 0, serSignedDat)
    #serSlopFilt = pd.Series(serSignedDat, index=dfCandle.index)
    arrEdgePoint = np.diff(serSignedDat)

    if noneFix == 0:
        arrEdgePointNonZero = arrEdgePoint.nonzero()[0]

        arrEdgePointVal = ma_filt[arrEdgePointNonZero]
        arrEdgePointChange = np.diff(arrEdgePointVal) / arrEdgePointVal[:-1] * 100
        # print arrEdgePointChange
        # print len(arrEdgePointNonZero), len(arrEdgePointVal), len(arrEdgePointChange)


        for i, change in enumerate(arrEdgePointChange):
            if np.isnan(change):
                continue

            if abs(change) < 0.5:
                idx = arrEdgePointNonZero[i]
                idxNxt = arrEdgePointNonZero[i+1]
                #print idx, idxNxt
                arrEdgePoint[idx] = 0
                arrEdgePoint[idxNxt] = 0


    # for i, data in enumerate(arrEdgePoint):
    #     if i == len(arrEdgePoint) - checkLen +1:
    #         break
    #
    #     if data != 0:
    #         for j in range(1, checkLen):
    #             if arrEdgePoint[i+j] != 0:
    #                 arrEdgePoint[i] = 0
    #                 arrEdgePoint[i+j] = 0
    #                 break


    return arrEdgePoint








def checkHighAndLowLine_MA5(n_df_candle, ma_ser, high_list, low_list, cur_pos_list):
    periods = 5
    filter_window = 5
    conv_per = filter_window / 2
    slop_base = 0
    live_point_per = 10
    if len(n_df_candle) < filter_window:
        high_list.append(0)
        low_list.append(0)
        cur_pos_list.append(0)
        return

    high_list.append(0)
    low_list.append(0)
    cur_pos_list.append(0)

    ma_slop = ma_ser.diff()
    ma_arr = np.array(ma_slop)
    ma_slop_filt = sci_sig.savgol_filter(ma_arr, filter_window, 2)

    close_ser = n_df_candle.close
    sample_ma = ma_slop_filt[-filter_window:]
    conv_point = np.diff(np.sign(sample_ma))
    non_zero_index_arr = np.nonzero(conv_point[0:conv_per]) # 기울기는 2일 전거만 비

    for non_zero_idx in non_zero_index_arr[0]:
        pos = non_zero_idx - len(sample_ma)

        if len(non_zero_index_arr[0]) > 1:
            continue

        if conv_point[non_zero_idx] == 2: # 기울기가 음에서 양으로 갈때 음의 인덱스 저점선이 형성되는 시점
            #idx_list = n_df_candle.index

            #low_point = close_ser[pos]
            low_point = ma_ser[pos]

            arr = np.array(low_list[pos-filter_window:])

            if len(arr.nonzero()[0]) == 0:
                low_list[pos] = low_point
                cur_pos_list[-1] = low_point
            elif arr[arr > 0].min() > low_point:
                for i, price in enumerate(arr):
                    if price == arr[arr > 0].min():

                        low_list[i - len(arr)] = 0
                        break

                low_list[pos] = low_point
                cur_pos_list[-1] = low_point

        if conv_point[non_zero_idx] == -2: # 기울기가 양에서 음으로 갈때 양의 인덱스 고점선이 형성되는 시점

            #idx_list = n_df_candle.index

            #high_point = close_ser[pos]
            high_point = ma_ser[pos]

            arr = np.array(high_list[pos - filter_window:])


            if len(arr.nonzero()[0]) == 0:
                high_list[pos] = high_point
                cur_pos_list[-1] = high_point
            elif arr[arr > 0].max() < high_point:
                for i, price in enumerate(arr):
                    if price == arr[arr > 0].max():

                        high_list[i - len(arr)] = 0
                        break

                high_list[pos] = high_point
                cur_pos_list[-1] = high_point




def checkHighAndLowLine(n_df_candle, high_list, low_list, cur_pos_list):
    periods = 10
    ban_point_period = 5
    if len(n_df_candle) < periods:
        high_list.append(0)
        low_list.append(0)
        cur_pos_list.append(0)
        return

    high_list.append(0)
    low_list.append(0)
    cur_pos_list.append(0)

    ma_ser = TA.MA(n_df_candle['close'], timeperiod=periods)
    ma_slop = ma_ser.diff()
    close_ser = n_df_candle.close

    if (ma_slop[-2] <= 0) and (ma_slop[-1] > 0):
        idx_list = n_df_candle.index

        low_point = close_ser[-periods-1:-1].min()
        low_point_idx = close_ser[-periods-1:-1].idxmin()

        int_index = idx_list.get_loc(low_point_idx)

        np_list = np.array(low_list[int_index-ban_point_period:int_index+ban_point_period])

        if np_list != [] and np_list.max() == 0:
            low_list[int_index+1] = low_point
            cur_pos_list[-1] = n_df_candle['close'][-1]


    elif (ma_slop[-2] > 0) and (ma_slop[-1] <= 0):
        idx_list = n_df_candle.index

        high_point = close_ser[-periods-1:-1].max()
        high_point_idx = close_ser[-periods - 1:-1].idxmax()

        int_index = idx_list.get_loc(high_point_idx)

        np_list = np.array(high_list[int_index - ban_point_period:int_index + ban_point_period])

        if np_list != [] and np_list.max() == 0:
            high_list[int_index+1] = high_point
            cur_pos_list[-1] = n_df_candle['close'][-1]


def waitAdnSee(n_df_candle, ref_index, cur_index, period, tr_flag):

    # 매도 혹은 매수 후 단기전인 추세를 보기 위한 함수
    # 일단 2일 은 기달리기
    if (cur_index - ref_index) > period:
        return 0
    else:
        #if tr_flag == TR_SELL:
        #    if checkOneYangbong(n_df_candle.open[-1], n_df_candle.close[-1]) == True:
        #        return 0
        return 1

def reDefineLowHighLine(trade_flag, dict_idx_line_info, h_point_list, l_point_list, ref_idx, cur_idx):
    period = 3

    if (cur_idx - ref_idx) < period:

        regist_line_m, re_x, re_y = getSlopFromSupRegiList(h_point_list)  # 고점선
        support_line_m, su_x, su_y = getSlopFromSupRegiList(l_point_list)  # 저점선

        MakeLineData(dict_idx_line_info, regist_line_m, re_x, re_y, support_line_m, su_x, su_y,
                        0, trade_flag)


# 횡보 잡기
def checkSideWay(ma_ser, period):
    close = ma_ser

    price_list = close[len(ma_ser) - period:len(ma_ser)]
    std_val = np.std(price_list)
    #print ma_ser.index[-1], '표준편차', std_val

    if std_val < 0.75:
        return True # 횡보
    return False




def checkTrend(n_df_candle, ma_ser, trend_list):
    periods = 5
    lock_trenc_date = 2
    global TREND_STATE
    global LOCK_TREND


    if len(n_df_candle) < periods:
        trend_list.append(0)
        return 0

    trend_list.append(0)
    close = n_df_candle.close

    if (ma_ser.max() - ma_ser.min()) > 0:
        nomalize_ma = (ma_ser - ma_ser.min()) / (ma_ser.max() - ma_ser.min())
    else:
        return NO_TREND

    #c_mean = close.mean()
    #c_std = np.std(close)

    std_forside = 0
    if len(close) > 7:
        std_forside = np.std(close[-3:])

    #nomalize_close = (close - close.min()) / (close.max() - close.min())
    #nomalize_close = (close - c_mean)/c_std

    ma_slop = nomalize_ma.diff()
    close_per = close.pct_change()

    strs = str(ma_ser.index[-1]) + '  --  '+str(ma_slop[-1]) + '    ' + str(close_per[-1]) + '   ' + str(std_forside)
    #f.write(strs)

    #common_func.trace_print('trace.txt', strs)

    #ma_slop_yesterday = normalSlop(ma_short_df[-3:-1].tolist())
    #ma_slop_today = normalSlop(ma_short_df[-2:].tolist())

    if TREND_STATE == NO_TREND:
        if (ma_slop[-2] < 0.0) and (ma_slop[-1] > 0.0):
            TREND_STATE = UP_TREND
            trend_list[-1] = UP_TREND
            taLog.info(n_df_candle.index[-1], ' UP_TREND')

        elif (ma_slop[-2] >= 0.0) and (ma_slop[-1] <= 0.0):
            TREND_STATE = DOWN_TREND
            trend_list[-1] = DOWN_TREND
            taLog.info(n_df_candle.index[-1], ' DOWN_TREND')


    # ---------------------------------------------------------------------------------

    if TREND_STATE == UP_TREND:
        if ma_slop[-1] <= 0.0:
            if (ma_slop[-1] > -0.03) and (LOCK_TREND < lock_trenc_date):
                LOCK_TREND += 1
                return TREND_STATE

            LOCK_TREND = 0
            TREND_STATE = DOWN_TREND
            trend_list[-1] = DOWN_TREND
            taLog.info(n_df_candle.index[-1], ' DOWN_TREND')
            #print strs

        if LOCK_TREND > 0:
            LOCK_TREND -= 1

        if close_per[-1] < -0.048:
            LOCK_TREND = 0
            TREND_STATE = DOWN_TREND
            trend_list[-1] = DOWN_TREND
            taLog.info(n_df_candle.index[-1], ' DOWN_TREND')
            #print strs



    elif TREND_STATE == DOWN_TREND:
        if ma_slop[-1] > 0.0:
            if (ma_slop[-1] < 0.005) and (LOCK_TREND < lock_trenc_date):
                LOCK_TREND += 1
                return TREND_STATE

            LOCK_TREND = 0
            TREND_STATE = UP_TREND
            trend_list[-1] = UP_TREND
            taLog.info( n_df_candle.index[-1], ' UP_TREND')
            #print strs

        if LOCK_TREND > 0:
            LOCK_TREND -= 1

    return TREND_STATE



def candlePatternSignal(n_df_candle, high_line_list, low_line_list, trend, date = -1):
    # trend_flag 가 1 이면 Trend를 보지않고 현제 패턴에만 따라서 하락 패턴이면
    def print_fuc(*args):
        print_flag = 1
        if print_flag == 1:
            taLog.debug(*args)

    #if len(n_df_candle) < 5:
    #    return 0
    open = n_df_candle.open
    high = n_df_candle.high
    low = n_df_candle.low
    close = n_df_candle.close

    # 상승장악형의 경우 100 하락 장악형의 경우 -100
    s_result = TA.CDLDOJI(open, high, low, close)
    if s_result[date] != 0:
        print_fuc(n_df_candle.index[date], ' ', 'Doji Exist', trend)

    s_result = TA.CDLEVENINGDOJISTAR(open, high, low, close)
    if s_result[date] != 0:
        print_fuc(n_df_candle.index[date], ' ', 'Evening Doji Star', trend)

    s_result = TA.CDLEVENINGSTAR(open, high, low, close)
    if s_result[date] != 0:
        print_fuc(n_df_candle.index[date], ' ', 'Evening Star', trend)

    # --- custm pattern ----------------------------
    res = customEveningStar(open, high, low, close)
    if res == True:
        return SUDEN_DOWN_TREND, C_CDLEVENINGSTAR

    # --- custm pattern ----------------------------
    s_result = customHangingMan(open[-2], high[-2], low[-2], close[-2])
    if s_result == True and checkGapAndPercentage(n_df_candle[:-1]) > 1:

        f_result = TA.CDLLONGLINE(open, high, low, close)
        if f_result[date] < 0:
            print_fuc(n_df_candle.index[date], ' ', 'Custom Hanging Man and Long Line')
            return SUDEN_DOWN_TREND, custom



    s_result = TA.CDLHAMMER(open, high, low, close)
    if s_result[date] > 0:
        print_fuc(n_df_candle.index[date], ' ', 'Hammer exist')
        if trend != UP_TREND:
            return UP_TREND, HAMMER
        else:
            return IMPORT_PATTERN, HAMMER


    if s_result[date -1] > 0:
        if getRaisePercentage(close[-2], close[-1]) < -0.5:
            m, idx, regi = getSlopFromSupRegiList(high_line_list)
            if regi < n_df_candle.close[-1]:
                return SUDEN_DOWN_TREND, HAMMER

    s_result = TA.CDLINVERTEDHAMMER(open, high, low, close)
    if s_result[date] != 0:
        print_fuc(n_df_candle.index[date], ' ', 'Inverted Hammer')
        if trend != UP_TREND:
            m, idx, sup = getSlopFromSupRegiList(low_line_list)
            if sup > n_df_candle.close[-1]:
                print_fuc('Invert Hammer Buy', sup, n_df_candle.close[-1])
                return UP_TREND, INHAMMER

    # 잘안나오는 패턴
    s_result = TA.CDL3WHITESOLDIERS(open, high, low, close)
    if s_result[date] != 0:
        print_fuc(n_df_candle.index[date], ' ', 'white solider exist')
        return UP_TREND, CDL3WHITESOLDIERS


    s_result = TA.CDLENGULFING(open, high, low, close)
    if s_result[date] > 0:
        print_fuc(n_df_candle.index[date], ' ', 'Bullish_Engulfing exist', trend)
        if trend != UP_TREND:
            return UP_TREND, Bullish_Engulfing

    elif s_result[date] < 0:
        pers = getRaisePercentage(open[-1], close[-1])
        print_fuc(n_df_candle.index[date], ' ', 'Bearish_Engulfing exist', trend)
        if pers < -3.5:
            print_fuc(n_df_candle.index[date], ' ', 'SUDDEN SELL ')
            return SUDEN_DOWN_TREND, Bearish_Engulfing

        if trend != DOWN_TREND:
            return DOWN_TREND, Bearish_Engulfing

    # 갭상승 후 거래량이 많은 장대 음봉은 일단 파는걸로 코딩
    s_result = TA.CDLLONGLINE(open, high, low, close)
    if s_result[date] < 0:
        print_fuc(n_df_candle.index[date], ' ', 'Long Line Detected ')
        if n_df_candle.close[-2] < n_df_candle.open[-1]:
            vol_result = TA.MA(n_df_candle.volume, timeperiod=5)
            if vol_result[-1] < n_df_candle.volume[-1]:
                if trend != DOWN_TREND:
                    print_fuc('LongLine Sell')
                    return DOWN_TREND, CDLLONGLINE

    # 샛별형
    s_result = TA.CDLMORNINGSTAR(open, high, low, close)
    if s_result[date] > 0:
        print_fuc(n_df_candle.index[date], ' ', 'Morning Star exist')
        return UP_TREND, CDLMORNINGSTAR

    # 잠자리 도지 보통 나온뒤 횡보하다 올라가는 경향이 있는듯 ? 횡보 추세가 필요할 듯 반대 그레이브 스톤
    s_result = TA.CDLDRAGONFLYDOJI(open, high, low, close)
    if s_result[date] > 0:
        print_fuc(n_df_candle.index[date], ' ', 'DragonFly exist')
        return UP_TREND, CDLDRAGONFLYDOJI

    # 샅바형 # 바닥권에서만 적용
    # s_result = TA.CDLBELTHOLD(n_df_candle.open, n_df_candle.high, n_df_candle.low, n_df_candle.close)
    # if s_result[date] > 0:
    #     if trend != UP_TREND:
    #         print_fuc(n_df_candle.index[date], ' ', 'Belt Hold UP')
    #         return UP_TREND, CDLBELTHOLD
    #
    # elif s_result[date] < 0:
    #     if trend != DOWN_TREND:
    #         print_fuc(n_df_candle.index[date], ' ', 'Belt Hold DOWM')
    #         return DOWN_TREND, CDLBELTHOLD

    # 긴 다리 doji 투자자들의 우유부단함 보통 다음날 양봉이면 가는듯  보이지만 워낙 자주나오는 패턴이라 주의 해야 할 듯
    '''
    s_result = TA.CDLLONGLEGGEDDOJI(n_df_candle.open, n_df_candle.high, n_df_candle.low, n_df_candle.close)
    if s_result[-2] > 0:
        print n_df_candle.index[-1], ' ''long legged doji'
        if checkOneYangbong(n_df_candle.open[-1], n_df_candle.high[-1], n_df_candle.low[-1], n_df_candle.close[-1]):
            return UP_SIGNAL
    '''
    # ------------down Signal ------------------------------------------------------------------------------------------

    s_result = TA.CDLHANGINGMAN(open, high, low, close)
    if s_result[date] != 0:
        print_fuc(n_df_candle.index[date], ' ''Hanging man exist')
        if trend != UP_TREND:
            return UP_TREND, CDLHANGINGMAN
        elif trend != DOWN_TREND:
            return DOWN_TREND, CDLHANGINGMAN


    # 전고점보다 높을 시 투입
    s_result = TA.CDLDARKCLOUDCOVER(open, high, low, close)
    if s_result[date] < 0:
        print_fuc(n_df_candle.index[date], ' Dark Cloud Cover')
        m, idx, regi = getSlopFromSupRegiList(high_line_list)
        if trend != DOWN_TREND and regi < n_df_candle.close[-1]:
            return DOWN_TREND, DARKCLOUD
        # print_fuc(n_df_candle.index[-1], ' ', 'Dark Cloud Cover')
        # arr = np.array(high_point_list)
        # arr_1de = arr.nonzero()
        # if n_df_candle.close[arr_1de[0][-1]] < n_df_candle.close[-1]:
        #     if trend > LIE_TREND or trend_flag:
        #         return DOWN_TREND

    return 0, None

# 현제 가격의 위치
def momentumSignal(df_candle, trend):

    F_K_P = 5
    S_K_P = 3
    S_D_P = 3
    RSI =30

    if len(df_candle) < F_K_P:
        return 0

    slowk, slowd = TA.STOCH(df_candle['high'], df_candle['low'], df_candle['close'],
                            fastk_period=F_K_P, slowk_period=S_K_P, slowk_matype=0, slowd_period=S_D_P, slowd_matype=0)

    if slowk[-1] < 65:
        return UP_TREND # 상승이 기대될수 있다는 의미
    else:
        return DOWN_TREND

    #if (slowd[-2] - slowk[-2] > 0) and (slowd[-1] - slowk[-1] <= 0) and slowk[-1] < RSI:
    # if slowd[-1] < 40:
    #     if trend == UP_TREND:
    #         return BUY_SIG
    # elif (slowd[-2] - slowk[-2] < 0) and (slowd[-1] - slowk[-1] >= 0):
    #     return SELL_SIG

    #return 0


def CheckTrendGraph(result_df):

    #marker
    trend_up = result_df.up_trend_ch_signal
    trend_down = result_df.down_trend_ch_signal

    pattern_up = [1 if x == UP_TREND else 0 for x in result_df.pattern_signal]
    pattern_down = [1 if x == DOWN_TREND else 0 for x in result_df.pattern_signal]

    momen_up = [1 if x == UP_TREND else 0 for x in result_df.momentum_signal]
    momen_down = [1 if x == DOWN_TREND else 0 for x in result_df.momentum_signal]

    slop_trend_up = result_df.slop_up
    slop_trend_down = result_df.slop_down

    high_line = result_df.high_point
    low_line = result_df.low_point
    cur_line = result_df.cur_point # high point, low point 를 설정했을 시점의 주가 데이터

    tr_buy = result_df.trade_buy
    tr_sell = result_df.trade_sell

    ##

    mark_list = getHighAndLowLineMA5(result_df, result_df.ma_5, len(result_df))

    new_high_line = np.where(mark_list == 2, 1, 0)
    new_low_line = np.where(mark_list == -2, 1, 0)

    line_52 = [result_df.close[-365:].min(), result_df.close[-365:].max()] # 52주 최저가, 최고
    sr_line = [high_line, low_line]
    #sr_line = [line_52]
    #print result_df.close[up_index]

    ma_list = [result_df.ma_5, result_df.ma_25]
    sep_tech_list = [result_df.slowk, result_df.slowd]
    #sep_tech_list = [result_df.ma5_savgol_filter]
    #sep_tech_list = [result_df.ma5_1_slop]
    #sep_tech_list = [result_df.nomalize_ma5_slop]
    #sep_tech_list = [result_df.rsi]

    #marker_list = [high_line, low_line, cur_line]
    #marker_list = [high_line, low_line]
    #marker_list = [new_high_line, new_low_line]
    marker_list = [tr_buy, tr_sell]
    #marker_list = [trend_up, trend_down]

    plot_candles(result_df, volume_bars=True, volume_tech= [result_df.ma_5_vol], technicals=ma_list,
                 marker=marker_list, line_52=line_52, sep_technicals=sep_tech_list, sr_line= sr_line)
    #plot_candles(anal_df, volume_bars=True, technicals=[result_df.ma_5], marker=[up, down])

# 리스트에서 0이 아닌 값을 걸러내서 두 점사이에 기울기를 구한다.
def getSlopFromSupRegiList(point_list):
    arr = getNonzeroIndexFromList(point_list)
    if len(arr) == 0:
        return None, 0, 0
    elif len(arr) == 1:
        return None, arr[-1], point_list[arr[-1]]

    x1 = arr[-2]
    x2 = arr[-1]
    y1 = point_list[arr[-2]]
    y2 = point_list[arr[-1]]

    m = getSlope(x1, x2, y1, y2)

    return m, x2, y2

'''
dict_idx_line_info
{
    'buy_index':
        'b_resi_coor': (x, y)
'b_supp_coor': (x, y)
'b_resi_slop': slop
'b_supp_slop': slop

'sell_index':
's_resi_coor': (x, y)
's_supp_coor': (x, y)
's_resi_slop': slop
's_supp_slop': slop
}
'''


TRADE_FLAG = [0]
LINE_EXCEED = 0
LOSS_PER = -3.0
LINE_AFTER_TRADE = {
    'buy_index': 0,
    'b_resi_coord': (0, 0.0),
    'b_supp_coord': (0, 0.0),
    'b_resi_slop': 0.0,
    'b_supp_slop': 0.0,

    'sell_index': 0,
    's_resi_coord': (0, 0.0),
    's_supp_coord': (0, 0.0),
    's_resi_slop': 0.0,
    's_supp_slop': 0.0,
    'loss_point': LOSS_PER,

    'ref_index': 0,
    'my_asset':{}
}

def setLineDataRefIDX(dict_line_info, val = 0):
    if val == 0:
        dict_line_info['ref_index'] = 3
    elif val > 0:
        dict_line_info['ref_index'] += val

def decLineDataRefIDX(dict_line_info, val = 0):
    if dict_line_info['ref_index'] > 0:
        dict_line_info['ref_index'] -= 1

    if val == -1:
        dict_line_info['ref_index'] = 0

def updateAsset(assets, candle_df, tr_flag=0):


    last_cash =assets['asset_cash'][-1]
    last_sock = assets['asset_stock'][-1]
    #print last_cash

    if tr_flag == 0:
        assets['asset_cash'].append(last_cash)
        assets['asset_stock'].append(last_sock)

    if len(candle_df) < 1:
        return

    cur_stock = assets['asset_stock'][-1]

    cur_price = candle_df.close[-1]
    cur_cash = assets['asset_cash'][-1]

    if tr_flag == TR_BUY:
        stock_cnt = int(cur_cash / int(cur_price))
        assets['asset_stock'][-1] = stock_cnt
        assets['asset_cash'][-1] -= cur_price*stock_cnt

    elif tr_flag == TR_SELL:
        assets['asset_stock'][-1] -= cur_stock
        assets['asset_cash'][-1] += cur_price*cur_stock





def actionBUY(candle_df, trade_flag, dict_idx_line_info, h_point_list, l_point_list):
    regist_line_m, re_x, re_y = getSlopFromSupRegiList(h_point_list)  # 고점선
    support_line_m, su_x, su_y = getSlopFromSupRegiList(l_point_list)  # 저점선

    trade_flag[0] = TR_BUY
    #print candle_df.index[-1], 'BUY', ' Pirce: ', candle_df.close[-1]
    my_asset = dict_idx_line_info['my_asset']
    updateAsset(my_asset, candle_df, TR_BUY)

    return MakeLineData(dict_idx_line_info, regist_line_m, re_x, re_y, support_line_m, su_x, su_y, len(candle_df) - 1,
                        TR_BUY)


def actionSELL(candle_df, trade_flag, dict_idx_line_info, h_point_list, l_point_list):
    global variationLossCut
    global defaultLossCut
    variationLossCut = defaultLossCut
    regist_line_m, re_x, re_y = getSlopFromSupRegiList(h_point_list)  # 고점선
    support_line_m, su_x, su_y = getSlopFromSupRegiList(l_point_list)  # 저점선

    trade_flag[0] = TR_SELL
    #print candle_df.index[-1], 'SELL', ' Pirce: ', candle_df.close[-1]
    my_asset = dict_idx_line_info['my_asset']
    updateAsset(my_asset, candle_df, TR_SELL)
    return MakeLineData(dict_idx_line_info, regist_line_m, re_x, re_y, support_line_m, su_x, su_y,
                        len(candle_df) - 1,
                        TR_SELL)

def trandStrategyDesign2(trend, pattern_sig, pattern_name, momentum_sig, volume_sig, l_point_list,
                        h_point_list, candle_df, ma_ser, dict_idx_line_info, sl_trend, assets):

    def print_fuc(*args):
        print_flag = 1
        if print_flag == 1:
            taLog.warning(*args)

    regist_line_m, re_x, re_y = getSlopFromSupRegiList(h_point_list) #고점선 (ma 5)
    support_line_m, su_x, su_y = getSlopFromSupRegiList(l_point_list) #저점선 (ma 5)
    dict_idx_line_info['my_asset'] = assets
    modi_trend = trend

    #----------------------공통
    cur_price = candle_df.close[-1]  # 금일 주가, 장중 주가
    gap_pct = checkGapAndPercentage(candle_df)  # gap check
    gap_line_idx = getLastGapLine(candle_df)
    gap_line = candle_df.close[gap_line_idx]
    volume = candle_df.volume


    # Long Term Trend 아래 함수로 대체
    coeff = linReg2(candle_df, ma_ser)
    long_trend = coeff[0]

    #----------------------------

    if trend == UP_TREND and TRADE_FLAG[0] == 0:
        return actionBUY(candle_df, TRADE_FLAG, dict_idx_line_info, h_point_list, l_point_list)

    if TRADE_FLAG[0] == TR_BUY:
        tr_res_idx, tr_res_price = dict_idx_line_info['b_resi_coord'] #고 ma 5
        tr_sup_idx, tr_sup_price = dict_idx_line_info['b_supp_coord'] #저 ma 5


        buy_index = dict_idx_line_info['buy_index']
        buy_price = candle_df.close[buy_index]  # 중간에 값이 변경되는지 확인

        rop = getRaisePercentage(buy_price, cur_price) # rate of profit 수익률

        # 손절 알고리즘 데이터 박지 말고 유동적으로 가야 할 듯
        if getLossCutPoint(candle_df, buy_price) == TR_SELL:
            return actionSELL(candle_df, TRADE_FLAG, dict_idx_line_info, h_point_list, l_point_list)

        tmp_trend = checkSlopTrend(candle_df, buy_index, 3, TR_BUY, volume)
        if tmp_trend != 0:
            modi_trend =tmp_trend



        print_fuc(candle_df.index[-1], ' ',
                  'SupLine', tr_sup_price, 'ResLine', tr_res_price, 'CurP', cur_price, 'SlopAndY', coeff)

        # 중요한 갭하락후 저항선은 갭하락 선이 된다 는 알고리즘 작성
        if (pattern_sig == DOWN_TREND) and rop > 4: # 수익을 어느정도 먹으면 다운 트렌드 패턴에 바로 매도
            if (pattern_name == CDLHANGINGMAN) and (isHighestPosIn6Month(candle_df) == False):
                taLog.error( 'haningman 안팜 ')
                return 0

            taLog.error('TRADE', 'pattern and 수익률 매도')

            getHighLowPctChg(candle_df, ma_ser, len(candle_df))###############

            return actionSELL(candle_df, TRADE_FLAG, dict_idx_line_info, h_point_list, l_point_list)
        elif (pattern_sig == DOWN_TREND) and (tr_res_price != 0) and (tr_res_price < cur_price):
            taLog.error('TRADE', 'pattern and 최고점 보다 높은경우 매도')

            getHighLowPctChg(candle_df, ma_ser, len(candle_df))###############

            return actionSELL(candle_df, TRADE_FLAG, dict_idx_line_info, h_point_list, l_point_list)
        elif (pattern_sig == DOWN_TREND) and (checkPriceBand(gap_line, cur_price, 0.7)):
            taLog.error('TRADE', 'pattern and Gap 저항선에서 떨어지는 양상을 보이는 경우')
            return actionSELL(candle_df, TRADE_FLAG, dict_idx_line_info, h_point_list, l_point_list)
        elif (pattern_sig == SUDEN_DOWN_TREND):
            taLog.error('TRADE', 'pattern Suden down trend')

            getHighLowPctChg(candle_df, ma_ser, len(candle_df))###############

            return actionSELL(candle_df, TRADE_FLAG, dict_idx_line_info, h_point_list, l_point_list)
        elif modi_trend == DOWN_TREND:
            # momentum 에 저항선 지지선을 같이 봐야 할 듯
            if momentum_sig == DOWN_TREND:
                taLog.error('TRADE', 'DownTrend + 모멘텀 매도 ')

                getHighLowPctChg(candle_df, ma_ser, len(candle_df))###############

                return actionSELL(candle_df, TRADE_FLAG, dict_idx_line_info, h_point_list, l_point_list)
            # 저점 대비 많이 오른 상태면 매도 한다.
            if isHighestPosIn6Month(candle_df) == True:
                taLog.error('TRADE', 'DownTrend 저점 대비 오른상태 매도 ')

                getHighLowPctChg(candle_df, ma_ser, len(candle_df)) ###############

                return actionSELL(candle_df, TRADE_FLAG, dict_idx_line_info, h_point_list, l_point_list)
            # 급하게 꺽기면 매도한다.

    elif TRADE_FLAG[0] == TR_SELL:
        tr_res_idx, tr_res_price = dict_idx_line_info['s_resi_coord'] #고 ma 5
        tr_sup_idx, tr_sup_price = dict_idx_line_info['s_supp_coord'] #저 ma 5

        sell_index = dict_idx_line_info['sell_index']
        sell_price = candle_df.close[sell_index]  # 중간에 값이 변경되는지 확인


        tmp_trend = checkSlopTrend(candle_df, sell_index, 5, TR_SELL, volume)

        if tmp_trend != 0:
            modi_trend =tmp_trend


        if pattern_sig == DOWN_TREND:
            setLineDataRefIDX(dict_idx_line_info)

        decLineDataRefIDX(dict_idx_line_info)
        if dict_idx_line_info['ref_index'] > 0:
            if getRaisePercentage(candle_df.close[-2], cur_price) < 0:
                setLineDataRefIDX(dict_idx_line_info, 1)
                 #decLineDataRefIDX(dict_idx_line_info, -1)
            # else:
            modi_trend = DOWN_TREND

        print_fuc(candle_df.index[-1], ' ',
                  'SupLine', tr_sup_price, 'ResLine', tr_res_price, 'CurP', cur_price, 'SlopAndY', coeff)

        if (pattern_sig == UP_TREND) and (long_trend > -2.5): # 처리 더 해줘야 해

            taLog.error('TRADE', 'pattern 매수')
            checkChartPattern(candle_df, ma_ser, len(candle_df))
            getHighLowPctChg(candle_df, ma_ser, len(candle_df))
            return actionBUY(candle_df, TRADE_FLAG, dict_idx_line_info, h_point_list, l_point_list)
        elif modi_trend == UP_TREND:
            # 주가 상태 Check
            taLog.error('TRADE', 'Trend 매수 ')
            checkChartPattern(candle_df, ma_ser, len(candle_df))
            getHighLowPctChg(candle_df, ma_ser, len(candle_df))
            return actionBUY(candle_df, TRADE_FLAG, dict_idx_line_info, h_point_list, l_point_list)

    return 0



def MakeLineData(line_after_trade, res_m, res_x, res_y, sup_m, sup_x, sup_y, trade_idx, trade_type):
    if trade_type == TR_BUY:
        if trade_idx != 0:
            line_after_trade['buy_index'] = trade_idx
            line_after_trade['loss_point'] = LOSS_PER

        line_after_trade['b_resi_coord'] = (res_x, res_y)
        line_after_trade['b_supp_coord'] = (sup_x, sup_y)
        line_after_trade['b_resi_slop'] = res_m
        line_after_trade['b_supp_slop'] = sup_m
        line_after_trade['ref_index'] = 0

        #pp = pprint.PrettyPrinter(indent=4)
        #pp.pprint(line_after_trade)

        return TR_BUY

    elif trade_type == TR_SELL:
        if trade_idx != 0:
            line_after_trade['sell_index'] = trade_idx
        line_after_trade['s_resi_coord'] = (res_x, res_y)
        line_after_trade['s_supp_coord'] = (sup_x, sup_y)
        line_after_trade['s_resi_slop'] = res_m
        line_after_trade['s_supp_slop'] = sup_m
        line_after_trade['ref_index'] = 0
        #pp = pprint.PrettyPrinter(indent=4)
        #pp.pprint(line_after_trade)
        return TR_SELL


    return 0


def estimateTrade(ser_buy, ser_sell, candle_df):
    f = open('estimate_trade.txt', 'w+')
    ser_close = candle_df.close

    for i in range(len(ser_buy)):
        if ser_buy[i] == 1:
            data = ser_close[i:i + 4].pct_change()
            strs = 'buy ' + str(ser_close.index[i]) + ' '
            for j in data:
                strs += str(j*100) + ', '
            strs += '\n'
            f.write(strs)

        if ser_sell[i] == 1:
            data = ser_close[i:i + 4].pct_change()
            strs = 'sell ' + str(ser_close.index[i]) + ' '
            for j in data:
                strs += str(j*100) + ', '
            strs += '\n'
            f.write(strs)

def startTest():
    #test_df = csvToDataFrame('AAPL.csv', path_from=1)
    test_df = csvToDataFrame('신세계.KS.csv', path_from=1)
    #anal_df = test_df['20160111':'20160920']
    anal_df = test_df['20180111':'20190228']
    #anal_df = test_df['20180111':]

    ma_ser = TA.MA(anal_df['close'], timeperiod=5)
    ma_25ser = TA.MA(anal_df['close'], timeperiod=7)
    ma_5_vol = TA.MA(anal_df['volume'], timeperiod=5)

    F_K = 5
    S_K = 3
    S_D = 3
    init_cash_won = 10000000 # 천만원

    slowk, slowd = TA.STOCH(anal_df['high'], anal_df['low'], anal_df['close'],
                            fastk_period=F_K, slowk_period=S_K, slowk_matype=0, slowd_period=S_D, slowd_matype=0)
    fastk, fastd = TA.STOCHF(anal_df['high'], anal_df['low'], anal_df['close'],
                            fastk_period=F_K, fastd_period=S_K, fastd_matype=0)

    real = TA.RSI(anal_df['close'], timeperiod=14)

    nomalize_ma = (ma_ser - ma_ser.min()) / (ma_ser.max() - ma_ser.min())
    nomalize_slop = nomalize_ma.diff()

    anal_df = anal_df.assign(ma_5=ma_ser.values)
    anal_df = anal_df.assign(nomalize_ma5_slop=nomalize_slop.values)
    anal_df = anal_df.assign(ma_25=ma_25ser.values)
    anal_df = anal_df.assign(slowk=slowk.values)
    anal_df = anal_df.assign(slowd=slowd.values)
    anal_df = anal_df.assign(fastk=fastk.values)
    anal_df = anal_df.assign(fastd=fastd.values)
    anal_df = anal_df.assign(rsi=real.values)
    anal_df = anal_df.assign(ma_5_vol=ma_5_vol.values)



    # no_na_ser = anal_df['ma_5'].dropna()
    no_na_ser = anal_df['ma_5']

    trend_list = []
    signal_list = []
    momentum_sig_list = []
    high_point_list = []
    low_point_list = []
    cur_point_list = []
    slop_trend = []
    prev_slop_trend = [NO_TREND] # memory

    assets = {
        'asset_cash': [init_cash_won],
        'asset_stock': [0]
    }


    buy_or_sell = []

    TREND_PERIOD = 3
    CANDLE_CHECK_PERIOD = 25 # 기간이 짧으면 패턴이 안뜸 .. 추세까지도 보는듯

    for i in range(0, len(no_na_ser)):
        updateAsset(assets, anal_df[0:i], 0)

        checkHighAndLowLine_MA5(anal_df[0:i], ma_ser[0:i], high_point_list, low_point_list, cur_point_list)
        #checkHighAndLowLine(anal_df[0:i], low_point_list, high_point_list, cur_point_list) # 저항선 지지선 ma5 로 설정하기
        trend = checkTrend(anal_df[0:i], ma_ser[0:i], trend_list)
        if i < TREND_PERIOD:
            signal_list.append(NO_TREND)
            momentum_sig_list.append(NO_TREND)
            slop_trend.append(NO_TREND)
            buy_or_sell.append(NO_TREND) # 0,1,2
            continue


        momentum_sig = momentumSignal(anal_df[0:i], trend)
        slop_t_signal = slopTrend(anal_df[0:i], prev_slop_trend) # 굉장히 부정확함
        #slop_t_signal = 0

        signal, pattern_name = candlePatternSignal(anal_df[0:i], high_point_list, low_point_list, trend)

        # trade_sig = trade_strategy(slop_t_signal, signal, momentum_sig, 0,
        #                             low_point_list, high_point_list, anal_df[0:i], LINE_AFTER_TRADE, prev_slop_trend)

        trade_sig = trandStrategyDesign2(trend, signal, pattern_name, momentum_sig, 0,
                                    low_point_list, high_point_list, anal_df[0:i], ma_ser[0:i], LINE_AFTER_TRADE, prev_slop_trend, assets)
        # trade_sig = trandStrategyDesign(trend, signal, pattern_name, momentum_sig, 0,
        #                            low_point_list, high_point_list, anal_df[0:i], ma_ser[0:i], LINE_AFTER_TRADE, prev_slop_trend)
        #trade_sig =0
        # 리스트에 추가 되는 인덱스는 한칸 다음으로 추가된다. 왼쪽으로 쉬프트 필요

        signal_list.append(signal)
        momentum_sig_list.append(momentum_sig)
        slop_trend.append(slop_t_signal)
        buy_or_sell.append(trade_sig)

    trend_ser = pd.Series(trend_list, index = no_na_ser.index)
    anal_df = anal_df.assign(trend=trend_ser.values)
    anal_df['up_trend'] = np.where(anal_df['trend'] == UP_TREND, 1, 0)
    anal_df['down_trend'] = np.where(anal_df['trend'] == DOWN_TREND, 1, 0)
    anal_df['pattern_signal'] = pd.Series(signal_list, index = no_na_ser.index).shift(-1)

    anal_df['up_trend_ch_signal'] = anal_df['up_trend'].diff().shift(-1)
    anal_df['down_trend_ch_signal'] = anal_df['down_trend'].diff().shift(-1)

    anal_df['momentum_signal'] = pd.Series(momentum_sig_list, index = no_na_ser.index).shift(-1)

    anal_df['high_point'] = pd.Series(high_point_list, index = no_na_ser.index).shift(-1)
    anal_df['low_point'] = pd.Series(low_point_list, index=no_na_ser.index).shift(-1)
    anal_df['cur_point'] = pd.Series(cur_point_list, index = no_na_ser.index).shift(-1)

    anal_df['close_1_slop'] = anal_df.close.diff()
    anal_df['close_2_slop'] = anal_df.close.diff(2) / 2
    anal_df['ma5_1_slop'] = anal_df.ma_5.diff()
    ma5_1_arr = np.array(anal_df['ma5_1_slop'])
    filtered = sci_sig.savgol_filter(ma5_1_arr, 7, 1)
    anal_df['ma5_savgol_filter'] = pd.Series(filtered, index = no_na_ser.index)

    anal_df['ma5_2_slop'] = anal_df.ma_5.diff(2) / 2

    slop_trend.pop(0)
    slop_trend.append(0)
    np_slop_arr = np.array(slop_trend)


    anal_df['slop_up'] = np.where(np_slop_arr == UP_TREND, 1, 0)
    anal_df['slop_down'] = np.where(np_slop_arr == DOWN_TREND, 1, 0)
    anal_df['slop_up_adj'] = np.where(np_slop_arr == UP_ADJ_TREND, 1, 0)
    anal_df['slop_up_to_down'] = np.where(np_slop_arr == UP_TO_DOWN_READY, 1, 0)
    anal_df['slop_lie'] = np.where(np_slop_arr == LIE_TREND, 1, 0)
    anal_df['slop_down_rally'] = np.where(np_slop_arr == DOWN_RALLY_TREND, 1, 0)
    anal_df['slop_up_ready'] = np.where(np_slop_arr == DOWN_TO_UP_READY, 1, 0)

    buy_or_sell.pop(0)
    buy_or_sell.append(0)
    np_buy_or_sell = np.array(buy_or_sell)
    anal_df['trade_buy'] = np.where(np_buy_or_sell == TR_BUY, 1, 0)
    anal_df['trade_sell'] = np.where(np_buy_or_sell == TR_SELL, 1, 0)

    estimateTrade(anal_df['trade_buy'], anal_df['trade_sell'], anal_df)

    #print anal_df[['up_trend', 'down_trend']]
    #checkTrendGragh(anal_df)
    #f.close()
    #common_func.trace_print('trace.txt', '-----------------------finish--------------------')


    taLog.info( '*'*20)
    taLog.info( '초기 현금 : ', init_cash_won)
    total_asset = assets['asset_cash'][-1] + assets['asset_stock'][-1] * anal_df.close[-1]
    taLog.info( '테스트 말 자산 :', assets['asset_cash'][-1] + assets['asset_stock'][-1] * anal_df.close[-1])
    taLog.info( '테스트 말 보유 주식 : ', assets['asset_stock'][-1], ' 테스트 말 주가 : ', anal_df.close[-1])
    taLog.info( '테스트 말 현금 :', assets['asset_cash'][-1])
    taLog.info( '테스트 기간 :', anal_df.index[0], ' ~', anal_df.index[-1])
    taLog.info( '테스트 기간 수익률: ', (total_asset - init_cash_won) * 100 / init_cash_won, '%')
    taLog.info( '테스트 기간 수익: ', total_asset - init_cash_won)
    init_stock = int (init_cash_won / anal_df.close[0])
    init_cash = init_cash_won - init_stock * anal_df.close[0]
    taLog.info( '테스트 기간 자연 수익(사 놓고 존버): ', ((init_stock * anal_df.close[-1]) - init_cash_won)*100/ init_cash_won, '%')
    taLog.info( '*' * 20)

    CheckTrendGraph(anal_df)


if __name__ == "__main__":
    CODES = 1

    if CODES == 1:
        startTest()

    else:

        test_df = csvToDataFrame('신세계.KS.csv')
        #test_df = csvToDataFrame(u'삼성전자.KS.csv', path_from=1)
        #test_df = csvToDataFrame('GOOG.csv', path_from=1)
        # anal_df = test_df['20160111':'20160920']
        anal_df = test_df['20160111':'20161107']
        ma_ser = TA.MA(anal_df['close'], timeperiod=5)


        ma_arr = np.array(ma_ser)
        ma_filt = sci_sig.savgol_filter(ma_arr, 13, 2)
        ma_ser_filt = pd.Series(ma_filt, index=anal_df.index)

        ma_list = [ma_ser_filt, ma_ser]
        mark_list = getHighAndLowLineMA5(anal_df, ma_ser, len(anal_df), 1)

        getFullTrendByLine(anal_df, ma_ser, len(anal_df))

        checkChartPattern(anal_df, ma_ser, len(anal_df))

        low_line = np.where(mark_list == 2, 1, 0)
        high_line = np.where(mark_list == -2, 1, 0)

        marker_list = [high_line, low_line]

        plot_candles(anal_df, technicals=ma_list, marker=marker_list)
        # assets = {
        #     'asset_cash': [200000],
        #     'asset_stock': [0]
        # }
        #
        # for i in range(0, len(anal_df)):
        #     updateAsset(assets, anal_df[0:i], 0)
        #
        # print assets
        # print 'end'
        # #print isHighestPosIn6Month(anal_df)


