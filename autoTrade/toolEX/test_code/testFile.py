
# coding=utf-8

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import talib as TA




def csvToDataFrame(file_name):
    df = pd.read_csv(file_name)

    df.index = df['Date'].apply(pd.to_datetime) # Date 의 Dtype을 Datetime 형식으로 변경 해야함
    ch_df = df.loc[:, 'open':]
    ch_df.columns = ['open', 'high', 'low', 'close', 'adj_close', 'volume']
    ch_df = ch_df.tz_localize("UTC")

    return ch_df


test_df = csvToDataFrame('GOOG.csv')
stock = test_df['20160111':'20160920']
close = stock.close
ma_ser = TA.MA(close, timeperiod=5)
close = ma_ser

TEST = 3

if TEST == 1:
    f = open('test.txt', 'w')

    std_list = []
    for i in range(3, len(close)):
        price_str = ''
        for j in close[i-3:i]:
            # 숫자 Print 깔끔 정리
            price_str += '%10f' % j + ' '
            std_list.append(np.std(close[i-3:i]))
        strs = str(stock.index[i-1]) + ' ' + price_str + ' : ' + str(np.std(close[i-3:i])) + '\n'
        f.write(strs)

    f.close()
    print(min(std_list))
    print(max(std_list))
elif TEST == 2:
    ma_slop = ma_ser.diff()

    f = open('test1.txt', 'w')
    for idx in range(2, len(ma_slop)):

        if np.isnan(ma_slop[idx - 1]) == False:

            percent = (ma_slop[idx] - ma_slop[idx -1]) / ma_slop[idx - 1]
            strs = str(stock.index[idx]) + ' ' + str(ma_slop[idx]) + ' '+ str(percent)+'\n'
            f.write(strs)
    f.close()

elif TEST == 3:
    mean = ma_ser.mean()
    std = np.std(ma_ser)

    standard_ma = (ma_ser - mean)/std

    test_df = csvToDataFrame('GOOG.csv')
    stock = test_df['20160111':'20160920']
    close = stock.close
    ma_ser = TA.MA(close, timeperiod=5)

    nomal_goog = (ma_ser - ma_ser.min()) / (ma_ser.max() - ma_ser.min())

    test_df = csvToDataFrame('삼성전자.KS.csv')
    stock = test_df['20160111':'20160920']
    close = stock.close
    ma_ser = TA.MA(close, timeperiod=5)

    nomal_sanmsung = (ma_ser - ma_ser.min()) / (ma_ser.max() - ma_ser.min())


    plt.plot(nomal_goog.index, nomal_goog, nomal_sanmsung)
    plt.show()
