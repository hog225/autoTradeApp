# coding=utf-8
# 이 파일은 Custom Data Bundle을 만들데 사용하며 ~/.zipline/extension.py 파일을 대치한 것임
# window 에서 path C:\Users\epikh\.zipline
# 명령어로 데이터 생성 zipline ingest -b custom-csvdir-bundle
import pandas as pd

from zipline.data.bundles import register
from zipline.data.bundles.csvdir import csvdir_equities

start_session = pd.Timestamp('2009-12-31', tz='utc')
end_session = pd.Timestamp('2016-03-18', tz='utc')

register(
    'custom-csvdir-bundle',
    csvdir_equities(
        ['daily'],
        'X:/autoTradeApp/toolEX/csv_file',
    ),
    calendar_name='NYSE' # US equities
#    start_session=start_session,
#    end_session=end_session
)