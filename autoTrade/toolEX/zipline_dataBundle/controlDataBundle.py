import os
from pandas import Timestamp
from zipline.data.bundles import load
from zipline.data.data_portal import DataPortal
from zipline.utils.calendars import get_calendar

SYM = 'TSLA'

now = Timestamp.utcnow()
timed = Timestamp('2011-01-14', tz='UTC')
past_timed = Timestamp('2008-12-01', tz='UTC')
print(('now=', now))

# instantiate zipline data objects
bundle = load('custom-csvdir-bundle', os.environ)
calendar = get_calendar('NYSE')
dp = DataPortal(
    bundle.asset_finder,
    calendar,
    first_trading_day=bundle.equity_minute_bar_reader.first_trading_day,
    equity_minute_reader=bundle.equity_minute_bar_reader,
    equity_daily_reader=bundle.equity_daily_bar_reader,
    adjustment_reader=bundle.adjustment_reader
)

# get apple equity and timeseries
i = bundle.asset_finder.lookup_symbol(SYM, timed)



ts = dp.get_history_window(
    [i],
    timed,
    bar_count=150,
    frequency='1d',
    field='close',
    data_frequency='daily'
)

adj_val = dp.get_adjusted_value(
    [i],
    field='close',
    dt=timed,
    perspective_dt=timed,
    data_frequency='daily',
)

adj_ts = dp.get_adjustments(
    [i],
    field='close',
    dt=timed,
    perspective_dt = past_timed
)

# this should be populated
print(('asset_name=', i.asset_name))

# this shouldn't have NaNs
print(('timeseries=', adj_val))