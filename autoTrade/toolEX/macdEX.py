# coding=utf-8
from autoTradeApp.pandasLib import *


def fnMACD(m_Df, m_NumFast=12, m_NumSlow=26, m_NumSignal=9):
    m_Df['EMAFast'] = m_Df['price'].ewm( span = m_NumFast, min_periods = m_NumFast - 1).mean()
    m_Df['EMASlow'] = m_Df['price'].ewm( span = m_NumSlow, min_periods = m_NumSlow - 1).mean()
    m_Df['MACD'] = m_Df['EMAFast'] - m_Df['EMASlow']
    m_Df['MACDSignal'] = m_Df['MACD'].ewm( span = m_NumSignal, min_periods = m_NumSignal-1).mean()
    m_Df['MACDDiff'] = m_Df['MACD'] - m_Df['MACDSignal']
    return m_Df

def fnLiveMACD(p_Df, m_NumFast=12, m_NumSlow=26, m_NumSignal=9):
    #test_df = p_Df
    #test_df['EMAFast']= p_Df['price'].ewm(span=m_NumFast, min_periods=m_NumFast - 1).mean()
    #print test_df
    tmp_Df = pd.DataFrame(index = [p_Df.index[-1]], columns=['price', 'EMAFast', 'EMASlow', 'MACD'])
    tmp_Df['price'] = p_Df['price'][-1]
    tmp_Df['EMAFast'] = p_Df['price'].ewm(span=m_NumFast, min_periods=m_NumFast - 1).mean()[-1]
    tmp_Df['EMASlow'] = p_Df['price'].ewm(span=m_NumSlow, min_periods=m_NumSlow - 1).mean()[-1]
    tmp_Df['MACD'] = tmp_Df['EMAFast'] - tmp_Df['EMASlow']
    return tmp_Df


# 이렇게 구현하면 안됨
def MACDLive(m_time, csv_size):
    start = 0
    res_df = pd.DataFrame()
    while True:
        start += 1
        if start < m_time:
            continue

        if start >= csv_size:
            return res_df

        p_df = getCloseDF(pd.read_csv('apple.csv'), 0, start)
        #print p_df
        res_df = res_df.append(fnLiveMACD(p_df))

def testfnMACD(m_Df, m_NumFast=12, m_NumSlow=26, m_NumSignal=9):
    result = fnMACD(m_Df)
    print(result.info())
    return result

    #plt.plot(result.index, result.EMAFast, result.EMASlow, result.MACD)
    #plt.show()

def MACDBackTest(init_money, csv_file_name):
    macd_df = testfnMACD(getCloseDF(pd.read_csv(csv_file_name)))
    data_sign = np.sign(macd_df.MACD)
    macd_signal_df = pd.DataFrame()
    for i in range(data_sign.size - 1):
        if data_sign[i] != data_sign[i + 1] \
                and np.isnan(data_sign[i]) == False \
                and np.isnan(data_sign[i+1]) == False:
            macd_signal_df = macd_signal_df.append(macd_df.iloc[i])

    profit_df = macd_test_process(init_money, macd_df, macd_signal_df['MACD'])
    return profit_df

# 기본 MACD - > + 로 돌파시 매수
def macd_test_process(init_money, macd_result_df, MACD_Signal_df):
    index_list = macd_result_df.index.tolist()


    profit_df = pd.DataFrame([[init_money, '', 0, 0]],
                             index=[macd_result_df.index[0]],
                             columns=['balance', 'trade', 'price', 'amt'])

    for i in range(MACD_Signal_df.size):
        cur_macd = MACD_Signal_df[i]
        index = index_list.index(MACD_Signal_df.index[i])
        next_MACD = macd_result_df['MACD'][index+1]
        if np.sign(cur_macd) < np.sign(next_MACD):
            profit_df = order_buy(MACD_Signal_df.index[i], macd_result_df['price'][index], profit_df)
            #print 'buy : ', macd_result_df.index[index], '  ', cur_stock_price
        else:
            profit_df = order_sell(MACD_Signal_df.index[i], macd_result_df['price'][index], profit_df)
            #print 'sell : ', macd_result_df.index[index], '  ',macd_result_df['price'][index]

    return profit_df


if __name__=="__main__":
    # CSV 파일로 MACD 를 구하는 예제

    print(MACDBackTest(100, 'apple.csv'))





