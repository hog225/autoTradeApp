# coding=utf-8

from pandasLib import *
import os
#from log import *

# taLog = LogWrapper(TA_STRA)

def getKospiKosdaqPriceFromYahoo(stockCode = None):
    GET_ALL_KOSDAQ_SYM = 0 # 번들 만들려면 세팅 해라
    GET_SPECIFIC_KOSDAQ_SYM = 1
    CODE = GET_ALL_KOSDAQ_SYM


    if CODE == GET_ALL_KOSDAQ_SYM:
        GET_CODE = 0
        MAKE_BUNDLE = 0

        if GET_CODE == 1: # 웹에서 종목코드를 가져옴
            df =getStockCodeFromWeb(1)
            exit()


        stock_data_df = readKOSPICode_csv()
        stock_code_df = stock_data_df["종목코드"]
        stock_name_df = stock_data_df["회사명"]

        # 1. get csv from yahoo
        #SYMs = ['AAPL', 'TSLA', 'AMD', 'MU', 'NOK', 'KO', 'NVDA', 'MSFT', 'GOOG']


        SYMs = stock_code_df.tolist()
        for i, sym in enumerate(SYMs):
            if stockCode != None:
                if stock_code_df[i] == stockCode:
                    readStockFromWeb(sym, stock_name_df[i])
                    return

            else:
                readStockFromWeb(sym, stock_name_df[i])

        # 2. C:\Users\epikh\.zipline 에서 Extension.py 파일을 수정
        if MAKE_BUNDLE:
            os.system('zipline ingest -b custom-csvdir-bundle')

    elif CODE == GET_SPECIFIC_KOSDAQ_SYM:

        SYMs = ['088800.KQ']

        for i, sym in enumerate(SYMs):
            readStockFromWeb(sym)

def getStockCSVFromWeb(companyNameList):

    if type(companyNameList) != list:
        print('input list ')
        return

    stock_data_df = readKOSPICode_csv()
    stock_code_df = stock_data_df["종목코드"]
    stock_name_df = stock_data_df["회사명"]
    interest_SYMs = []
    company_names = []

    for companyName in companyNameList:
        try:
            idx = stock_name_df[stock_name_df==companyName].index[0]
            interest_SYMs.append(stock_code_df[idx])
            company_names.append(companyName)
        except:
            print('no company in kospi code')
            pass
    print(interest_SYMs)

    for i, sym in enumerate(interest_SYMs):

        readStockFromWeb(sym, company_names[i])

if __name__ == "__main__":
    #getStockCSVFromWeb([u'삼성전자'])

    getKospiKosdaqPriceFromYahoo('004170.KS')


