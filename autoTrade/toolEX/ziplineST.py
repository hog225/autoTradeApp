# coding=utf-8


import pandas as pd
import numpy as np
import datetime


from zipline.api import order, symbol, sid, record, order_target, get_environment, set_commission, commission, order_target_percent, get_order, set_symbol_lookup_date
from zipline.algorithm import TradingAlgorithm, log
from zipline.data.bundles import register
import talib as TA
# from autoTradeApp.pandasLib import *
from .taStrategy import *
from autoTradeApp.common_func import *





def getPercentIncVol(today_vol, vol_ma):
    return ((today_vol - vol_ma) / vol_ma) *100

def getPercentIncCandle(open, close):
    return ((close - open) / open) * 100

def getPriceCandlePosition(open, close, percent):
    return open + (((close - open) * percent) / 100)

def getGapPercentage(ma_long_val, today_price_val):
    return ((today_price_val - ma_long_val) / ma_long_val) * 100

def readQueue(q):
    return q.queue

def getDeviation(close_df):

    input = np.array(close_df)
    mean = np.mean(input)
    std = np.std(input)
    deviation_arr = np.subtract(input, mean)
    deviation = np.sum(deviation_arr)

    print(std, '   ', deviation)


def updateQueue(q, q_len, new_value):
    if q.qsize() < q_len:
        q.put(new_value)
    else:
        q.get_nowait()
        q.put(new_value)





def sto_strategy1(context, slowk, slowd, current_position, ma_long, df_price):
    last_slowk, slowk_val = slowk[-2:]
    last_slowd, slowd_val = slowd[-2:]
    if (slowk_val < context.rsi_buy or slowd_val < context.rsi_buy) and current_position <= 0:
        #if np.sign(last_slowd - last_slowk) == 1 and np.sign(slowd - slowk) <= 0:
        order_target_percent(context.asset, 0.9)
        print('Sto is at ' + str(df_price.index[-1]) + ', Buy ' + str(current_position) + ' shares')


    # If either the slowk or slowd are larger than 90, the stock is
    # 'overbought' and the position is closed.

    elif slowk_val > context.rsi_sell and current_position > 0:
        context.sell_flag = 1
        if df_price[-1] > ma_long + ma_long * context.diff:
            order_target(context.asset, 0)
            context.sell_flag = 0
            print('Sto is at ' + str(df_price.index[-1]) + ', MA Sell ' + str(current_position) + ' shares')


    elif (context.sell_flag == 1 and slowk_val <= context.rsi_sell) and current_position >0:
        order_target(context.asset, 0)
        context.sell_flag = 0
        print('Sto is at ' + str(df_price.index[-1]) + ', Sell ' + str(current_position) + ' shares')

def sto_strategy2(context, slowk, slowd, current_position, ma_long_val, OHLC_dat, ma_short_df):

    lastday_signal = np.sign(slowk[-2] - slowd[-2]) # -
    today_signal = np.sign(slowk[-1] - slowd[-1]) # 0, +
    df_price = OHLC_dat['close']

    ma_gap_percent = getGapPercentage(ma_long_val, df_price[-1])

    ma_slop_yesterday = linReg(ma_short_df[-4:-1].tolist())
    ma_slop_today = linReg(ma_short_df[-3:].tolist())

    trend = checkTrend(ma_short_df, df_price)
    down_signal = candlePatternSignal(OHLC_dat, trend)
    price_slop = linReg(df_price[-3:].tolist())
    if (lastday_signal < 0) and (today_signal >= 0) and (current_position <= 0) \
            and (slowk[-1] < 20): # Cross 전략 Buy slowk 가 slowd 를 아래서 위로 뚫을 때

    #if (ma_slop_yesterday < 0.0) and (ma_slop_today > 1.0)\
    #        and (current_position <= 0):

        order_target_percent(context.asset, 0.9)
        print('Sto is at ' + str(df_price.index[-1]) + ', Buy ' + str(current_position) + ' shares')


    # If either the slowk or slowd are larger than 90, the stock is
    # 'overbought' and the position is closed.

    # and (ma_short_df[-1] > ma_long_val) \ MA5 가 25 일 선보다 위에 있을때
    elif down_signal == DOWN_TREND and (current_position > 0) and (slowk[-1] > 50):

    #elif (slowk[-1] > 80) \
    #        and (current_position > 0)\
    #        and (ma_slop_today < 0.5):

        order_target(context.asset, 0)
        context.sell_flag = 0
        print('Sto is at ' + str(df_price.index[-1]) + ', Sell ' + str(current_position) + ' shares')
        print('slop ', price_slop)

def trend_strategy1(context, current_position, ma_long_val, ma_short_df, df_price, slop_list):

    today_ma_5_val = ma_short_df[-1]

    if len(slop_list) < 3:
        return

    # 매수
    if (slop_list[1] < slop_list[2]) and (slop_list[2] < 1) and (current_position <= 0) \
            and (ma_short_df[-1] < ma_long_val):
        print('Trend Strategy Price' + str(df_price.index[-1]) + ', BUY ' + str(current_position) + ' shares')
        print(slop_list)
        print(ma_long_val)
        print(ma_short_df[-1])
        print('-------------------------\n')
        order_target_percent(context.asset, 0.9)
    # 매도
    #
    elif (slop_list[1] > slop_list[2]) and (slop_list[2] < 1) and (current_position > 0) \
            and (ma_short_df[-1] > ma_long_val):
        print('Trend Strategy Price' + str(df_price.index[-1]) + ', Sell ' + str(current_position) + ' shares')
        print(slop_list)
        print(ma_long_val)
        print(ma_short_df[-1])
        print('-------------------------\n')
        order_target(context.asset, 0)

def targetBUY(context, buy_price):
    context.buy_price = buy_price
    order_target_percent(context.asset, 0.9)

def targetSELL(context):
    order_target(context.asset, 0)

def trend_strategy2(context, current_position, ma_short_df, df_price, OHLC_dat, slowk_val):
    return_state = checkTrend(ma_short_df)

    updateQueue(context.price_q, 3, return_state)
    trend_list = readQueue(context.price_q)



    # 횡보
    if (len(trend_list) >= 3) and ((trend_list[0] == trend_list[2]) and (trend_list[0] != trend_list[1])):

        if (current_position > 0) and (context.buy_price > df_price[-1]):
            print(df_price.index[-1], ' ', 'Sell NO Trend' + str(current_position) + ' shares')

            targetSELL(context)
        elif (current_position < 0): # 매수 금지
            pass

    # 매수
    elif return_state == UP_TREND:
        s_result = TA.CDLLONGLINE(OHLC_dat.open, OHLC_dat.high, OHLC_dat.low, OHLC_dat.close)
        # 상승 추세 전환시 매수
        if current_position <= 0:

            print(df_price.index[-1], ' ', 'TREND UP BUY ' + str(current_position) + ' shares', ' ', str(trend_list))
            targetBUY(context, df_price[-1])
        # 상승 추세에 장대 음봉이 발생 했을 시 매도
        elif (current_position > 0) and (s_result[-1] < 0) :
            print(df_price.index[-1], ' ', 'TREND UP SELL JANGDAE [-] BONG:' + str(current_position) + ' shares')

            targetSELL(context)

    # 하락 추세 전환 시 매도
    elif return_state == DOWN_TREND:
        if current_position > 0:
            print(df_price.index[-1], ' ', 'Sell ' + str(current_position) + ' shares', ' ', str(trend_list))

            targetSELL(context)
        elif current_position <= 0:
            yang_bong = checkYangBong(OHLC_dat)
            if yang_bong['isYang'][-3:].sum() == 3:
                print(df_price.index[-1], ' ', 'Yangbong BUY ' + str(current_position) + ' shares')

                targetBUY(context.asset, df_price[-1])







def zipline_stoch(context, data):
    # Skip first 300 days to get full windows
    record(stock_price=data.current(context.asset, 'price'))
    context.i += 1
    if context.i < context.n * 2:
        return


    df_price = data.history(context.asset, 'price', bar_count=context.n, frequency="1d")
    df_open_price = data.history(context.asset, 'open', bar_count=context.n, frequency="1d")
    df_high_price = data.history(context.asset, 'high', bar_count=context.n, frequency="1d")
    df_low_price = data.history(context.asset, 'low', bar_count=context.n, frequency="1d")

    OHLC_dat = makeOHLCDataFrame(df_price, df_open_price, df_high_price, df_low_price, 5)

    df_price_for_sto = data.history(context.asset, 'price', bar_count=context.n * 2, frequency="1d")
    df_price_short_ma = data.history(context.asset, 'price', bar_count=context.ma_short_pe + 10, frequency="1d")

    ma_long = data.history(context.asset, 'price', bar_count=context.ma_long_pe, frequency="1d").mean()
    ma_short = data.history(context.asset, 'price', bar_count=context.ma_short_pe, frequency="1d").mean()
    ma_vol = data.history(context.asset, 'volume', bar_count=context.vol_ma, frequency="1d").mean()

    highval = df_price_for_sto.rolling(window=context.n).max()[context.n:]
    lowval = df_price_for_sto.rolling(window=context.n).min()[context.n:]
    current_position = context.portfolio.positions[context.asset].amount
    slowk, slowd = TA.STOCH(highval, lowval, df_price,
                            fastk_period=5,
                            slowk_period=3,
                            slowk_matype=0,
                            slowd_period=3,
                            slowd_matype=0)

    last_slowk, slowk_val = slowk[-2:]
    last_slowd, slowd_val = slowd[-2:]

    df_short_ma = TA.MA(df_price_short_ma, timeperiod = context.ma_short_pe)

    df_short_ma = df_short_ma.dropna()
    short_slope = trend2(df_short_ma[-2:])
    short_slope = short_slope[0]
    updateQueue(context.slop_q, 3, short_slope)
    slop_list = readQueue(context.slop_q)

    #checkTrend(df_short_ma)
    #trend_strategy2(context, current_position, df_short_ma, df_price, OHLC_dat, slowk_val)
    #sto_strategy1(context, slowk, slowd, current_position, ma_long, df_price)

    sto_strategy2(context, slowk, slowd, current_position, ma_long, OHLC_dat, df_short_ma) # 이전략 발전

    #trend_strategy1(context, current_position, ma_long, df_short_ma, df_price, slop_list)

    # Save values for later inspection
    record(highval=highval[-1],
           lowval=lowval[-1],
           slowk = slowk_val,
           slowd = slowd_val,
           last_slowk=last_slowk,
           last_slowd=last_slowd,
           ma_long = ma_long,
           ma_short=ma_short,
           vol = data.current(context.asset, 'volume'),
           ma_vol = ma_vol,
           slop = short_slope)

def analyze_stoch(context, perf):

    print("last capital : ", perf.portfolio_value[-1], " 수익율 : ", ((perf.portfolio_value[-1] - perf.portfolio_value[0])/perf.portfolio_value[0]) * 100, '%')
    print("기간 시장 수익율 :   ", ((perf['stock_price'][-1] - perf['stock_price'][0]) / perf['stock_price'][0]) * 100, '%')


    fig = plt.figure()
    ax1 = fig.add_subplot(411)
    perf.portfolio_value.plot(ax=ax1)
    ax1.set_ylabel('portfolio value in $')
    ax1.grid()

    perf_trans = perf.ix[[t != [] for t in perf.transactions]]
    buys = perf_trans.ix[[t[0]['amount'] > 0 for t in perf_trans.transactions]]
    sells = perf_trans.ix[[t[0]['amount'] < 0 for t in perf_trans.transactions]]

    ax2 = fig.add_subplot(412)
    perf[['stock_price', 'ma_long', 'ma_short']].plot(ax=ax2)
    ax2.set_ylabel('price')

    ax2.plot(buys.index, perf['stock_price'].ix[buys.index],
             '^', markersize=5, color='m')
    ax2.plot(sells.index, perf['stock_price'].ix[sells.index],
             'v', markersize=5, color='k')

    ax3 = fig.add_subplot(413)
    perf[['slowk', 'slowd']].plot(ax=ax3)
    ax3.axhline(y=context.rsi_sell, ls='dashed', lw=0.2)
    ax3.axhline(y=context.rsi_buy, ls='dashed', lw=0.2)


    ax3.plot(buys.index, perf.slowk.ix[buys.index],
             '^', markersize=5, color='m')
    ax3.plot(sells.index, perf.slowk.ix[sells.index],
             'v', markersize=5, color='k')
    ax3.set_ylabel('price in $')

    ax4 = fig.add_subplot(414)
    ax4.bar(perf.index, perf['vol'], width=1, align='center')
    ax4.plot(perf.index, perf['ma_vol'], color='r')
    ax4.set_ylabel('vol')

    plt.legend(loc=0)
    plt.show()

##------------------------------------------VOLUME Strategy -----------------------

def getTrend(df_price, ma_length):
    if len(df_price) < ma_length:
        return False

    df_ma = TA.MA(df_price, timeperiod=ma_length)

    slop = (df_price[-1] - df_price[ma_length-1]) / float((len(df_price) - 1) - 5)

    print(slop)
    if slop <= 0:
        return -1 # 하락 추세
    elif 0 < slop < 0.5:
        return 1 # 횡보 추세
    elif slop >= 0.5:
        return 2 # 상승 추세

def zipline_volume(context, data):
    # Skip first 300 days to get full windows
    context.i += 1
    record(stock_price=data.current(context.asset, 'price'))
    if context.i < context.n * 2:
        return

    df_price = data.history(context.asset, 'price', bar_count=context.n, frequency="1d")
    df_open_price = data.history(context.asset, 'open', bar_count=context.n, frequency="1d")
    df_high_price = data.history(context.asset, 'high', bar_count=context.n, frequency="1d")
    df_low_price = data.history(context.asset, 'low', bar_count=context.n, frequency="1d")

    volumes = data.history(context.asset, 'volume', bar_count=context.vol_ma, frequency="1d")

    ma_long = data.history(context.asset, 'price', bar_count=context.ma_long_pe, frequency="1d").mean()
    ma_short = data.history(context.asset, 'price', bar_count=context.ma_short_pe, frequency="1d").mean()
    ma_vol = data.history(context.asset, 'volume', bar_count=context.vol_ma, frequency="1d").mean()
    current_vol = volumes[-1]

    per_increase = getPercentIncCandle(df_price[-2], df_price[-1])
    cur_per_candle = getPercentIncCandle(df_open_price[-1], df_price[-1])
    past_per_candle = getPercentIncCandle(df_open_price[-2], df_price[-2])

    #Jangdae_yangbong = TA.CDLLONGLINE(df_open_price, df_high_price, df_low_price, df_price)
    hammer = TA.CDLHAMMER(df_open_price, df_high_price, df_low_price, df_price)
    per_vol = getPercentIncVol(current_vol, ma_vol) # volume 이평선과 현제 volume 의 Percentage

    current_position = context.portfolio.positions[context.asset].amount

    if (hammer[-1] > 0) and current_position <= 0:
        order_target_percent(context.asset, 0.9)
        context.rocket_candle = True
        print('VOl Buy' + str(df_price.index[-1]) + ', Buy ' + str(current_position) + ' shares')


    #elif (context.rocket_candle == True) and current_position > 0:
    #    minimum_line = getPriceCandlePosition(df_open_price[-2], df_price[-2], 50)
    #    if minimum_line < df_price[-1]:
    #        print 'Sell Rocket down after Rocket UP'
    #        order_target(context.asset, 0)
    #    context.rocket_candle = False


    elif (cur_per_candle < 0) and current_position > 0:
        context.m_candle_cnt = context.m_candle_cnt + 1
        if context.m_candle_cnt == 3:
            order_target(context.asset, 0)
            print('Sell 4 days Minus Candle')
            context.m_candle_cnt = 0

            print('Vol Sell ' + str(df_price.index[-1]) + ', Sell ' + str(current_position) + ' shares')

    elif (cur_per_candle >= 0) and current_position > 0:
        if context.m_candle_cnt > 0:
            context.m_candle_cnt = context.m_candle_cnt - 1


    # Save values for later inspection
    record(ma_long = ma_long,
           ma_short=ma_short,
           vol = current_vol,
           ma_vol = ma_vol)



def analyze_volume(context, perf):

    print("last capital : ", perf.portfolio_value[-1], " 수익율 : ", ((perf.portfolio_value[-1] - perf.portfolio_value[0])/perf.portfolio_value[0]) * 100, '%')
    print("기간 시장 수익율 :   ", ((perf['stock_price'][-1] - perf['stock_price'][0]) / perf['stock_price'][0]) * 100, '%')

    fig = plt.figure()
    ax1 = fig.add_subplot(311)
    perf.portfolio_value.plot(ax=ax1)
    ax1.set_ylabel('portfolio value in $')
    ax1.grid()

    perf_trans = perf.ix[[t != [] for t in perf.transactions]]
    buys = perf_trans.ix[[t[0]['amount'] > 0 for t in perf_trans.transactions]]
    sells = perf_trans.ix[[t[0]['amount'] < 0 for t in perf_trans.transactions]]

    ax2 = fig.add_subplot(312)
    perf[['stock_price', 'ma_long', 'ma_short']].plot(ax=ax2)
    ax2.set_ylabel('price')

    ax2.plot(buys.index, perf['stock_price'].ix[buys.index],
             '^', markersize=5, color='m')
    ax2.plot(sells.index, perf['stock_price'].ix[sells.index],
             'v', markersize=5, color='k')

    ax3 = fig.add_subplot(313)
    ax3.bar(perf.index, perf['vol'], width=1, align='center')
    ax3.plot(perf.index, perf['ma_vol'], color='r')
    ax3.set_ylabel('vol')

    plt.legend(loc=0)
    plt.show()


if __name__ == "__main__":
    pass