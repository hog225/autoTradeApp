import logging
import os
import sys


L_END = "\033[0m"
L_BOLD= "\033[1m"
L_INVERSE = "\033[7m"
L_BLACK= "\033[30m"
L_RED = "\033[31m"
L_GREEN = "\033[32m"
L_YELLOW = "\033[33m"
L_BLUE = "\033[34m"
L_PURPLE = "\033[35m"
L_CYAN= "\033[36m"
L_WHITE= "\033[37m"
L_BGBLACK= "\033[40m"
L_BGRED= "\033[41m"
L_BGGREEN= "\033[42m"
L_BGYELLOW = "\033[43m"
L_BGBLUE= "\033[44m"
L_BGPURPLE = "\033[45m"
L_BGCYAN= "\033[46m"
L_BGWHITE= "\033[47m"



class LogWrapper():
    sep = ' '
    def __init__(self, log_name):

        self.logger = logging.getLogger(log_name)
        self.logger.setLevel(logging.DEBUG)


        self.logOption = [1, 1, 1, 1, 1, 1, 1]

        self.stream_hander = logging.StreamHandler()
        stream_formatter = logging.Formatter('%(name)s] %(message)s')

        self.stream_hander.setFormatter(stream_formatter)
        self.logger.addHandler(self.stream_hander)


        path = os.path.join(os.path.dirname(__file__), 'log')
        if not os.path.exists(path):
            os.mkdir(path)

        file_path = os.path.join(path, log_name + '.log')

        file_handler = logging.FileHandler(file_path)
        file_formatter = logging.Formatter('%(name)s] %(message)s')
        file_handler.setFormatter(file_formatter)
        self.logger.addHandler(file_handler)



    def info(self, *args):
        if self.logOption[0] == 0:
            return

        self.setLogColor(L_PURPLE)

        self.logger.info(self.sep.join("{}".format(a) for a in args))

    def debug(self, *args):
        if self.logOption[1] == 0:
            return

        self.setLogColor(L_YELLOW)

        self.logger.debug(self.sep.join("{}".format(a) for a in args))

    def warning(self, *args):
        if self.logOption[2] == 0:
            return

        self.setLogColor(L_WHITE)

        self.logger.warning(self.sep.join("{}".format(a) for a in args))

    def error(self, *args):
        if self.logOption[3] == 0:
            return

        self.setLogColor(L_RED)

        self.logger.error(self.sep.join("{}".format(a) for a in args))

    def critical(self, *args):
        if self.logOption[4] == 0:
            return
        self.setLogColor(L_GREEN)

        self.logger.critical(self.sep.join("{}".format(a) for a in args))

    def exception(self, *args):
        if self.logOption[5] == 0:
            return

        self.setLogColor(L_CYAN)

        self.logger.exception(self.sep.join("{}".format(a) for a in args))

    def log(self, *args):
        if self.logOption[6] == 0:
            return

        self.logger.log(self.sep.join("{}".format(a) for a in args))

    def setLogColor(self, col):
        stream_formatter = logging.Formatter(col + '%(name)s] %(message)s' + L_END)
        self.stream_hander.setFormatter(stream_formatter)

    def setLogOption(self, optionList):
        if len(optionList) != len(self.logOption):
            return 0

        self.logOption = optionList

TA_STRA = 'taStrategy'



if __name__=="__main__":

    genLog = LogWrapper('test')
    genLog.info('df')
    genLog.debug('df',34, [34,3,43,5,5])
    # genLog.debug('343434')
    # genLog.error('3435')
    # klog.info('fuck')