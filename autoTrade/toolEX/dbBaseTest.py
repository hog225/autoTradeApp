# coding=utf-8
import mysql.connector as mariadb
from mysql.connector import errorcode
import datetime
from autoTradeApp.common_func import *
import pandas as pd

DATABASE = 'chart_dat'
USER = 'yghong'
PASSWORD = 'ghddudrl'


def db_select(table,col,key):
    #print "%0.5f" % time.time()
    return_DB = []
    #sema_db.acquire()

    mariadb_connection = mariadb.connect(user=USER, password=PASSWORD, database=DATABASE)
    cursor = mariadb_connection.cursor()

    if type(key) == int:
        query = "SELECT * FROM " + table + " WHERE " + col + "=" + str(key)
    else:
        query = "SELECT * FROM "+table+" WHERE "+col+"="+'"'+key+'"'
    try:
        cursor.execute(query)
    except:
        print('Query Fail')
        pass

    for db_tuple in cursor:
        return_DB.append(db_tuple)

    mariadb_connection.close()
    #print "%0.5f" % time.time()
    #sema_db.release()

    return return_DB



def db_select_all_to_pd(table):
    # sema_db.acquire()
    df = 0
    mariadb_connection = mariadb.connect(user=USER, password=PASSWORD, database=DATABASE, host="192.168.0.18")

    query = "SELECT * FROM " + table

    try:
        df = pd.read_sql(query, mariadb_connection)
    except:
        mariadb_connection.close()
        return 0

    mariadb_connection.close()

    # sema_db.release()

    return df



def db_insert(table, col_name_tuple, value_tuple):

    insert_id = 0
    #sema_db.acquire()

    mariadb_connection = mariadb.connect(user=USER, password=PASSWORD, database=DATABASE)
    cursor = mariadb_connection.cursor()

    #insert_query = "INSERT INTO %s %s VALUES %s" % (table, str(col_name_tuple), str(value_tuple))
    #insert_query = "INSERT INTO %s (device_id, binding_method, create_date, modify_date) VALUES %s" \
    #               % (table , str(value_tuple))
    insert_query = "INSERT INTO " + table + " ("+ ','.join(col_name_tuple) +") " + "VALUES " + str(value_tuple)

    #print_query( insert_query)
    db_str  = ' DB Insert %s ' % table
    mlog(db_str, DBB)

    try:
        cursor.execute(insert_query)
        insert_id = cursor.lastrowid

        mariadb_connection.commit()

    except mariadb.Error as err:
            print((err.msg))
    else:
        pass

    mariadb_connection.close()
    #print "%0.5f" % time.time()
    #sema_db.release()

    return insert_id

def db_update_single(table, col_name,value,key,key_val):

    update_id = 0
    #sema_db.acquire()

    mariadb_connection = mariadb.connect(user=USER, password=PASSWORD, database=DATABASE)
    cursor = mariadb_connection.cursor()

    update_query = \
        "update " + table + " "\
        "SET "+ col_name+"="+get_db_string_by_type(value) + " " + \
        "WHERE " + str(key) +'='+get_db_string_by_type(key_val)

    #print_query( update_query)

    db_str  = ' DB Update Single %s ' % table
    mlog(db_str, DBB)

    try:
        cursor.execute(update_query)
        update_id = cursor.lastrowid

        mariadb_connection.commit()
    except mariadb.IntegrityError as e:
        if not e[0] == 1062:
            raise
        else:
            print("MY ERROR 1062: " + e[1])
        return False


    mariadb_connection.close()
    #print "%0.5f" % time.time()
    #sema_db.release()

    return update_id

def db_update(table, col_name_tuple,value_tuple,key,key_val):

    update_id = 0
    #sema_db.acquire()

    mariadb_connection = mariadb.connect(user=USER, password=PASSWORD, database=DATABASE)
    cursor = mariadb_connection.cursor()

    assignment_list = []
    for index, col_name in generator_list(col_name_tuple):
        assignment_list.append(col_name + '=' + get_db_string_by_type(value_tuple[index]))
    #print assignment_list

    update_query = \
        "update " + table + " "\
        "SET "+ ','.join(assignment_list) + " " + \
        "WHERE " + str(key) +'='+get_db_string_by_type(key_val)

   # print_query( update_query)
    db_str  = ' DB Update %s ' % table
    mlog(db_str, DBB)

    try:
        cursor.execute(update_query)
        update_id = cursor.lastrowid

        mariadb_connection.commit()
    except mariadb.IntegrityError as e:
        if not e[0] == 1062:
            raise
        else:
            print("MY ERROR 1062: " + e[1])
        return False


    mariadb_connection.close()
    #print "%0.5f" % time.time()
    #sema_db.release()

    return update_id



if __name__=="__main__":
    DB_NAME = 'btc_1M_CANDLE'
    #DB_NAME = 'btc_1M_CANDLE'
    df = db_select_all_to_pd(DB_NAME)
    df.to_clipboard()
    print(df.info())