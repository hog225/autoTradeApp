# coding=utf-8

import pandas_datareader.data as web
import pandas as pd
import numpy as np
import datetime
import matplotlib.pyplot as plt

from zipline.api import order, symbol, record, order_target, get_environment, set_commission, commission, order_target_percent, get_order
from zipline.algorithm import TradingAlgorithm, log
from zipline.utils.run_algo import run_algorithm
import talib


#-- zipline

def changeDFforziplineOrder(df, start=None, end=None):

    df.index = df['Date'].apply(pd.to_datetime) # Date 의 Dtype을 Datetime 형식으로 변경 해야함
    ch_df = df[['Adj Close']]
    ch_df.columns = ["AAPL"]
    ch_df = ch_df.tz_localize("UTC")
    print(ch_df[start:end].size)
    return ch_df[start:end]

def makeTS(date_str):
    """creates a Pandas DT object from a string"""
    return pd.Timestamp(date_str, tz='utc')


# 거래정보 출력
def getTradeInfo(result):
    for i in range(len(result.transactions.values)):
        if result.transactions.values[i] != []:
            print(result.transactions.values[i])

def simulation_zipline_result(algo_res):
    revenue = algo_res.portfolio_value
    plt.plot(algo_res.index, revenue)

# initialize the strategy
def macd_initialize(context):
    context.i = 0

    context.stock = symbol('AAPL')
    context.day_count = 50

    # Create a variable to track the date change

def macd_handle_data(context, data):
    context.i += 1
    if context.i < context.day_count:
        return

    # Set the new date
    # Load historical data for the stocks
    prices = data.history(context.stock, 'price', bar_count= context.day_count, frequency = '1d')
    # Create the MACD signal and pass in the three parameters: fast period, slow period, and the signal.
    # This is a series that is indexed by sids.
    #macd = prices.apply(MACD, fastperiod=12, slowperiod=26, signalperiod=9)
    macd, signal, hist = MACD(prices, fastperiod=12, slowperiod=26, signalperiod=9) # 실제론 macd 차가 리턴됨
    transaction_signal = macd - signal
    # Iterate over the list of stocks

    current_position = context.portfolio.positions[context.stock].amount

    # MACD 매매 신호
    # MACD 가 양으로 증가하면 매수
    # MACD 가 MACDSignal을 골드크로스(아래서 위로 뚫을때) 하면 매수
    # Close position for the stock when the MACD signal is negative and we own shares.
    # sell
    if transaction_signal < 0 and current_position > 0:
        order_target(context.stock, 0)

        #log.info('MACD is at ' + str(macd) + ', Sell ' + str(current_position) + ' shares')
        print('MACD is at ' + str(macd) + ', Sell ' + str(current_position) + ' shares')
    # Enter the position for the stock when the MACD signal is positive and
    # our portfolio shares are 0.
    # buy
    elif transaction_signal > 0 and current_position == 0:
        o = order_target_percent(context.stock, 1)
        #log.info('MACD is at ' + str(macd) + ', Buy ' + str(get_order(o).amount) + ' shares')
        print('MACD is at ' + str(macd) + ', Buy ' + str(get_order(o).amount) + ' shares')

    record(AAPL=data.current(context.stock, 'price'),
           MACD=macd,
           MACDSignal = signal,
           MACDHist = hist)


# Define the MACD function
def MACD(prices, fastperiod=12, slowperiod=26, signalperiod=9):

    macd, signal, hist = talib.MACD(prices,
                                    fastperiod=fastperiod,
                                    slowperiod=slowperiod,
                                    signalperiod=signalperiod)
    return macd[-1], signal[-1], hist[-1]


def macd_analyze(context, perf):
    fig = plt.figure()
    ax1 = fig.add_subplot(311)
    perf.portfolio_value.plot(ax=ax1)
    ax1.set_ylabel('portfolio value in $')

    perf_trans = perf.ix[[t != [] for t in perf.transactions]]
    buys = perf_trans.ix[[t[0]['amount'] > 0 for t in perf_trans.transactions]]
    sells = perf_trans.ix[[t[0]['amount'] < 0 for t in perf_trans.transactions]]

    ax2 = fig.add_subplot(312)
    perf['AAPL'].plot(ax=ax2)
    ax2.plot(buys.index, perf.AAPL.ix[buys.index], '^', markersize=5, color='m')
    ax2.plot(sells.index, perf.AAPL.ix[sells.index], 'v', markersize=5, color='k')
    ax2.set_ylabel('price')

    ax3 = fig.add_subplot(313)
    perf[['MACD', 'MACDSignal']].plot(ax=ax3)
    ax3.axhline(y = 0, ls='dashed', lw=0.8)
    ax3.plot(buys.index, perf.MACD.ix[buys.index], '^', markersize=5, color='m')
    ax3.plot(sells.index, perf.MACD.ix[sells.index], 'v', markersize=5, color='k')
    ax3.set_ylabel('price in $')
    plt.legend(loc=0)
    plt.show()

if __name__=="__main__":
    #pd_objectCreation()
    #draw_gragh(read_apple_stock()) # Yahoo 를 이용해 주식 데이터를 가져온 뒤 그래프 그리기
    #draw_gragh_from_csv('apple.csv')
    #read_apple_stock()


    #Basic zipline
    algo = TradingAlgorithm(initialize=macd_initialize, handle_data=macd_handle_data, analyze=macd_analyze)
    test_df = changeDFforziplineOrder(pd.read_csv('apple.csv'), 60, 815)
    result = algo.run(test_df)
    print("last capital : ", result.portfolio_value[-1], " 수익율 : ", ((result.portfolio_value[-1] - result.portfolio_value[0])/result.portfolio_value[0]) * 100, '%')
    print("기간 시장 수익율 :   ", ((test_df.AAPL[-1] - test_df.AAPL[0]) / test_df.AAPL[0]) * 100, '%')


    # DMA Strategy

    #algo = TradingAlgorithm(initialize=initialize, handle_data=handle_data, analyze=analyze)

    '''
    # zipline interm
    algo = TradingAlgorithm(start=makeTS("2011-11-01"), end=makeTS("2011-11-20"), initialize=initialize, handle_data=handle_data, data_frequency='minute')
    df = changeDFforziplineOrder(pd.read_csv('apple.csv'))
    result = algo.run(df)
    getTradeInfo(result)
    run_algorithm()
    print result.portfolio_value[0]
    '''





