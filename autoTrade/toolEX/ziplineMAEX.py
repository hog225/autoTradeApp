# coding=utf-8


import pandas as pd
import numpy as np
import datetime
import matplotlib.pyplot as plt

from zipline.api import order, symbol, sid, record, order_target, get_environment, set_commission, commission, order_target_percent, get_order, set_symbol_lookup_date
from zipline.algorithm import TradingAlgorithm, log
from zipline.data.bundles import register
import talib
import os


#-- zipline

def changeDFforziplineOrder(df, start=None, end=None):

    df.index = df['Date'].apply(pd.to_datetime) # Date 의 Dtype을 Datetime 형식으로 변경 해야함
    ch_df = df.loc[:, 'Open':]
    ch_df.columns = ['open', 'high', 'low', 'close', 'AAPL', 'volume']
    ch_df = ch_df.tz_localize("UTC")
    print(ch_df[start:end].size)
    return ch_df[start:end]

def makeTS(date_str):
    """creates a Pandas DT object from a string"""
    return pd.Timestamp(date_str, tz='utc')

# ------------------DMA ----------------


def initialize(context):
    context.i = 0
    context.asset = symbol('AAPL')
    # context.volume = symbol('volume') # bundle 데이터 사용 테스트 때문에 막음
    print(get_environment('arena'))
    print(get_environment('data_frequency'))

def fnMA(df_data):

    short_mavg = talib.MA(df_data, timeperiod=20)
    long_mavg = talib.MA(df_data, timeperiod=50)

    return short_mavg[-2], short_mavg[-1], long_mavg[-2], long_mavg[-1]

def handle_data(context, data):
    # Skip first 300 days to get full windows
    context.i += 1
    if context.i < 50:
        return

    df_price = data.history(context.asset, 'price', bar_count=50, frequency="1d")
    df_openPrice = data.history(context.asset, 'open', bar_count=50, frequency="1d")

    prev_short_mavg, short_mavg, prev_long_mavg, long_mavg = fnMA(df_price)
    difference = np.sign(short_mavg - long_mavg) - np.sign(prev_short_mavg - prev_long_mavg)

    #if short_mavg > long_mavg:
    if difference != 0 and np.sign(short_mavg - long_mavg) > 0:
        # order_target orders as many shares as needed to
        # achieve the desired number of shares.
        order_target(context.asset, 100)


    elif short_mavg - long_mavg > 20:
        order_target(context.asset, 0)

    # Save values for later inspection
    record(AAPL=data.current(context.asset, 'price'),
           short_mavg=short_mavg,
           long_mavg=long_mavg,
           difference = difference,
           open=data.current(context.asset, 'open'))


def analyze(context, perf):
    fig = plt.figure()
    ax1 = fig.add_subplot(211)
    perf.portfolio_value.plot(ax=ax1)
    ax1.set_ylabel('portfolio value in $')

    ax2 = fig.add_subplot(212)
    perf['AAPL'].plot(ax=ax2)
    perf[['short_mavg', 'long_mavg']].plot(ax=ax2)

    perf_trans = perf.ix[[t != [] for t in perf.transactions]]
    buys = perf_trans.ix[[t[0]['amount'] > 0 for t in perf_trans.transactions]]
    sells = perf_trans.ix[
        [t[0]['amount'] < 0 for t in perf_trans.transactions]]
    ax2.plot(buys.index, perf.short_mavg.ix[buys.index],
             '^', markersize=5, color='m')
    ax2.plot(sells.index, perf.short_mavg.ix[sells.index],
             'v', markersize=5, color='k')
    ax2.set_ylabel('price in $')

    # bundle 데이터 사용 테스트 때문에 막음
    #ax3 = fig.add_subplot(313)
    #ax3.bar(perf.index, perf['vol'], width=1, align='center')
    #ax3.set_ylabel('vol')
    plt.legend(loc=0)
    plt.show()

# ------------------DMA ----------------


if __name__ == "__main__":
    os.system("zipline run -f ziplineMAEX.py --start 2010-1-1 --end 2018-1-1 -o pickle_file/ma_apple.pickle")

    #Basic zipline
    '''
    algo = TradingAlgorithm(initialize=initialize, handle_data=handle_data)
    #algo = TradingAlgorithm(initialize=initialize, handle_data=handle_data, analyze=analyze)
    test_df = changeDFforziplineOrder(pd.read_csv('apple.csv'))
    result = algo.run(test_df)
    print "last capital : ", result.portfolio_value[-1]
    '''



