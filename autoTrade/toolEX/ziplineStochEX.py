# coding=utf-8
import os
import sys
import queue
sys.path.insert(0, os.path.dirname(os.path.abspath("ziplineStochEX.py")))
from .ziplineST import *

D_BUNDLE = 'GOOG'
#-- zipline


def makeTS(date_str):
    """creates a Pandas DT object from a string"""
    return pd.Timestamp(date_str, tz='utc')

def testTalibStoch(test_df, n):
    high_df = test_df[D_BUNDLE].rolling(window=n).max()
    low_df = test_df[D_BUNDLE].rolling(window=n).min()
    slowk, slowd = TA.STOCH(high_df, low_df, test_df[D_BUNDLE], fastk_period=5, slowk_period=3, slowk_matype=0,
                               slowd_period=3, slowd_matype=0)
    return slowk, slowd

# ------------------DMA ----------------


def initialize(context):
    context.i = 0

    context.n = 30 # 스토케스틱 Fast K가 증가하면 늘려 줘야 함
    context.rsi_sell = 85
    context.rsi_buy = 25

    context.sell_flag = 0
    context.diff = 0.1

    context.ma_long_pe = 25
    context.ma_short_pe = 5


    context.vol_ma = 20
    context.vn = 20
    context.m_candle_cnt = 0
    context.rocket_candle = 0

    context.slop_q = queue.Queue()
    context.price_q = queue.Queue()
    context.buy_price = 0.0
    context.asset = symbol(D_BUNDLE)
    # context.volume = symbol('volume')
    # context.openP = symbol('open')


def handle_data(context, data):
    zipline_stoch(context, data)
    #zipline_volume(context, data)


def analyze(context, perf):
    analyze_stoch(context, perf)
    #analyze_volume(context, perf)
# ------------------stoch ----------------


if __name__ == "__main__":
    # Quantopian 사이트에서 Fetcher - Load any CSV file 로 검색해서
    # Basic zipline

    path_str = os.path.join(os.path.dirname(os.path.abspath(__file__)), __file__)
    data_bundle_name = 'custom-csvdir-bundle'
    start_se = '2017-04-01'
    end_se = '2017-10-20'

    print('Sotck to anal = ',D_BUNDLE)

    #command_str = "zipline run -f %s -b quandl --start 2015-10-1 --end 2018-1-30 -o pickle_file/%s_stoc.pickle" % (path_str, D_BUNDLE)
    # zipline run -f X:\autoTradeApp\toolEX\ziplineStochEX.py -b custom-csvdir-bundle
    command_str = "zipline run -f %s " \
                  "-b %s " \
                  "--start %s --end %s " \
                  "-o pickle_file/%s_stoc.pickle" % \
                  (path_str, data_bundle_name, start_se, end_se, D_BUNDLE)
    os.system(command_str)



    exit()
    # data bundle 이용 안할 시

    S_SYM = 'AAPL'
    if not findFileOndir(S_SYM + '.csv'):
        print('download csv from web ')
        readStockFromWeb(S_SYM) # csv 파일 만들어 주는 함수


    algo = TradingAlgorithm(initialize=initialize, handle_data=handle_data, analyze=analyze)
    test_df = changeDFforziplineOrder(pd.read_csv(S_SYM + '.csv'))
    # test_df = changeDFforziplineOrder(pd.read_csv('TSLA.csv'))
    #test_df.index.get_loc('20120904')
    result = algo.run(test_df)
    print("last capital : ", result.portfolio_value[-1], " 수익율 : ", ((result.portfolio_value[-1] - result.portfolio_value[0])/result.portfolio_value[0]) * 100, '%')
    print("기간 시장 수익율 :   ", ((test_df.AAPL[-1] - test_df.AAPL[0]) / test_df.AAPL[0]) * 100, '%')




