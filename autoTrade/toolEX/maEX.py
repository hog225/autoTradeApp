from autoTradeApp.pandasLib import *

P_DF_PERIOD = 2

def fnMA(csv_file_name):
    df = getCloseDF(pd.read_csv(csv_file_name))
    df = df[0:10]
    #df_serise = df['price']


    trend_series = pd.Series(index=df.index, data=np.NaN)
    slop_series = pd.Series(index=df.index, data=np.NaN)

    df['ma5'] = TA.MA(df.price, timeperiod= 5)
    df_serise = df['ma5']

    for i in range(len(df_serise)):

        if i < P_DF_PERIOD:
            continue

        part_df = df_serise[(i - P_DF_PERIOD):i]
        slop = trend2(part_df)
        coe_list = [slop[0] * x + slop[1] for x in range(len(part_df))]
        slop_series[i] = slop[0]
        trend_series[i] = coe_list[-1]
    #coe_series = pd.Series(index=df.index, data=(v[0] for v in coe_list))
    df['trend'] = trend_series
    df['slop'] = slop_series


    #df['ma15'] = ta.MA(df.price, timeperiod=15)
    #df['ma300'] = ta.MA(df.price, timeperiod=300)
    return df

def show_plt(df):
    df[['price', 'ma5', 'trend']].plot()
    #df[['trend']].plot()
    plt.grid(alpha=1)
    plt.show()



if __name__=="__main__":
    apl_df = fnMA("apple.csv")
    print(apl_df)
    show_plt(apl_df)
