# coding=utf-8
from autoTradeApp.pandasLib import *

PERIOD_FASTK = 5
PERIOD_SLOWK = 3

PERIOD_SLOWD = 7

RSI_SELL = 85
RSI_BUY = 25

SLOW_K = 'Slow_K'
SLOW_D = 'Slow_D'

PRICE = 'close'
AMT = 'amount'
TRANS = 'transactions'



def show_plt(perf):
    fig = plt.figure()
    ax1 = fig.add_subplot(311)
    perf.portfolio_value.plot(ax=ax1)
    ax1.set_ylabel('portfolio value in $')
    ax1.grid()


    buys = perf.ix[[t > 0 for t in perf.transactions]]
    sells = perf.ix[[t < 0 for t in perf.transactions]]

    ax2 = fig.add_subplot(312)
    perf[['close']].plot(ax=ax2)
    ax2.set_ylabel('price')

    # CSV 파일이 Date 가 UTC 로 안되어 있는 상태에서 DF로 읽어서 아래 루틴 적용되면 에러 발생함
    # TypeError: float() argument must be a string or a number
    ax2.plot(buys.index, perf[PRICE].ix[buys.index],
             '^', markersize=5, color='m')
    ax2.plot(sells.index, perf[PRICE].ix[sells.index],
             'v', markersize=5, color='k')

    ax3 = fig.add_subplot(313)
    perf[[SLOW_K, SLOW_D]].plot(ax=ax3)
    ax3.axhline(y=RSI_SELL, ls='dashed', lw=0.2)
    ax3.axhline(y=RSI_BUY, ls='dashed', lw=0.2)

    #plt.legend(loc=0)
    plt.show()

# price: 종가(시간 오름차순), n: 기간 ,지우진 말기
def fnStoch(m_Df, n=14):
    sz = len(m_Df['close'])
    if sz < n:
        # show error message
        raise SystemExit('입력값이 기간보다 작음')
    tempSto_K=[]
    for i in range( sz):
        if i >= n-1:
            tempUp =m_Df['close'][i] - min(m_Df['low'][i-n+1:i+1])
            tempDown = max(m_Df['High'][i-n+1:i+1]) -  min(m_Df['low'][i-n+1:i+1])
            tempSto_K.append( tempUp / tempDown )
        else:
            tempSto_K.append(0) #n보다 작은 초기값은 0 설정
    m_Df['Sto_K'] = pd.Series(tempSto_K,  index=m_Df.index)

    m_Df['Sto_D'] = pd.Series(pd.rolling_mean(m_Df['Sto_K'] , 3))
    m_Df['Sto_SlowD'] = pd.Series(pd.rolling_mean(m_Df['Sto_D'], 3))

    return m_Df

def TAfnStoch(m_Df):
    ser_high_val = m_Df['high']
    ser_low_val = m_Df['low']
    ser_price_val = m_Df['close']
    slow_K, slow_D = TA.STOCH(ser_high_val, ser_low_val, ser_price_val,
                            fastk_period=PERIOD_FASTK,
                            slowk_period=PERIOD_SLOWK,
                            slowk_matype=0,
                            slowd_period=PERIOD_SLOWD,
                            slowd_matype=0)
    m_Df['Slow_K'] = pd.Series(slow_K, index=m_Df.index)
    m_Df['Slow_D'] = pd.Series(slow_D, index=m_Df.index)
    return m_Df

# Cross Signal 생성 ma cross, stoch cross 등등
# 인자로 두개의 cross 파라미터를 받아야 함
def getCrossSignal(df, strategy_func):
    res_df = strategy_func(df)
    signals = pd.DataFrame(index= df.index)
    signals['signal'] = 0.0

    signals['signal'][PERIOD_SLOWK:] = np.where(res_df['Slow_K'][PERIOD_SLOWK:] > res_df['Slow_D'][PERIOD_SLOWK:], 1.0, 0.0)
    signals['transactions'] = signals['signal'].diff()

    result_df = pd.concat([res_df, signals], axis=1) # append 를 써도 됨
    return result_df

def order_buy(index, price, pro_df):
    cur_stock_price = price
    prev_balance = pro_df['balance'][-1]
    prev_stock_amt = pro_df['amt'][-1]

    can_buy_stock_amt = prev_balance // cur_stock_price
    cur_stock_amt = int(can_buy_stock_amt) + prev_stock_amt
    cur_balance = prev_balance - can_buy_stock_amt * cur_stock_price
    cur_trade = 'buy'
    add_df = pd.DataFrame([[cur_balance, cur_trade, cur_stock_price, cur_stock_amt]]
                          , index=[index]
                          , columns=['balance', 'trade', 'price', 'amt'])
    pro_df = pro_df.append(add_df)
    return pro_df

def order_sell(index, price, pro_df):
    cur_stock_price = price
    prev_balance = pro_df['balance'][-1]
    prev_stock_amt = pro_df['amt'][-1]

    can_sell_stock_amt = prev_stock_amt
    cur_stock_amt = prev_stock_amt - can_sell_stock_amt
    cur_balance = prev_balance + can_sell_stock_amt * cur_stock_price
    cur_trade = 'sell'
    add_df = pd.DataFrame([[cur_balance, cur_trade, cur_stock_price, cur_stock_amt]]
                          , index=[index]
                          , columns=['balance', 'trade', 'price', 'amt'])
    pro_df = pro_df.append(add_df)
    return pro_df


def backTestWithPosition(df, initial_capital, trade_method =1):
    # strategy 를 통해서 거래 내용이 담긴 DataFrame 을 가져옴
    result_df = 0
    if trade_method == 1:
        result_df = getCrossSignal(df, TAfnStoch)

    result_df['amount'] = 0.0
    result_df['portfolio_value'] = 0.0
    result_df['portfolio_value'][0] = initial_capital

    for idx in range(len(result_df)):
        if idx == 0:
            continue

        # buy process
        if result_df.loc[result_df.index[idx]].transactions > 0:
            prev_portfolio = result_df['portfolio_value'][idx - 1]
            prev_amount = result_df['amount'][idx - 1]

            can_buy_amount = prev_portfolio / result_df['close'][idx]

            cur_amount = can_buy_amount + prev_amount
            cur_portfolio = prev_portfolio - can_buy_amount * result_df['close'][idx]

            result_df['portfolio_value'][idx] = cur_portfolio
            result_df['amount'][idx] = cur_amount

        elif result_df.loc[result_df.index[idx]].transactions < 0:
            prev_portfolio = result_df['portfolio_value'][idx - 1]
            prev_amount = result_df['amount'][idx - 1]

            can_sell_amount = prev_amount

            cur_amount =  prev_amount - can_sell_amount
            cur_portfolio = prev_portfolio + can_sell_amount * result_df['close'][idx]

            result_df['portfolio_value'][idx] = cur_portfolio
            result_df['amount'][idx] = cur_amount

        else:
            result_df['portfolio_value'][idx] = result_df['portfolio_value'][idx-1]
            result_df['amount'][idx] = result_df['amount'][idx - 1]


    return result_df

def analResult(perf):
    print("last capital : ", perf.portfolio_value[-1], " 수익율 : ", ((perf.portfolio_value[-1] - perf.portfolio_value[0])/perf.portfolio_value[0]) * 100, '%')
    print("기간 시장 수익율 :   ", ((perf['close'][-1] - perf['close'][0]) / perf['close'][0]) * 100, '%')



if __name__ =="__main__":
    # 아래 CSV 파일은 db_lib.py 에서 db_to_csv('btc_1M_CANDLE') 로 얻을 수 있다 .

    file_name = 'btc_1M_CANDLE.csv'
    file_path = os.path.join(os.getcwd(), 'csv_file', 'minute', file_name)

    df = getDateIndexDF(pd.read_csv(file_path))

    # 백 테스트
    result_df = backTestWithPosition(df, 10000000)


    DfToPickle(result_df, 'btc_1M_CANDLE')
    print(result_df.info())

    analResult(result_df)
    #result_df = pd.DataFrame
    #result_df['difference'] = slow_k - df['Sto_SlowD']
    #result_df['gold_cross'] = np.sign(result_df.difference.shift(1)) > np.sign(result_df.difference)
    #result_df['dead_cross'] = np.sign(result_df.difference.shift(1)) < np.sign(result_df.difference)
    #result_df['transaction'] = np.where(result_df['gold_cross'] == True , 'buy', 'Not' )
    #result_df['transaction'] = np.where(result_df['dead_cross'] == True, 'sell', result_df['transaction'])
    show_plt(result_df)