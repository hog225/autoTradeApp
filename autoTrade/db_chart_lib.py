# -*- coding: utf-8 -*-

from .db_lib import *



def create_coin_chart(currency):
    #check_db_name = currency+'_ticker'

    mariadb_connection = mariadb.connect(user=USER, password=PASSWORD, database=DATABASE)
    cursor = mariadb_connection.cursor()

    ask_name_db = currency+'_ask'
    bid_name_db = currency+'_bid'
    order_book_name_list = [ask_name_db, bid_name_db]
    tick_name_db = currency+'_ticker'
    cnadle_db_name_list = []
    for candle_db_name in CANDLE_LIST:
        cnadle_db_name_list.append(currency+'_'+candle_db_name)
    db_tables = {}

    for order_book_name in order_book_name_list:
        db_tables[order_book_name] = (
            "CREATE TABLE `%s` ("
            " `id` INT NOT NULL AUTO_INCREMENT,"
            " `ts` DATETIME,"
            " `price1` FLOAT,"
            " `qty1` FLOAT,"
            " `price2` FLOAT,"
            " `qty2` FLOAT,"
            " `price3` FLOAT,"
            " `qty3` FLOAT,"
            " `price4` FLOAT,"
            " `qty4` FLOAT,"
            " `price5` FLOAT,"
            " `qty5` FLOAT,"
            " PRIMARY KEY (`id`)"
            ")" % order_book_name
        )

    db_tables[tick_name_db] = (
        "CREATE TABLE `%s` ("
        " `id` INT NOT NULL AUTO_INCREMENT,"
        " `ts` DATETIME,"
        " `currency` CHAR(5),"
        " `high` FLOAT,"
        " `low` FLOAT,"
        " `last` FLOAT,"
        " `volume` FLOAT,"
        " PRIMARY KEY (`id`)"
        ")" % tick_name_db
    )

    for candle_db in cnadle_db_name_list:
        db_tables[candle_db] = (
        "CREATE TABLE `%s` ("
        " `id` INT NOT NULL AUTO_INCREMENT,"
        " `ts` DATETIME,"
        " `currency` CHAR(5),"
        " `open` FLOAT,"
        " `high` FLOAT,"
        " `low` FLOAT,"
        " `close` FLOAT,"
        " `adjclose` FLOAT,"
        " `volume` FLOAT,"
        " PRIMARY KEY (`id`)"
        ")" % candle_db
    )


    for name, ddl in db_tables.items():
        try:
            print("Creating table {}: ".format(name), end='')
            cursor.execute(ddl)
        except mariadb.Error as err:
            if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                print("already exists.")
            else:
                print(err.msg)
        else:
            print("OK")


    mariadb_connection.close()

def delete_coin_chart(currency):
    mariadb_connection = mariadb.connect(user=USER, password=PASSWORD, database=DATABASE)
    cursor = mariadb_connection.cursor()

    ask_name_db = currency+'_ask'
    bid_name_db = currency+'_bid'
    tick_name_db = currency+'_ticker'
    cnadle_db_name_list = []
    for candle_db_name in CANDLE_LIST:
        cnadle_db_name_list.append(currency+'_'+candle_db_name)
    chart_db_name_list = [ask_name_db, bid_name_db, tick_name_db] + cnadle_db_name_list

    for db_name in chart_db_name_list:
        try:
            db_str = 'DROP TABLE %s' % db_name
            print("Deleting table {}: ".format(db_name), end='')
            cursor.execute(db_str)
        except mariadb.Error as err:
            if err.errno == errorcode.ER_BAD_TABLE_ERROR:
                print ("No table exist. ")
            else:
                print (err.msg)
        else:
            print ('OK')

def insertTicker(json_str):
    if type(json_str) != dict:
        clog('invalid data type',DBB)
        return

    if ('result' in json_str) != True:
        clog('No key Result', DBB);
        return

    if json_str["result"] == "success":
        int_ts = int(json_str["timestamp"])
        date_str = datetime.fromtimestamp(int_ts )
        times = '%s' % date_str

        currency = str(json_str['currency'])
        high = float(json_str['high'])
        low = float(json_str['low'])
        last = float(json_str['last'])
        volume = float(json_str['volume'])

        table_name = currency+'_ticker'
        db_insert(table_name, TICKER_TUP, (times, currency, high, low, last, volume))

    else:
        clog("json result is not success ", DBB)
        return

def insertChartMonList(currency):

    ts_int = time.time()
    date_str = datetime.fromtimestamp(ts_int)
    ts = '%s' % date_str

    db_insert(CURRENCY_MON_LIST, ('ts', 'mon_currency'), (ts, currency))

def selectChartMonList():
    return_list = db_select_all(CURRENCY_MON_LIST)
    return return_list



def candle_write(currency, count):
    tmp_table = []
    ticker_db_name = currency+'_ticker'
    data = select_for_candle(ticker_db_name, count)
    if data == []:
        db_str = 'No db found for %s ' % currency
        clog(db_str, DBB)
    #print data
    last = data[0][5]
    volume = data[0][6]
    for i in data:
        tmp_table.append(i[5])
    tmp_table.sort()
    l_pri = tmp_table[0]
    h_pri = tmp_table[-1]

    m1_candle_name = currency+'_'+CANDLE_LIST[0]
    last_candle = select_for_candle(m1_candle_name, 1)
    if last_candle != []:
        p_price = last_candle[0][6] # open 가격
    else:
        p_price = last

    ts_int = time.time()
    date_str = datetime.fromtimestamp(ts_int)
    ts = '%s' % date_str


    #print tmp_table
    #print 'count = %d l_pri = %s, h_pri = %s, last = %s prev_pri = %s ' % (count,l_pri, h_pri, last, p_price)
    if len(tmp_table) != count:
        strs = '1minute candle mismatch table and count '
        clog(strs, DBB)

    #check data validation
    '''
    tmp_time_table = []
    for j in data:
        tmp_time_table.append(j[1])
    if check_data_validation(tmp_time_table, 10) == False:
        strs = '1minute candle data time mismatch '
        mlog(strs)
    '''

    res = db_insert(m1_candle_name, CANDLE_LOW_TUP, (ts, currency, p_price, h_pri, l_pri, last, last, volume))
    if res > 0:
        tstr = 'insert 1minute candle success '
        clog(tstr, DBB)

    else:
        tstr = 'insert 1minute candle fail '
        clog(tstr, DBB)

def candle_write_using_candle_db(currency, using_table_candle,write_table_candle, count, time_interval):
    tmp_data = []
    data = select_for_candle(using_table_candle,count)
    if data == []:
        db_str = 'No candle db found for %s ' % currency
        clog(db_str, DBB)

    last_price = data[0][6]
    volume = data[0][8]


    lprice_table = []
    hprice_table = []
    for price in data:
        lprice_table.append(price[5])
        hprice_table.append(price[4])
    lprice_table.sort()
    hprice_table.sort()
    lowest_price = lprice_table[0]
    highest_price = hprice_table[-1]

    last_candle = select_for_candle(write_table_candle,1)
    if last_candle != []:
        p_price = last_candle[0][6]
    else:
        p_price = last_price
    ts_int = time.time()
    date_str = datetime.fromtimestamp(ts_int)
    ts = '%s' % date_str

    res = db_insert(write_table_candle, CANDLE_LOW_TUP, (ts, currency, p_price, highest_price, lowest_price, last_price, last_price, volume))
    if res > 0:
        strs = 'insert %s candle success ' % write_table_candle
        mlog(strs, DBB)
    else:
        strs = 'insert %s candle fail ' % write_table_candle
        mlog(strs, DBB)



if __name__ == "__main__":
    #delete_coin_chart('btc')
    #delete_coin_chart('iota')

    #create_init_db()
    #insertChartMonList('btc')
    #create_coin_chart('btc')



    #results = selectChartMonList()
    #print (results)

    pass



