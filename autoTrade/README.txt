﻿

**********************************************
초기 설정법 
1. 마리아 디비 설치 
sudo apt-get install mariadb-server
 * [옵션] 외부접근 설정 vim /etc/mysql/mariadb.conf.d/50-server.cnf 에 들어가서 
   bind-address 변경 후 마리아 디비 재시작 
 * 이때 CentOS7 의 경우에는 3306 포트 방화벽 풀어줘야 함 

2. 파이썬 마리아 디비 커넥터 설치 
pip install mysql-connector-python-rf 

3. 데이터 베이스 계정 생성 
USER - yghong PASSWORD - ghddudrl

SELECT HOST, USER, PASSWORD FROM USER; - 사용자 확인 
CREATE USER '아이디'@'%' IDENTIFIED BY '비밀번호'; - 사용자 생성 
GRANT ALL PRIVILEGES ON 데이터베이스.* TO '아이디'@'%'; - 권한 주기 
FLUSH PRIVILEGES; - 새로 고침

4. 데이터 베이스 생성 
DATABASE = chart_dat
create database chart_dat[데이터베이스 이름]

5. 프로그램 디렉토리 세팅 
- [mypath]/autoTradeApp/ 에 소스 다운로드 
- export PYTHONPATH=${PYTHONPATH}:[mypath] - 이 문장으로 python path 설정  
CentOS 7
아래 명령어 /.bash_profile 에 써야함
export PYTHONPATH=${PYTHONPATH}:/home/yghong/AutoTrade[mypath]
아래 명령어 하면 덥어 써짐
export PYTHONPATH=/home/yghong/AutoTrade[mypath]

6 mqtt 서버 설치 
sudo apt-get install mosquitto

7. 필요한 패키지 설치 pip 이용 
pip list 로 봐서 필요한 패키지만 뽑아서 txt 파일로 만들어 놓기 
- gevent 
- numpy
- requests
- httplib2
- paho-mqtt
- zipline

8. zipline 설치
* Cent OS
 - yum install gcc
 - yum install python-devel
 - yum install gcc-c++
 - pip install zipline

* Window
 - 아나콘다를 굳이 설치하지 않아도 VC ++ 9 만 설치 되면 될 듯 ? (어짜피 소스용으로만 쓸것 임으로 )
 - 아나콘다 Navigator 주피터 QT
 - 아나콘다 설치 https://www.anaconda.com/download/#windows
 - http://aka.ms/vcpython27 에서 VC ++ 9 다운로드 후 설치
 - 아나콘다 프롬프트 실행 (설치된 프로그램임)
 - 실행한 프롬프트에서 pip install zipline 으로 패키지 설치

* Ubuntu
 - dpkg 에러 날수도 있음 sudo rm -rf /var/lib/dpkg/lock -> reboot 후 안되면 터미널에서 시키는 대로 
 - sudo apt-get install libatlas-base-dev python-dev gfortran pkg-config libfreetype6-dev
 - pip install zipline 
 - virtualenv 사용해서 할 것
* zipline API key : Uu_Vvw-au7Kx-hEy-s1E

 9. matplotlib
  - 설치시 후 실행 시 tkinter 오류시 -> yum install tkinter
  - candlestick_ohlc의 경우 matplotlib 버전이 2.1.2 이하면 사용 가능하고 상위 버전이면 
    관련 finance 모듈이 mpl-finance로 떨어저 나감 즉 mpl-finance를 설치 해야함 

 10 TA-Lib
 *Winsow
  - https://www.lfd.uci.edu/~gohlke/pythonlibs/#ta-lib
  - 위 사이트에서 TA_Lib-0.4.17-cp27-cp27m-win32.whl 를 다운받아서 pip install 로 설치
    PC에 파이선이 32비트로 설치되어 있어서 64 비트는 안깔림

 * Linux
  - https://mrjbq7.github.io/ta-lib/install.html, Dependencies 칸을 따라하는데
  Centos7 64 비트의 경우에는 하기와 같이 한다.
  +++++++++++++
    ./configure --prefix=/usr  --exec-prefix=/usr --libdir=/usr/lib64
    make
    make install
  +++++++++++++

  *** Zipline Extension.py

**********************************************
가상환경 conda
생성 : conda create --name [환경이름] python=2.7
삭제 : conda-env remove -n [환경이름]
source activate autoTradeApp

* matplotlib 사용시 아래와 같이 해줘야 함
echo "backend: TkAgg" >> ~/.matplotlib/matplotlibrc

가상환경 패키지 체크
conda list -n myenv
conda list --export > package-list.txt



**********************************************

show full columns from testtable;

mysql db 용량 확인
du -h /var/lib/mysql

CREATE TABLE testtable(
	ID INT NOT NULL AUTO_INCREMENT,
	price FLOAT,
	PRIMARY KEY (ID)
);

CREATE TABLE askLow (
	id INT NOT NULL AUTO_INCREMENT,
	ts DATETIME,
	price1 FLOAT,
	qty1 FLOAT,
	price2 FLOAT,
	qty2 FLOAT,
	price3 FLOAT,
	qty3 FLOAT,
	price4 FLOAT,
	qty4 FLOAT,
	price5 FLOAT,
	qty5 FLOAT,
	primary key (id)
)

CREATE TABLE askHigh (
	id INT NOT NULL AUTO_INCREMENT,
	ts DATETIME,
	price1 FLOAT,
	qty1 FLOAT,
	price2 FLOAT,
	qty2 FLOAT,
	price3 FLOAT,
	qty3 FLOAT,
	price4 FLOAT,
	qty4 FLOAT,
	price5 FLOAT,
	qty5 FLOAT,
	primary key (id)
)

ALTER TABLE askLow
DROP COLUMN ts;

ALTER TABLE askLow
ADD ts DATETIME;

ALTER TABLE askLow MODIFY COLUMN ts DATETIME FIRST;
ALTER TABLE askLow MODIFY COLUMN id INT NOT NULL AUTO_INCREMENT;
alter table balance_info add used_balance FLOAT default 0;
alter table balance_info add limit_balance FLOAT default 0;



모든 DB 데이터 삭제-
DELETE FROM askLow;

M_1_CANDLE
M_3_CANDLE
M_5_CANDLE
M_15_CANDLE
M_30_CANDLE
M_1H_CANDLE
M_2H_CANDLE
M_4H_CANDLE
M_6H_CANDLE
M_12H_CANDLE
M_1D_CANDLE

CREATE TABLE M_1_CANDLE (
	id INT NOT NULL AUTO_INCREMENT,
	ts DATETIME,
	last_price FLOAT,
	L_price FLOAT,
	H_price FLOAT,
	prev_price FLOAT,
	primary key (id)
)

CREATE TABLE M_1D_CANDLE (
	id INT NOT NULL AUTO_INCREMENT,
	ts DATETIME,
	last_price FLOAT,
	L_price FLOAT,
	H_price FLOAT,
	prev_price FLOAT,
	primary key (id)
);

CREATE TABLE  sell_ask(
	id INT NOT NULL AUTO_INCREMENT,
	ts DATETIME,
	orderid CHAR(255),
	price FLOAT,
	qty FLOAT,
	status CHAR(10),
	currency CHAR(5),
	primary key (id)
);

CREATE TABLE  order_history(
	id INT NOT NULL AUTO_INCREMENT,
	ts DATETIME,
	orderid CHAR(255),
	orderway CHAR(15),
	currency CHAR(5),
	qty FLOAT,
	used FLOAT,
	primary key (id)
);

CREATE TABLE  test_buy(
	id INT NOT NULL AUTO_INCREMENT,
	ts DATETIME,
	orderid CHAR(255),
	price FLOAT,
	qty FLOAT,
	status CHAR(10),
	currency CHAR(5),
	primary key (id)
);

CREATE TABLE  margin_info(
	id INT NOT NULL AUTO_INCREMENT,
	ts DATETIME,
	currency CHAR(5),
	avg_unit_price FLOAT,
	cur_price FLOAT,
	buy_price FLOAT,
	cur_qty FLOAT,
	profit_loss_per FLOAT,
	primary key (id)
);

CREATE TABLE  balance_info(
	id INT NOT NULL AUTO_INCREMENT,
	ts DATETIME,
	currency CHAR(5),
	avail FLOAT,
	balance FLOAT,
	primary key (id)
);



DB 초기화 루틴

DELETE FROM M_1_CANDLE;
DELETE FROM M_3_CANDLE;
DELETE FROM M_5_CANDLE;
DELETE FROM M_10_CANDLE;
DELETE FROM M_15_CANDLE;
DELETE FROM askHigh;
DELETE FROM askLow;
DELETE FROM bidHigh;
DELETE FROM bidLow;
DELETE FROM buy_bid;
DELETE FROM sell_ask;
DELETE FROM margin_info;


alter table askLow auto_increment=1;


아래 명령어 /.bash_profile 에 써야함
export PYTHONPATH=${PYTHONPATH}:/home/yghong/AutoTrade
아래 명령어 하면 덥어 써짐
export PYTHONPATH=/home/yghong/AutoTrade

ss
pdb.set_trace()