# -*- coding: utf-8 -*-

import gevent
from .common_func import *
from .api_task import *
import pandas_datareader.data as web


memory_table = []
#bid low 사려는 가장 낮은 가격

#아래 합수 테스트 DB 를 조회함
def get_order_data(coin_price, currency, way_dic):

    order_way = way_dic[WAY_KEY]
    real = way_dic[REAL_KEY]

    if order_way == TR_WAY_BUY:
        avail = check_balance(KRW, real)
        if avail > 0:
            qty = get_avail_qty_tobuy(coin_price, avail,100, TR_FEE)
            print(coin_price, ' ', qty, '  ', currency)
            return [coin_price, qty, currency]

    elif order_way == TR_WAY_SELL:
        avail = check_balance(currency, real)
        if avail > 0:
            qty = avail
            print(coin_price, ' ', qty, '  ', currency)
            return [coin_price, qty, currency]

    return False


def check_balance(currency, real):
    data = check_balance_data(currency, real)
    avail = float(data[3])
    used = int(data[5])
    limit = int(data[6])
    if limit - used > 0 and currency == KRW :
        if avail < limit - used:
            if avail < 5000:#한화 기준 5000 원 미만일 땐 거래 안함
                return 0
            else:
                return avail
        else:
            return limit-used
    elif currency != KRW:
        return avail
    else:
        print('Can not buy cause limited balance')
        return 0


def getTrDataFromOption(opt_dic, way_dic):

    option_list = []
    if type(opt_dic) != dict:
        return 3

    if (OP_CURENCY in opt_dic) == False:
        return 4

    option_list.append(opt_dic[OP_CURENCY])
    currency = option_list[0]

    real = way_dic[REAL_KEY]
    order_way = way_dic[WAY_KEY]

    # price
    dat = check_balance_data(KRW, real)
    my_avail_balance = int(dat[3])
    if order_way == TR_WAY_BUY:
        if my_avail_balance < MINIMUM_BALANCE:
            return 2

    #pdb.set_trace()
    #gevent.sleep(0)

    last_price, err_num = apiGetLastPrice(currency)

    if last_price == False:
        return err_num
    print('My available balance : %d, Last Price Of [%s] : %s ' % (my_avail_balance, currency, last_price))

    if OP_PRICE in opt_dic and checkisNumber(opt_dic[OP_PRICE]):
        option_list.append(float(opt_dic[OP_PRICE]))
        last_price = opt_dic[OP_PRICE]
    else:
        option_list.append(float(last_price))

    if OP_QTY_TR_QT_PER in opt_dic and checkisNumber(opt_dic[OP_QTY_TR_QT_PER]):

        percent = 25*int(opt_dic[OP_QTY_TR_QT_PER])
        if order_way == TR_WAY_BUY:
            qty = get_avail_qty_tobuy(last_price, my_avail_balance, percent, TR_FEE)
        else:
            db_list = check_balance_data(currency, real)
            if db_list == None:
                return 6

            qty = get_approc_val(db_list[3]) * float(percent / 100.0)

        option_list.append(qty)

    elif OP_QTY_TR_QT in opt_dic and checkisNumber(opt_dic[OP_QTY_TR_QT]):
        if order_way == TR_WAY_BUY:
            if my_avail_balance < last_price * float(opt_dic[OP_QTY_TR_QT]) + (last_price * float(opt_dic[OP_QTY_TR_QT])) * TR_FEE:
                return 2
            else:
                option_list.append(float(opt_dic[OP_QTY_TR_QT]))
        else:
            db_list = check_balance_data(currency, real)
            if db_list == None:
                return 6

            qty = get_approc_val(db_list[3])
            if qty < float(opt_dic[OP_QTY_TR_QT]):
                return 2
            else:
                option_list.append(float(opt_dic[OP_QTY_TR_QT]))


    else:
        if order_way == TR_WAY_BUY:
            qty = get_avail_qty_tobuy(last_price, my_avail_balance, 100, TR_FEE)
        else:
            db_list = check_balance_data(currency, real)
            if db_list == None:
                return 6

            qty = get_approc_val(db_list[3])
        option_list.append(qty)

    return option_list

def task_trading_controller_rx():
    print(C_BOLD + '**TRADING CONTROLLER RX START**' + C_END)
    frame_info = getframeinfo(currentframe())
    while True:
        #sema_trade_con.acquire()
        try:
            data =0
            data = trade_control_rx_q.get_nowait()

            option, para1, para2, json_dat = data
            src_block = para2
            str_dat = '%s : option : %s, para1 : %s, para2 : %s, info : %s' \
                % (frame_info.function, str(option), str(para1), str(para2), str(json_dat))
            mlog(str_dat, TR)
            #if option == GET_ORDER_BOOK:
            #    res = my_strategy(json_dat[BID][0][PRICE],json_dat[BID][-1][PRICE],json_dat[ASK][-1][PRICE],json_dat[ASK][0][PRICE],0)

            if para1 == MQB:
                mlog('+++++++++++++++task_trading_controller() MQTT order recived+++++++++++++++', TR)
                opt = getOptionFromMQ(json_dat)
                key_opt, way_dic = getTradeWay(option)

                if opt.get(OP_APP_ORDER_ID) == None:
                    trade_control_tx_q.put_nowait(get_msg_for_q(TR_ORDER_NAK, para1=None ,para2=src_block, data_list = [way_dic, 11, 0, 0, 'None']))
                else:
                    APP_ORDER_ID = int(opt.get(OP_APP_ORDER_ID))

                if key_opt == MQ_TR_ORDER:
                    Gsem_order.acquire()
                    para = getTrDataFromOption(opt, way_dic)

                    if type(para) != list:
                        mlog(CMN_ERR_CODE[para], TR) # 여기서 Para 는 error code
                        trade_control_tx_q.put_nowait(get_msg_for_q(TR_ORDER_NAK, APP_ORDER_ID, src_block, [way_dic, para, 0, 0, 'None']))

                        #sema_trade_con.release()
                        Gsem_order.release()
                    else:
                        currency, price, qty = para
                        #api_q.put_nowait(get_msg_for_q(GET_LAST_PRICE, TEST_BUY, src_block, currency))
                        api_q.put_nowait(get_msg_for_q(API_ORDER, APP_ORDER_ID, src_block, [price, qty, currency, way_dic]))

                elif key_opt == MQ_TR_CANCEL:
                    currency = getCurrencyFromOption(opt)

                    if type(currency) != list:
                        # currency 는 에러 코드
                        way_dic[WAY_KEY] = TR_WAY_CANCEL
                        trade_control_tx_q.put_nowait(get_msg_for_q(TR_ORDER_NAK, APP_ORDER_ID, src_block, [way_dic, currency, 0, 0, 'None']))
                    else:
                        api_q.put_nowait(get_msg_for_q(CANCEL_ORDER, APP_ORDER_ID, src_block, [currency[0], way_dic]))


                elif key_opt == MQ_TR_MARGIN:
                    currency = getCurrencyFromOption(opt)

                    if type(currency) != list:
                        mqtt_q.put_nowait(get_msg_for_q(option, APP_ORDER_ID, src_block, currency))
                    else:
                        api_q.put_nowait(get_msg_for_q(UPDATE_MARGINE, APP_ORDER_ID, src_block, [currency[0], way_dic]))


                elif option == MQ_SHOW_PROCESS:
                    # TODO Client 로 주는 루틴 개발 필요
                    print('*'*10)
                    for i in PRO_LIST:
                        print(i.name)
                    print('*' * 10)

        except NQueue.Empty:
            pass
        gevent.sleep(0)
        #sema_trade_con.release()

def task_trading_controller_tx():
    print(C_BOLD + '**TRADING CONTROLLER TX START**' + C_END)
    frame_info = getframeinfo(currentframe())
    while True:
        #sema_trade_con.acquire()
        try:
            data = trade_control_tx_q.get_nowait()

            option, para1, para2, json_dat = data
            app_id = para1
            src_block = para2
            str_dat = '%s : option : %s, para1 : %s, para2 : %s, info : %s' \
                % (frame_info.function, str(option), str(para1), str(para2), str(json_dat))
            mlog(str_dat, TR)
            #if option == GET_ORDER_BOOK:
            #    res = my_strategy(json_dat[BID][0][PRICE],json_dat[BID][-1][PRICE],json_dat[ASK][-1][PRICE],json_dat[ASK][0][PRICE],0)

            if option == TR_ORDER_ACK:
                w_dic = json_dat[0]
                strings = 'orderId=%s, way=%d, real=%d ' % (json_dat[1], w_dic.get(WAY_KEY), w_dic.get(REAL_KEY))
                mqtt_q.put_nowait(get_msg_for_q(MQ_TR_ACK, para1= app_id, para2=src_block, data_list=strings))

            elif option == TR_ORDER_NAK:
                w_dic = json_dat[0]
                err_code = json_dat[1]
                price, qty, currency = json_dat[2:5]

                strings = 'orderId=None, way=%d, real=%d, err_code=%d ' % \
                          (w_dic.get(WAY_KEY), w_dic.get(REAL_KEY), err_code)
                mqtt_q.put_nowait(get_msg_for_q(MQ_TR_NAK, para1=app_id, para2=src_block, data_list=strings))
                mlog('order fail', TR)

            # --------------from task_trading_proc---------------------

            elif option == TRADE_ORDER_COM:
                orderId, cur, price, qty, way_dic = json_dat

                #mq_key_str = makeMQmsgByTradeWay('trade', way_dic, 'filled')
                #print mq_key_str
                strings = 'orderId=%s, cur=%s, price=%s, qty=%s, way=%d, real=%d ' \
                          % (orderId, cur, price, qty, way_dic.get(WAY_KEY), way_dic.get(REAL_KEY))
                mqtt_q.put_nowait(get_msg_for_q(MQ_TR_COM, para1=app_id, para2=src_block, data_list=strings))

            elif option == TRADE_ORDER_FAIL:
                orderId, cur, price, qty, way_dic = json_dat

                #mq_key_str = makeMQmsgByTradeWay('trade', way_dic, 'fail')
                #print mq_key_str
                strings = 'orderId=%s, cur=%s, price=%s, qty=%s, way=%d, real=%d, fail_code=%d' \
                          % (orderId, cur, price, qty, way_dic.get(WAY_KEY), way_dic.get(REAL_KEY), 0)
                mqtt_q.put_nowait(get_msg_for_q(MQ_TR_FAIL, para1=app_id, para2=src_block, data_list=strings))

            elif option == UPDATE_MARGINE_ACK:

                update_result, currency, way_dic = json_dat
                mlog(update_result,TR)

                mq_key_str = makeMQmsgByTradeWay('trade', way_dic, 'complete')
                mqtt_q.put_nowait(get_msg_for_q(mq_key_str, para1=app_id, para2=src_block, data_list=update_result))

            elif option == GET_LAST_PRICE_ACK:
                second_option = app_id

                last_price, currency, way_dic = json_dat
                str_dat = 'second command option :%d, last price: %s '% (second_option, str(last_price))
                mlog(str_dat,TR)
                strs = 'Coin %s last price is %s ' % currency, last_price
                mqtt_q.put_nowait(get_msg_for_q(mq_key_str, para1=app_id, para2=src_block, data_list=strs))
                #result = get_order_data(float(last_price), currency, second_option)
                #if result == False:
                #    mlog('Order Fail ', TR)
                #else:
                    #api_q.put_nowait(get_msg_for_q(second_option, APP_ORDER_ID, src_block, result))


        except NQueue.Empty:
            pass
        gevent.sleep(0)
        #sema_trade_con.release()

if __name__ == "__main__":

    para = getOptionFromMQ('-c iota -qp 1')
    key_opt, way_dic = getTradeWay('trade_sell_test')
    print(para)

    print(getTrDataFromOption(para, way_dic))
