# coding=utf-8
from .db_lib import *
#!! for commit

def create_init_db():
    #check_db_name = currency+'_ticker'
    print('\x1b[1;35m------Make INIT DB------\x1b[1;m')
    mariadb_connection = mariadb.connect(user=USER, password=PASSWORD, database=DATABASE)
    cursor = mariadb_connection.cursor()

    db_tables = {}

    db_tables['currency_mon_list'] = (
        "CREATE TABLE `currency_mon_list` ("
        " `id` INT NOT NULL AUTO_INCREMENT,"
        " `ts` DATETIME,"
        " `mon_currency` CHAR(5),"
        " PRIMARY KEY (`id`),"
        " UNIQUE (`mon_currency`)"
        ")"
    )

    db_tables['balance_info'] = (
        "CREATE TABLE `balance_info` ("
        "`id` INT(11) NOT NULL AUTO_INCREMENT,"
        "`ts` DATETIME NULL DEFAULT NULL,"
        "`currency` CHAR(5) NULL DEFAULT NULL,"
        "`avail` DOUBLE NULL DEFAULT NULL,"
        "`balance` DECIMAL NULL DEFAULT NULL,"
        "`used_balance` DECIMAL NULL DEFAULT '0',"
        "`limit_balance` DECIMAL NULL DEFAULT '0',"
        "PRIMARY KEY (`id`)"
        ")"
    )

    db_tables['balance_info_test'] = (
        "CREATE TABLE `balance_info_test` ("
        "`id` INT(11) NOT NULL AUTO_INCREMENT,"
        "`ts` DATETIME NULL DEFAULT NULL,"
        "`currency` CHAR(5) NULL DEFAULT NULL,"
        "`avail` DOUBLE NULL DEFAULT NULL,"
        "`balance` DECIMAL NULL DEFAULT NULL,"
        "`used_balance` DECIMAL NULL DEFAULT '0',"
        "`limit_balance` DECIMAL NULL DEFAULT '0',"
        "PRIMARY KEY (`id`)"
        ")"
    )

    db_tables['buy_bid'] = (
        "CREATE TABLE `buy_bid` ("
        "`id` INT(11) NOT NULL AUTO_INCREMENT,"
        "`ts` DATETIME NULL DEFAULT NULL,"
        "`orderid` CHAR(255) NULL DEFAULT NULL,"
        "`price` DECIMAL NULL DEFAULT NULL,"
        "`qty` DOUBLE NULL DEFAULT NULL,"
        "`status` CHAR(10) NULL DEFAULT NULL,"
        "`currency` CHAR(5) NULL DEFAULT NULL,"
        "PRIMARY KEY (`id`)"
        ")"
    )

    db_tables['sell_ask'] = (
        "CREATE TABLE `sell_ask` ("
        "`id` INT(11) NOT NULL AUTO_INCREMENT,"
        "`ts` DATETIME NULL DEFAULT NULL,"
        "`orderid` CHAR(255) NULL DEFAULT NULL,"
        "`price` DECIMAL NULL DEFAULT NULL,"
        "`qty` DOUBLE NULL DEFAULT NULL,"
        "`status` CHAR(10) NULL DEFAULT NULL,"
        "`currency` CHAR(5) NULL DEFAULT NULL,"
        "PRIMARY KEY (`id`)"
        ")"
    )
    db_tables['margin_info'] = (
        "CREATE TABLE `margin_info` ("
        "`id` INT(11) NOT NULL AUTO_INCREMENT,"
        "`ts` DATETIME NULL DEFAULT NULL,"
        "`currency` CHAR(5) NULL DEFAULT NULL,"
        "`avg_unit_price` FLOAT NULL DEFAULT NULL,"
        "`cur_price` DECIMAL NULL DEFAULT NULL,"
        "`buy_price` DECIMAL NULL DEFAULT NULL,"
        "`cur_qty` DOUBLE NULL DEFAULT NULL,"
        "`profit_loss_per` DECIMAL NULL DEFAULT NULL,"
        "PRIMARY KEY (`id`)"
        ")"
    )

    db_tables['margin_info_test'] = (
        "CREATE TABLE `margin_info_test` ("
        "`id` INT(11) NOT NULL AUTO_INCREMENT,"
        "`ts` DATETIME NULL DEFAULT NULL,"
        "`currency` CHAR(5) NULL DEFAULT NULL,"
        "`avg_unit_price` FLOAT NULL DEFAULT NULL,"
        "`cur_price` DECIMAL NULL DEFAULT NULL,"
        "`buy_price` DECIMAL NULL DEFAULT NULL,"
        "`cur_qty` DOUBLE NULL DEFAULT NULL,"
        "`profit_loss_per` DECIMAL NULL DEFAULT NULL,"
        "PRIMARY KEY (`id`)"
        ")"
    )

    db_tables['order_history'] = (
        "CREATE TABLE `order_history` ("
        "`id` INT(11) NOT NULL AUTO_INCREMENT,"
        "`ts` DATETIME NULL DEFAULT NULL,"
        "`apporderid` CHAR(255) NULL DEFAULT NULL,"
        "`orderid` CHAR(255) NULL DEFAULT NULL,"
        "`orderway` CHAR(15) NULL DEFAULT NULL,"
        "`currency` CHAR(5) NULL DEFAULT NULL,"
        "`qty` DOUBLE NULL DEFAULT NULL,"
        "`unitprice` DECIMAL NULL DEFAULT NULL,"
        "`dealamount` DECIMAL NULL DEFAULT NULL,"
        "`fee` DECIMAL NULL DEFAULT NULL,"
        "`finalamount` DECIMAL NULL DEFAULT NULL,"
        "`vote` CHAR(15) NULL DEFAULT NULL,"
        "PRIMARY KEY (`id`)"
        ")"
    )

    db_tables['order_history_test'] = (
        "CREATE TABLE `order_history_test` ("
        "`id` INT(11) NOT NULL AUTO_INCREMENT,"
        "`ts` DATETIME NULL DEFAULT NULL,"
        "`apporderid` CHAR(255) NULL DEFAULT NULL,"
        "`orderid` CHAR(255) NULL DEFAULT NULL,"
        "`orderway` CHAR(15) NULL DEFAULT NULL,"
        "`currency` CHAR(5) NULL DEFAULT NULL,"
        "`qty` DOUBLE NULL DEFAULT NULL,"
        "`unitprice` DECIMAL NULL DEFAULT NULL,"
        "`dealamount` DECIMAL NULL DEFAULT NULL,"
        "`fee` DECIMAL NULL DEFAULT NULL,"
        "`finalamount` DECIMAL NULL DEFAULT NULL,"
        "`vote` CHAR(15) NULL DEFAULT NULL,"
        "PRIMARY KEY (`id`)"
        ")"
    )

    for name, ddl in db_tables.items():
        print_str = ''
        try:
            print_str = "Creating table {}: ".format(name)
            cursor.execute(ddl)
        except mariadb.Error as err:
            if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                print_str = print_str + 'Already Exist'
                print(print_str)
            else:
                print_str = print_str + err.msg
                print(print_str)
        else:
            print_str = print_str + 'OK'
            print(print_str)

    mariadb_connection.close()


def delete_trade_db():
    print('\x1b[1;35m------Delete Trade DB------\x1b[1;m')
    mariadb_connection = mariadb.connect(user=USER, password=PASSWORD, database=DATABASE)
    cursor = mariadb_connection.cursor()

    chart_db_name_list = [
                             'currency_mon_list',
                             'balance_info',
                             'balance_info_test',
                             'buy_bid',
                             'sell_ask',
                             'margin_info',
                             'margin_info_test',
                             'order_history',
                             'order_history_test'
                             ]

    for db_name in chart_db_name_list:
        try:
            db_str = 'DROP TABLE %s' % db_name
            print_str = "Deleting table {}: ".format(db_name)
            cursor.execute(db_str)
        except mariadb.Error as err:
            if err.errno == errorcode.ER_BAD_TABLE_ERROR:
                print_str = print_str + 'No table exist. '
                print(print_str)
            else:
                print_str = print_str + err.msg
                print(print_str)
        else:
            print_str = print_str + 'OK'
            print(print_str)
    mariadb_connection.close()


def balance_info_update(currency, add_avail, add_balance, real):
    ts_int = time.time()
    date_str = datetime.fromtimestamp(ts_int)
    ts = '%s' % date_str

    if real == TR_WAY_TEST:
        balance_info_list = db_select_all(TEST_BALANCE_INFO_TBL)
    else:
        balance_info_list = db_select_all(BALANCE_INFO_TBL)

    for balance_info in balance_info_list:
        if (balance_info[2] == currency):
            avail, balance = balance_info[3:5]
            if currency == KRW:
                new_avail = int(add_avail) + int(avail)
                new_balance = int(add_balance) + int(balance)
            else:
                new_avail = get_approc_val(add_avail) + float(avail)
                new_balance = get_approc_val(add_balance) + float(balance)
            val_tup = (ts, currency, new_avail, new_balance)
            if real == TR_WAY_TEST:
                db_update(TEST_BALANCE_INFO_TBL, BALANCE_INFO_TUP, val_tup,'currency', currency)
            else:
                db_update(BALANCE_INFO_TBL, BALANCE_INFO_TUP, val_tup, 'currency', currency)
            return

    val_tup = (ts, currency, add_avail, add_balance)
    if real == TR_WAY_TEST:
        db_insert(TEST_BALANCE_INFO_TBL, BALANCE_INFO_TUP, val_tup)
    else:
        db_insert(BALANCE_INFO_TBL, BALANCE_INFO_TUP, val_tup)
    return


def balance_info_inital(currency, add_avail, add_balance, real):
    ts_int = time.time()
    date_str = datetime.fromtimestamp(ts_int)
    ts = '%s' % date_str

    balance_info_list = db_select_all(TEST_BALANCE_INFO_TBL)
    for balance_info in balance_info_list:
        if (balance_info[2] == currency):
            val_tup = (ts,currency, add_avail, add_balance)
            db_update(TEST_BALANCE_INFO_TBL, BALANCE_INFO_TUP, val_tup,'currency', currency)
            return

    val_tup = (ts, currency, add_avail, add_balance)
    if real == TR_WAY_TEST:
        db_insert(TEST_BALANCE_INFO_TBL, BALANCE_INFO_TUP, val_tup)
    else:
        db_insert(BALANCE_INFO_TBL, BALANCE_INFO_TUP, val_tup)
    return


def krw_wr_init_balance(limit_balance, real):
    print('limit balance : ',limit_balance)
    if real == TR_WAY_TEST:
        db_update_single(TEST_BALANCE_INFO_TBL,LB,limit_balance, 'currency', KRW)
    else:
        db_update_single(BALANCE_INFO_TBL, LB, limit_balance, 'currency', KRW)


def update_used_balance(used_balance, real):
    print('update_used_balance %f' % used_balance)
    if real == TR_WAY_TEST:
        data = check_balance_data(KRW, real)
    else:
        data = check_balance_data(KRW, real)

    used_bal = data[5]
    total_used_bal = float(used_bal) + used_balance
    if total_used_bal <= 0:
        total_used_bal = 0

    if real == TR_WAY_TEST:
        db_update_single(TEST_BALANCE_INFO_TBL, UB, total_used_bal, 'currency', KRW)
    else:
        db_update_single(BALANCE_INFO_TBL, UB, total_used_bal, 'currency', KRW)


def check_balance_data(currency, real):
    if real == TR_WAY_TEST:
        data = db_select(TEST_BALANCE_INFO_TBL, CURRENCY, currency)
    else:
        data = db_select(BALANCE_INFO_TBL, CURRENCY, currency)
    if len(data) == 0:
        return None
    return data[0]


def avr_margine_write(buy_price, qty, currency, real):
    ts_int = time.time()
    date_str = datetime.fromtimestamp(ts_int)
    ts = '%s' % date_str

    if real == TR_WAY_TEST:
        coin_balance_info_list = db_select_all(TEST_MARGINE_INFO_TBL)
    else:
        coin_balance_info_list = db_select_all(MARGINE_INFO_TBL)

    for coin_balance in coin_balance_info_list:
        if coin_balance[2] == currency:
            #update
            ori_aup, cur_price, total_buy_krw,cur_qty = coin_balance[3:7]

            new_aup, new_buy_krw,new_qty=unit_price(ori_aup,float(cur_qty),buy_price,qty)
            new_cur_price = buy_price*new_qty
            pal = profit_and_loss(new_cur_price,new_buy_krw)
            val_tup = (ts,currency,new_aup,new_cur_price,new_buy_krw,new_qty,pal)
            if real == TR_WAY_TEST:
                db_update(TEST_MARGINE_INFO_TBL,MARGINE_INFO_TUP,val_tup,'currency',currency)
            else:
                db_update(MARGINE_INFO_TBL, MARGINE_INFO_TUP, val_tup, 'currency', currency)
            return True

    if qty < 0:
        return False
    new_aup, new_buy_krw,new_qty=unit_price(0,0,buy_price,qty)
    new_cur_price = buy_price*new_qty
    pal = profit_and_loss(new_cur_price,new_buy_krw)
    val_tup = (ts,currency,new_aup,new_cur_price,new_buy_krw,new_qty,pal)
    if real == TR_WAY_TEST:
        db_insert(TEST_MARGINE_INFO_TBL,MARGINE_INFO_TUP,val_tup)
    else:
        db_insert(MARGINE_INFO_TBL, MARGINE_INFO_TUP, val_tup)
    return True

def avr_margine_update(cur_coin_krw, currency, real):
    ts_int = time.time()
    date_str = datetime.fromtimestamp(ts_int)
    ts = '%s' % date_str
    if real == TR_WAY_TEST:
        coin_balance_info_list = db_select_all(TEST_MARGINE_INFO_TBL)
    else:
        coin_balance_info_list = db_select_all(MARGINE_INFO_TBL)

    for coin_balance in coin_balance_info_list:
        if (coin_balance[2] == currency):
            #update
            ori_aup, cur_price, total_buy_krw, cur_qty = coin_balance[3:7]

            ori_aup = float(ori_aup)
            cur_price = float(cur_price)
            total_buy_krw = float(total_buy_krw)
            cur_qty = float(cur_qty)

            new_cur_price = float(cur_coin_krw * cur_qty)
            pal = profit_and_loss(new_cur_price, total_buy_krw)
            print('\x1b[1;35mcurrent %s price = %d \x1b[1;m' % (currency,int(cur_coin_krw)))
            val_tup = (ts, currency, ori_aup, new_cur_price, total_buy_krw, cur_qty, pal)
            if real == TR_WAY_TEST:
                db_update(TEST_MARGINE_INFO_TBL,MARGINE_INFO_TUP,val_tup,'currency', currency)
            else:
                db_update(MARGINE_INFO_TBL, MARGINE_INFO_TUP, val_tup, 'currency', currency)
            mlog('update margin info for looking', DBB)
            return



def order_app_id_write(app_id, real):
    ts_int = time.time()
    date_str = datetime.fromtimestamp(ts_int)
    ts = '%s' % date_str
    tup = (ts, app_id)
    # 중복 검사 루틴 들어가야 함 아예 DB를 Unique로 ?
    if real == TR_WAY_TEST:
        db_insert(TEST_ORDER_HISTORY_TBL, APP_ORDER_ID_TUP, tup)
    else:
        db_insert(ORDER_HISTORY_TBL, APP_ORDER_ID_TUP, tup)

def read_order_history_by_appid(app_id, real):
    if real == TR_WAY_TEST:
        data = db_select(TEST_ORDER_HISTORY_TBL, APPID, app_id)
    else:
        data = db_select(ORDER_HISTORY_TBL, APPID, app_id)
    if len(data) == 0:
        return None
    return data[0]


def update_order_history_by_appid(app_id, column, value, real):
    ts_int = time.time()
    date_str = datetime.fromtimestamp(ts_int)
    ts = '%s' % date_str

    if type(column) != list or type(value) != list:
        return

    col_tup = tuple(['ts'] + column)
    val_tup = tuple([ts] + value)
    if real == TR_WAY_TEST:
        db_update(TEST_ORDER_HISTORY_TBL, col_tup, val_tup, APPID, app_id)
    else:
        db_update(ORDER_HISTORY_TBL, col_tup, val_tup, APPID, app_id)

def order_history_update(app_id, order_id, order_way, currency, qty, unitprice, fee, vote, real):
    ts_int = time.time()
    date_str = datetime.fromtimestamp(ts_int)
    ts = '%s' % date_str

    o_way = ['BUY', 'SELL']

    deal_amount = qty*unitprice
    trade_fee = qty*unitprice*fee/100
    if order_way == 0:
        finalamount = deal_amount + trade_fee
    else:
        finalamount = deal_amount - trade_fee

    val_tup = (ts, order_id, o_way[order_way], currency, qty, unitprice, deal_amount, trade_fee, finalamount, vote)
    if real == TR_WAY_TEST:
        db_update(TEST_ORDER_HISTORY_TBL, ORDER_HISTORY_TUP, val_tup, 'apporderid', app_id)
    else:
        db_update(ORDER_HISTORY_TBL, ORDER_HISTORY_TUP, val_tup, 'apporderid', app_id)


def order_history_insert(order_id, order_way, currency, qty, unitprice, fee, real):
    ts_int = time.time()
    date_str = datetime.fromtimestamp(ts_int)
    ts = '%s' % date_str

    o_way = ['BUY', 'SELL']

    deal_amount = qty*unitprice
    trade_fee = qty*unitprice*fee/100
    if order_way == 0:
        finalamount = deal_amount + trade_fee
    else:
        finalamount = deal_amount - trade_fee

    val_tup = (ts, order_id, o_way[order_way], currency, qty, unitprice, deal_amount, trade_fee, finalamount)
    if real == TR_WAY_TEST:
        db_insert(TEST_ORDER_HISTORY_TBL,ORDER_HISTORY_TUP,val_tup)
    else:
        db_insert(ORDER_HISTORY_TBL, ORDER_HISTORY_TUP, val_tup)


if __name__ == "__main__":
    #order_history_update(12, 'dfdf', 1, 'btc', 100.1234, 1320, 1, 1, 1)
    #print check_balance_data('krw', 1)
    # delete_trade_db()
    pass
