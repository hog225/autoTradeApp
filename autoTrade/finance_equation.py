# coding=utf-8
import numpy as np
import math

MINIMUM_BALANCE = 5000
#평균 단가
def unit_price(ori_aup, ori_qty,add_price, add_qty):
    current_qty = np.around(ori_qty + add_qty, 8)
    if current_qty <= 0:
        return 0,0,0

    org_total_assets_to_buy_coin =  ori_aup * np.around(ori_qty,8)
    add_asset = add_price * add_qty
    buy_krw = org_total_assets_to_buy_coin+add_asset

    new_aup = (buy_krw) /float(current_qty)

    return np.around(new_aup,8), buy_krw, current_qty

def profit_and_loss(u_price, o_price):
    if o_price == 0:
        return 0
    profit_percentage = (u_price/float(o_price) )-1
    strs = '%0.2f' % (profit_percentage*100)

    #return float(strs)
    return float(strs)

def get_approc_val(qty):
    if qty == 0:
        return 0
    if qty >= 1:
        qty_length = math.ceil(math.log(qty,10))
    else:
        qty_length = 0

    float_length = int(8 - qty_length)
    print(float_length)
    if float_length>0:
        decimals = np.around(qty, decimals=float_length)
    else:
        decimals = np.float32(qty)
        #decimals = np.floor(decimals)

    return decimals



def get_avail_qty_tobuy(unitprice, mybalance, ratioofmybalance, fee):
    # TODO 주식의 경우 변경이 필요함  QTY 가 무조건 INT 임으로
    uprice = float(unitprice)
    buy_amount = mybalance * ratioofmybalance/100
    buy_amount = buy_amount * (1-(fee/100))
    avail_qty = buy_amount/uprice

    return get_approc_val(avail_qty)


def getPercentIncVol(today_vol, vol_ma):
    return ((today_vol - vol_ma) / vol_ma) *100


def getPercentIncCandle(open, close):
    return ((close - open) / open) * 100

def getPriceCandlePosition(open, close, percent):
    return open + (((close - open) * percent) / 100)

if __name__ == "__main__":
    print(get_approc_val(12345678.434234))
    #print unit_price(12000,0.23245,24000,-0.23245)
