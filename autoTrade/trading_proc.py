# -*- coding: utf-8 -*-
from .api_func import *
from .common_func import *
from multiprocessing import Process, Queue

import gevent
from .db_trade_lib import *

PRO_LIST = []
PRO_Q = NQueue.Queue()


def task_process_api(stop_flag, app_id, order_id, currency, pro_q, is_ask, src_block, test_price=None, test_qty=None):
    orderId = order_id
    cur = currency
    start_time = time.time()
    testprice = test_price
    testqty = test_qty
    print('task_process_api trade_amount : ' + str(test_price*test_qty) + ' qty : ' + str(test_qty) + ' price' + str(test_price))

    real = TR_WAY_REAL if orderId.find('test') < 0 else TR_WAY_TEST

    way_dic = {
        WAY_KEY: is_ask,
        REAL_KEY: real
    }

    print('task_process_api ORDER PROCESSING ! order id : %s ' % orderId)
    #while True:

    if real ==TR_WAY_TEST:
        gevent.sleep(0)

    result = request_orderinfomation_api(orderId, cur, real, test_price, test_qty)
    if result["result"] == 'success':
        if result["status"]=='filled':
            # TODO Filled 도 수량에 따라 될 듯 즉 적은수량이 Fill 되고 추 후 또되면 문제됨 변경 필요
            print('task_process_api filled ')
            pro_q.put_nowait(get_msg_for_q(TRADE_ORDER_COM, app_id, src_block, [orderId, cur, result["price"], result["qty"], way_dic]))
            return 0
        else:
            pass

    if stop_flag == 1:
        #result = request_cancelorder_api(orderId, cur, result["price"], result["qty"])
        re = cancel_order_all_api(orderId, cur, real)
        if re['result']== 'success':
            pro_q.put_nowait(get_msg_for_q(TRADE_ORDER_FAIL, app_id, src_block, [orderId, cur, result["price"], result["qty"], way_dic]))
        else:
            pro_q.put_nowait(get_msg_for_q(TRADE_ORDER_ERROR, app_id, src_block, [orderId, cur, result["price"], result["qty"], way_dic]))
        return 0


def task_trading_proc():
    print(C_BOLD + '**TRADING PROC START**' + C_END)
    frame_info = getframeinfo(currentframe())
    while True:
        #sema_trade_proc.acquire()
        try:
            data = PRO_Q.get_nowait()

            option, para1, para2, info = data
            app_id = para1
            src_block = para2
            str_dat = '%s : option : %s, para1 : %s, para2 : %s, info : %s' \
                % (frame_info.function, str(option), str(para1), str(para2), str(info))
            mlog(str_dat, TRADE)

            if option == TRADE_ORDER_COM:
                # db 에 쓰기
                # strategy task 로 라우팅 하기

                # mlog('TEST BUY_Order ID, CURRENCY, Price, Qty :', str(info))
                orderId, cur, test_price, test_qty, way_dic = info
                order_way = way_dic[WAY_KEY]
                real = way_dic[REAL_KEY]
                # TODO 아래 트레이스 변경 위에 test_price test 삭제
                str_dat = 'TRADE ORDER COM Order ID, CURRENCY, Price, Qty :' + str(info)
                mlog(str_dat,TRADE)

                trade_amount = round(test_qty * test_price)
                print('trading_proc trade_amount : ' + str(trade_amount) + ' qty : '+str(test_qty) + ' price'+ str(test_price))

                if order_way == TR_WAY_BUY:
                    # TODO 함수화

                    avr_margine_write(test_price, test_qty, cur, real)
                    balance_info_update(cur, test_qty, test_qty, real)
                    balance_info_update(KRW, 0, -trade_amount, real)
                    #order_history_update(app_id, orderId, 0, cur, test_qty, test_price, TR_FEE, 1, real)

                elif order_way == TR_WAY_SELL:
                    # TODO 함수화
                    avr_margine_write(test_price, -test_qty, cur, real)
                    balance_info_update(cur, 0, -test_qty, real)
                    balance_info_update(KRW, trade_amount, trade_amount, real)
                    update_used_balance(-trade_amount, real)
                    #order_history_update(app_id, orderId, 1, cur, test_qty, test_price, TR_FEE, 1, real)


                update_order_history_by_appid(app_id, [VOTE], [STR_VOTE[VOTE_FILL]], real)
                delete_pro_list(app_id)


                # 아래 Order Cancel 로 변경하고 BUY_FAIL 이랑 SELL FAIL이랑 통합하기
                # Trading Controller 에게 알려줘서 MQ 로 Response를 줄 수 있도록 한다.

            elif option == TRADE_ORDER_FAIL:
                orderId, cur, price, qty, way_dic = info
                order_way = way_dic[WAY_KEY]
                real = way_dic[REAL_KEY]
                total_amount = price * qty
                print('task_trading_proc total_amount %f ' % total_amount)

                if order_way == TR_WAY_BUY:
                    mlog('TEST_BUY_FAIL', TRADE)
                    #balance_list = check_balance_data(KRW, real)
                    #org_bal = balance_list[4]
                    balance_info_update(KRW, total_amount, 0, real)
                    update_used_balance(-total_amount, real)

                else:
                    mlog('TEST_SELL_FAIL', TRADE)
                    #balance_list = check_balance_data(cur, real)
                    #org_bal = balance_list[4]
                    balance_info_update(cur, qty, 0, real)

                update_order_history_by_appid(app_id, [VOTE], [STR_VOTE[VOTE_FAIL]], real)
                delete_pro_list(app_id)



            elif option == CANCEL_ORDER_COM:
                # db에 데이터 쓰고 trading controller 로 전달
                # 여기서 PRO_LIST 는 지우지 않는다 .
                pass

            elif option == CANCEL_ORDER_FAIL:
                # db에 데이터 쓰고 trading controller 로 전달
                # 여기서 PRO_LIST 는 지우지 않는다 .
                pass

            elif option == TRADE_ORDER_ERROR:
                mlog('ORDER ERROR OCCUR', TRADE)
                delete_pro_list(app_id)


            trade_control_tx_q.put_nowait(data)
            print('send trade control q ')

        except:
            pass
        gevent.sleep(0)
        #sema_trade_proc.release()



def delete_pro_list(app_id):
    for i, proc in generator_list(PRO_LIST):
        if proc.getName() == str(app_id):
            proc.kill()
            PRO_LIST.pop(i)
            return True
    return False

def find_timer_pro_list(app_id):
    for i, proc in generator_list(PRO_LIST):
        if proc.getName() == str(app_id):
            return proc
    return False