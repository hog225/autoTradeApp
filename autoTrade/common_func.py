# -*- coding: utf-8 -*-
import time
import os
from datetime import datetime, date, timedelta
import queue as NQueue
from numbers import Number
import sys
from inspect import currentframe, getframeinfo
from threading import Semaphore
from gevent.lock import Semaphore as GSemaphore
import pdb


api_q = NQueue.Queue()
trade_control_rx_q = NQueue.Queue()
trade_control_tx_q = NQueue.Queue()
mqtt_q = NQueue.Queue()

#getcoinchart
chart_candle_q = NQueue.Queue()
chart_mqtt_q = NQueue.Queue()
chart_api_q = NQueue.Queue()
chart_control_q = NQueue.Queue()

#strategy
st_mqtt_q = NQueue.Queue()
stra_q = NQueue.Queue()


#sema
# TODO 한방에 여러개의 데이터로 Order 시 DB Controller 에 데이터가 몰려서 DB 변경 이전의 데이터를 참조함 임시방편으로 세마로 막음
Gsem_order =GSemaphore(1)


sema_db = Semaphore()
sema_api = Semaphore()
sema_trade_proc = Semaphore()
sema_trade_con = Semaphore()
sema_mqtt_sub = Semaphore()






#api option
GET_ORDER_BOOK = 1
API_ORDER = 2
TR_ORDER_ACK = 12
TR_ORDER_NAK = 13
TRADE_ORDER_COM = 5
TRADE_ORDER_FAIL = 6
#BUY  = 2
#SELL = 3
CHECK_ACCOUNT = 4
#BUY_COM = 5
#SELL_COM = 6
CHECK_ORDER_OK = 7
CANCEL_ORDER = 8
TEST_CANCEL_ORDER = 9
#TEST_BUY = 10
#TEST_SELL = 11
#BUY_ORDER_ACK = 12
#BUY_ORDER_NAK = 13
USER_ORDER = 14
#TEST_BUY_COM = 15
#TEST_SELL_COM = 16
#TEST_BUY_FAIL = 17
#TEST_SELL_FAIL = 18
#BUY_FAIL=19
#SELL_FAIL=20
UPDATE_MARGINE_ACK = 21
#SELL_ORDER_NAK = 22
USER_ORDER_ACK = 23
USER_ORDER_NAK = 24
GET_LAST_PRICE = 25
UPDATE_MARGINE = 26
GET_LAST_PRICE_ACK = 27
TRADE_ORDER_ERROR = 28
CANCEL_ORDER_ACK = 29
CANCEL_ORDER_NAK = 30
CANCEL_ORDER_COM = 31
CANCEL_ORDER_FAIL = 32

#chart
#CHART_USER_CONTROL = 100
GET_TICKER = 101


#strategy
EVENT_SIGNAL = 200



#candle option
M_1_CANDLE = 1
M_3_CANDLE = 2
M_5_CANDLE = 3
M_15_CANDLE = 4
M_30_CANDLE = 5
H_1_CANDLE = 6
H_2_CANDLE = 7
H_3_CANDLE = 8
H_4_CANDLE = 9
H_6_CANDLE = 10
H_12_CANDLE = 11
D_1_CANDLE = 12

PRICE_LOW_TUP = ('ts','price1', 'qty1',   'price2', 'qty2',   'price3', 'qty3',   'price4', 'qty4',  'price5', 'qty5')
PRICE_TUP_NO_TS = ('price1', 'qty1',   'price2', 'qty2',   'price3', 'qty3',   'price4', 'qty4',  'price5', 'qty5')
CANDLE_LOW_TUP = ('ts', 'currency', 'open', 'high', 'low', 'close', 'adjclose', 'volume')
ORDER_TUP = ('ts',	'orderid',	'price','qty','status','currency')
APP_ORDER_ID_TUP = ('ts',	'apporderid')
MARGINE_INFO_TUP = ('ts', 'currency', 'avg_unit_price','cur_price', 'buy_price','cur_qty','profit_loss_per')
BALANCE_INFO_TUP = ('ts', 'currency', 'avail', 'balance')
BALANCE_INFO_TUP_INIT = ('ts', 'currency', 'avail', 'balance','limit_balance')
ORDER_HISTORY_TUP = ('ts','orderid','orderway','currency','qty','unitprice','dealamount','fee','finalamount','vote')
TICKER_TUP = ('ts', 'currency', 'high', 'low', 'last', 'volume')



VOTE_FILL = 0
VOTE_CANCEL = 1
VOTE_FAIL = 2
VOTE_DURING =3
STR_VOTE = ['filled', 'canceled', 'fail', 'during']

LB = 'limit_balance'
UB = 'used_balance'


BID = 'bid'
ASK = 'ask'
CURRENCY = 'currency'
TS = 'timestamp'
PRICE = 'price'
APPID = 'apporderid'
ORDDERID = 'orderid'
VOTE = 'vote'

ASKH = 'askHigh' # 인덱스가 높은 것이 고가 5 최고가
ASKL = 'askLow'  # 인덱스가 높은 것이 고가 0 최저가
BIDH = 'bidHigh' # 인덱스가 낮은 것이 고가 5 최저가
BIDL = 'bidLow' # 인덱스가 낮은 것이 고가 0 최고가

CANDLE_LIST = [
        '1M_CANDLE',
        #'3M_CANDLE'
        '5M_CANDLE',
        #'15M_CANDLE'
        '30M_CANDLE',
        '1H_CANDLE',
        #'2H_CANDLE'
        #'4H_CANDLE'
        '6H_CANDLE',
        '12H_CANDLE',
        '1D_CANDLE'
]

CURRENCY_MON_LIST = 'currency_mon_list'
BUY_TBL = 'buy_bid'
SELL_TBL = 'sell_ask'
MARGINE_INFO_TBL = 'margin_info'
BALANCE_INFO_TBL = 'balance_info'
ORDER_HISTORY_TBL = 'order_history'
TEST_BALANCE_INFO_TBL = 'balance_info_test'
TEST_MARGINE_INFO_TBL = 'margin_info_test'
TEST_ORDER_HISTORY_TBL = 'order_history_test'



KRW = "krw"
BTC = "btc"
BCH = "bch"
QTUM = "qtum"
IOTA = "iota"



#MQTT
MQ_BLOCK_TRADING=1
MQ_BLOCK_STRAT=2
MQ_BLOCK_CHART=3
MQ_BLOCK_CLIENT=4



#MQTT Key
MQ_TEST_BUY = 'trade_buy_test'
MQ_TR_ACK = 'trade_order_ack'
MQ_TR_NAK = 'trade_order_nak'
MQ_TR_COM = 'trade_order_com'
MQ_TR_FAIL = 'trade_order_fail'

MQ_BUY_ACK = 'trade_buy_test_success'
MQ_BUY_NAK = 'trade_buy_test_fail'
MQ_TEST_SELL = 'trade_sell_test'
MQ_SELL_ACK = 'trade_sell_test_success'
MQ_SELL_NAK = 'trade_sell_test_fail'
MQ_TEST_CANCEL = 'trade_cancel_test'
MQ_CANCEL_ACK = 'trade_cancel_success'
MQ_CANCEL_NAK = 'trade_cancel_fail'

MQ_BUY_COM = 'trade_buy_filled'
MQ_SELL_COM = 'trade_sell_filled'
MQ_TEST_BUY_COM = 'trade_buy_test_filled'
MQ_TEST_SELL_COM = 'trade_sell_test_filled'
MQ_BUY_FAIL = 'trade_buy_fail'
MQ_SELL_FAIL = 'trade_sell_fail'
MQ_TEST_BUY_FAIL = 'trade_buy_test_fail'
MQ_TEST_SELL_FAIL = 'trade_sell_test_fail'

MQ_SHOW_PROCESS = 'trade_show_process'


MQ_TEST_UPDATE_MARGINE = 'trade_update_margine'

MQ_CHART_DAT_COLLECT_START = 'chart_collect_start'
MQ_CHART_DAT_COLLECT_STOP = 'chart_collect_stop'
MQ_CHART_GETTESTDB = 'chart_gettestdb'
MQ_CHART_GETTASK = 'chart_get_testinfo'
MQ_CHART_DAT_DELETE = 'chart_delete_db'
MQ_CHART_NOTI_TICK = 'chart_noti_tick'
MQ_CHART_NOTI_1MIN = 'chart_noti_1min'

# MQ Trans key
MQ_TR_ORDER = 'order'
MQ_TR_MARGIN = 'margine'
MQ_TR_CANCEL = 'order_cancel'

#trace =
STRAT = 'ST' # blue
API =  'API' # green
MQB = 'MQ' # red
DBB = 'DB' #yellow
TR = 'TRD'
TRADE = 'TRADE'

CONCh = 'CC'
CANDLE = 'WC' #wirte candle

CMN_SUCCESS = 0
CMN_ERR_INVALID_COIN_PRICE = 1
CMN_ERR_NOT_ENOUGH_MONEY = 2
CMN_ERR_INVALID_FORMAT = 3
CMN_ERR_INVALID_COIN = 4
CMN_ERR_REQ_FAIL = 5
CMN_ERR_DB_SELECT_ERROR = 6
CMN_ERR_DB_UPDATE_ERROR = 7
CMN_ERR_DB_INSERT_ERROR = 8
CMN_ERR_DB_CREATE_ERROR = 9
CMN_ERR_DB_DELETE_ERROR = 10
CMN_ERR_NO_APP_ID = 11
CMN_ERR_INVALID_ORDER_WAY = 12
CMN_ERR_TOO_MANY_TIMER = 13
CMN_ERR_ASSIGN_FAIL = 14
CMN_ERR_RESULT_FAIL = 15

CMN_ERR_CODE = {
    1 : 'Invalid Coin Price',
    2 : 'Not enough Money',
    3 : 'Invalid Format',
    4 : 'Invalid Coin',
    5 : 'Request Fail',
    6 : 'DB Select Error',
    7 : 'DB Update Error',
    8 : 'DB Insert Error',
    9 : 'DB Create Error',
    10 : 'DB Delete Error',
    11: 'No APP ID',
    12: 'Invalid Order way',
    13: 'Too Many Timer',
    14: 'Assign Fail',
    15: 'Result Fail',
}

WAY_KEY = 'way'
REAL_KEY = 'real'

TR_WAY_BUY = 0
TR_WAY_SELL = 1
TR_WAY_CANCEL = 2
TR_WAY_UPDATE = 3
TR_WAY_NULL = 255

TR_WAY_REAL = 0
TR_WAY_TEST = 1



C_END = "\033[0m"
C_BOLD= "\033[1m"
C_INVERSE = "\033[7m"
C_BLACK= "\033[30m"
C_RED = "\033[31m"
C_GREEN = "\033[32m"
C_YELLOW = "\033[33m"
C_BLUE = "\033[34m"
C_PURPLE = "\033[35m"
C_CYAN= "\033[36m"
C_WHITE= "\033[37m"
C_BGBLACK= "\033[40m"
C_BGRED= "\033[41m"
C_BGGREEN= "\033[42m"
C_BGYELLOW = "\033[43m"
C_BGBLUE= "\033[44m"
C_BGPURPLE = "\033[45m"
C_BGCYAN= "\033[46m"
C_BGWHITE= "\033[47m"

#trade and chart option
#mq  OPTION

OP_CURENCY = 1
OP_PRICE = 2
OP_QTY_TR_QT_PER = 3
OP_QTY_TR_QT = 4
OP_APP_ORDER_ID = 5
# OPTION Default

OP_DF_CURENCY = 'btc'
OP_DF_PRICE = 'last'
OP_DF_QTY = 'all'


#financial
TR_FEE = 0.05 # %
#---------------------------------------------------------------------------------------------
def findFileOndir(f_name):
    for file_name in os.listdir(os.getcwd()):
        if file_name == f_name:
            return True
    return False

def trace_print(f_name, str_to_write):


    for i in os.listdir(os.getcwd()):
        if i == 'log':
            res = 1
            break

    if res == 0:
        os.system('mkdir log')

    file_name_path = os.path.join(os.getcwd(), 'log', f_name)

    try:
        f = open(file_name_path, 'r+')
    except IOError as e:
        print(file_name_path, '  Created ')
        f = open(file_name_path, 'w+')

    f.read()
    pos = f.tell()
    f.seek(pos)
    string = (str_to_write + '\n')
    f.write(string)
    f.close()


def writeDataToFile(f_name, str_to_write):
        res = 0
        file_name = f_name + '-'+time.strftime('%Y-%m-%d')
        for i in os.listdir(os.getcwd()):
                if i == 'log':
                        res = 1
                        break

        if res == 0:
                os.system('mkdir log')

        file_name_path = os.path.join(os.getcwd(),'log',file_name)
        try:
                f = open(file_name_path, 'r+')
        except IOError as e:
                print(file_name_path, '  Created ')
                f=open(file_name_path,'w+')

        f.read()
        pos = f.tell()
        f.seek(pos)
        string = ('[INFO][%s]  ' % time.ctime() + str_to_write + '\n')
        f.write(string)
        f.close()

def mlog(strs, block=0):
        if block == STRAT:
                print('\x1b[1;34m{STRAT}%s\x1b[1;m' % strs)
        elif block == API:
                print('\x1b[1;32m{API}%s\x1b[1;m' % strs)
        elif block == MQB:
                print('\x1b[1;31m{MQB}%s\x1b[1;m' % strs)
        elif block == DBB:
                print('\x1b[1;33m{DBB}%s\x1b[1;m' % strs)
        elif block == TR:
                print('\x1b[1;35m{TR}%s\x1b[1;m' % strs)
        elif block == TRADE:
                print('\x1b[1;36m{TRADE}%s\x1b[1;m' % strs)
        else:
                print(strs)

        #writeDataToFile('log', str(strs))


def clog(strs, block=0):
        if block == CONCh:
                print('\x1b[1;36m%s\x1b[1;m' % strs)
        elif block == CANDLE:
                print('\x1b[1;41m%s\x1b[1;m' % strs)
        elif block == DBB:
                print('\x1b[1;33m%s\x1b[1;m' % strs)
        else:
                print(strs)

        writeDataToFile('chartlog', strs)


def print_query(str):
        print(str)

def print_time(threadName, delay):
        count = 5
        while count:
                time.sleep(delay)
                print(threadName, ' time : ',time.strftime('%H:%M:%S'))
                count -= 1


def generator_return2(n, m):
        j = 0
        for i in range(n, m):
                yield j, i
                j += 1

def generator_list(m):
        j = 0
        for i in m:
                yield j, i
                j += 1

def get_date_str_now():
        str_now = '%s' % datetime.now()
        return str_now

def checkHexString(str):
        try:
                nums = int(str,16)
                if str.find("0x") < 0:
                        return 'NULL'
                return nums
        except:
                return 'NULL'
def checkDecString(str):
        try:
                nums = int(str)
                return nums
        except:
                return 'NULL'

def get_db_string_by_type(value):
        if isinstance(value,Number):
                strs = "%s"%value
        elif value == None:
                strs = 'NULL'
        elif checkHexString(value) != 'NULL':
                strs = "%d" % checkHexString(value)
        elif checkDecString(value) != 'NULL':
                strs = "%d" % checkDecString(value)
        else:
                strs = "'%s'"%value
        return strs

def get_select_list_string(table1, table2, flag):
        #output_table 0 output both table
        #output_table 1 output table1
        #output_table 2 output table2
        if flag == 1:
                return str(table1) + '.' + '*'
        elif flag == 2:
                return str(table2) + '.' + '*'
        else:
                return '*'

def makeItemListToTupleForMemory(item_db_list):
        return_tup = {}
        i = 1
        for item_db_tup in item_db_list:
                return_tup[str(i)] = item_db_tup[1]
                i += 1
        return return_tup



def getPriceListFromJsonList(list):
        lists = []
        for i in list:
                lists.append(float(i['price']))
                lists.append(float(i['qty']))
        return lists

def get_msg_for_q(option,para1=0,para2=0,data_list=[]):

        return [option,para1,para2,data_list]

def make_app_orderid():
        return str(int(time.time()))


def checkisNumber(str):
    return str.replace('.', '', 1).isdigit()


def getTradeWay(key_option):
    _opt = key_option
    order_dic = {}

    # keyoption 에 order 단어 가 들어가 있으면 문제됨 ... key_option을 INT 로 만들 필요 있을듯 ?
    if key_option.find('trade') != 0:
        return key_option, {}

    opt_list = key_option.split('_')
    opt_list.pop(0)

    if opt_list[0] == 'buy' or opt_list[0] == 'sell':
        _opt = MQ_TR_ORDER
        for opt in opt_list:
            if opt == 'buy':
                order_dic[WAY_KEY] = TR_WAY_BUY

            elif opt == 'sell':
                order_dic[WAY_KEY] = TR_WAY_SELL

            elif opt == 'test':
                order_dic[REAL_KEY] = TR_WAY_TEST

    elif opt_list[0] == 'margine':
        _opt = MQ_TR_MARGIN
        for opt in opt_list:
            if opt == 'test':
                order_dic[REAL_KEY] = TR_WAY_TEST
            elif opt == 'update':
                order_dic[WAY_KEY] = TR_WAY_UPDATE
        if REAL_KEY not in order_dic: order_dic[REAL_KEY] = TR_WAY_REAL;

    elif opt_list[0] == 'cancel':
        _opt = MQ_TR_CANCEL
        for opt in opt_list:
            if opt == 'test':
                order_dic[REAL_KEY] = TR_WAY_TEST
        if REAL_KEY not in order_dic: order_dic[REAL_KEY] = TR_WAY_REAL;

    return _opt, order_dic


def makeMQmsgByTradeWay(function, way_dic, operation):
    # function - trade, chart ...
    # operation - complete, fail, during ...
    # waydic - buy, sell, test, real ...
    # 나중에 수정 필요
    mq_key_str = ''
    if way_dic[WAY_KEY] == TR_WAY_BUY or way_dic[WAY_KEY] == TR_WAY_SELL:
        order_way = ['buy', 'sell']
        real = [None, 'test']
        buy_or_sell = order_way[way_dic[WAY_KEY]]
        test_or_real = real[way_dic[REAL_KEY]]

        way_str = buy_or_sell if test_or_real == None else buy_or_sell + '_' + test_or_real
        mq_key_factor_list = [function, way_str, operation]
        mq_key_str = '_'.join(mq_key_factor_list)
    elif way_dic[WAY_KEY] == TR_WAY_UPDATE:
        way_str = 'update'
        mq_key_factor_list = [function, way_str, operation]
        mq_key_str = '_'.join(mq_key_factor_list)

    return mq_key_str




def getOptionFromMQ(strs):
    idx = 0
    para = {}
    while True:
        cur_idx = strs[idx:].find('-')
        if cur_idx < 0:
            return para

        so_idx = strs[idx+cur_idx+1:].find('-')

        if so_idx <0:
            command_data = strs[idx+cur_idx+1:]
        else:
            command_data = strs[idx+cur_idx+1:idx+cur_idx+so_idx]

        idx = idx + cur_idx + 1

        list = command_data.split()

        if len(list) >= 2:
            command = list[0]
            data = list[1].strip()

            if command == 'c':
                para[OP_CURENCY] = data
            elif command == 'p':
                para[OP_PRICE] = data
            elif command == 'qp':
                para[OP_QTY_TR_QT_PER] = data
            elif command == 'q':
                para[OP_QTY_TR_QT] = data
            elif command == 'o':
                para[OP_APP_ORDER_ID] = data

def getCurrencyFromOption(opt_dic):
    option_list = []
    if type(opt_dic) != dict:
        return 3

    if (OP_CURENCY in opt_dic) == False:
        return 4

    option_list.append(opt_dic[OP_CURENCY])
    return option_list

def dic_dump(obj, nested_level=0, output=sys.stdout):
        #list 도 가능 .. 코드는 IOT 코드에서
    spacing = '   '
    if type(obj) == dict:
        print('%s{' % ((nested_level) * spacing), file=output)
        for k, v in list(obj.items()):
            if hasattr(v, '__iter__'):
                if type(v) != list:
                    print('%s%s:' % ((nested_level + 1) * spacing, k), file=output)
                    dic_dump(v, nested_level + 1, output)
                else:
                    print('%s%s: %s' % ((nested_level + 1) * spacing, k, v), file=output)
            else:
                print('%s%s: %s' % ((nested_level + 1) * spacing, k, v), file=output)
        print('%s}' % (nested_level * spacing), file=output)
    else:
        print('%s%s' % (nested_level * spacing, obj), file=output)

if __name__ == "__main__":
    #para = getOptionFromMQ('-c btc -p 3000 -qp 1')
    #print para
    print(findFileOndir('TSLA.csv'))

