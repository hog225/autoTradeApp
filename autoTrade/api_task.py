# -*- coding: utf-8 -*-

from .trading_proc import *
from .timer_task import *
from .finance_equation import *


def task_api():
    print(C_BOLD + '**TASK API START**' + C_END)
    #APP_ORD_ID = 0
    while True:
        #sema_api.acquire()
        try:
            data = 0
            data = api_q.get_nowait()
            mlog('-----------task_api()--------------- ', API)
            option, para1, para2, info = data
            #APP_ORD_ID += 1

            if option == GET_LAST_PRICE:
                sec_option = para1
                src_block = para2
                cur, way_dic = info

                send_dat, err_num =apiGetLastPrice(cur)
                if err_num == CMN_SUCCESS:
                    trade_control_tx_q.put_nowait(get_msg_for_q(GET_LAST_PRICE_ACK, sec_option, src_block, [send_dat, cur,way_dic]))

            elif option == UPDATE_MARGINE:

                app_id = para1
                src_block = para2
                cur, way_dic = info

                send_dat, err_num = apiGetLastPrice(cur)
                if err_num == CMN_SUCCESS:
                    avr_margine_update(float(send_dat), cur, way_dic[REAL_KEY])
                    trade_control_tx_q.put_nowait(get_msg_for_q(UPDATE_MARGINE_ACK, app_id, src_block, ['margine_updated', cur, way_dic]))

            elif option == API_ORDER:

                app_id = para1
                src_block = para2
                price, qty, currency, way_dic = info
                trade_amount = round(price * qty)
                print('api trade_amount : ' + str(trade_amount) + ' qty : '+str(qty) + ' price'+ str(price))


                order_way = way_dic[WAY_KEY] if WAY_KEY in way_dic else TR_WAY_NULL
                real = way_dic[REAL_KEY] if REAL_KEY in way_dic else TR_WAY_TEST

                if order_way == TR_WAY_NULL:
                    mlog('Invalid Order Way', API)
                    trade_control_tx_q.put_nowait(get_msg_for_q(TR_ORDER_NAK, app_id, src_block, [way_dic, 12, price, qty, currency]))

                    Gsem_order.release()
                    gevent.sleep(0)
                    continue

                if len(PRO_LIST) > 10 or qty <= 0 or price <= 0:
                    trade_control_tx_q.put_nowait(get_msg_for_q(TR_ORDER_NAK, app_id, src_block, [way_dic, 13, price, qty, currency]))

                    Gsem_order.release()
                    gevent.sleep(0)
                    continue

                result = request_order_api(price, qty, currency, order_way, real)

                str_dat = 'order way [%d] 0:BUY 1:SELL, real [%d] 0:REAL 1:TEST ' % (order_way, real)
                str_dat += 'price, qty, currency: '+ str(info)+ ' orderid:'+ str(result.get('orderId'))
                mlog(str_dat, API)

                if result["result"] == 'success' and result.get("orderId") != None:
                    if order_way == TR_WAY_BUY:
                        update_used_balance(trade_amount, real)
                        balance_info_update(KRW, -trade_amount, 0, real)
                    elif order_way == TR_WAY_SELL:
                        balance_info_update(currency, -qty, 0, real)

                    # 아래 프로세스 사용시 좀 문제가 되는듯 하지만 나중에 쓸지 모르니 남겨두기
                    #p = Process(target=task_process_api, name=app_id, \
                    #            args=(app_id, result["orderId"], currency, PRO_Q, order_way, src_block, price, qty))
                    #p.daemon = True
                    #p.start()
                    # process 가 시작 될 때까지 기다림

                    ttmr = tradeTimer(ORDER_FILL_CHK_TIME, task_process_api, \
                                     app_id, result["orderId"], currency, PRO_Q, order_way, src_block, price, qty)

                    ttmr.setName(app_id)
                    ttmr.start()

                    while ttmr.is_alive() == False:
                        gevent.sleep(0)

                    if ttmr.is_alive() == True:
                        # TODO 아래 변경 필요
                        order_app_id_write(app_id, real)
                        #update_order_history_by_appid(app_id, [ORDDERID], [result["orderId"]], real)
                        order_history_update(app_id, result["orderId"], way_dic[WAY_KEY],
                                             currency, qty, price, TR_FEE, STR_VOTE[VOTE_DURING], real)

                        PRO_LIST.append(ttmr)
                        trade_control_tx_q.put_nowait(get_msg_for_q(TR_ORDER_ACK, app_id, src_block, [way_dic, result["orderId"]]))
                    else:
                        print('task_process_api assign Fail')
                        trade_control_tx_q.put_nowait(get_msg_for_q(TR_ORDER_NAK, app_id, src_block, [way_dic, 14, price, qty, currency]))


                        if order_way == TR_WAY_BUY:
                            update_used_balance(-trade_amount, real)
                            balance_info_update(KRW, trade_amount, 0, real)
                        elif order_way == TR_WAY_SELL:
                            balance_info_update(currency, -qty, 0, real)
                else:
                    trade_control_tx_q.put_nowait(get_msg_for_q(TR_ORDER_NAK, app_id, src_block, [way_dic, 15, price, qty, currency]))
                    strs = 'error Order result :'+str(result.get("result"))+', orderId:'+str(result.get("orderId"))
                    mlog(strs,API)

                    if order_way == TR_WAY_BUY:
                        update_used_balance(-trade_amount, real)
                        balance_info_update(KRW, trade_amount, 0, real)
                    elif order_way == TR_WAY_SELL:
                        balance_info_update(currency, -qty, 0, real)

                Gsem_order.release()
                # order id 확인 되면 db에 저장 요청시 수익률 공개

            elif option == CANCEL_ORDER:
                cur, way_dic = info
                app_id = para1
                src_block = para2

                real = way_dic[REAL_KEY] if REAL_KEY in way_dic else TR_WAY_TEST
                way_dic[WAY_KEY] = TR_WAY_CANCEL
                data_list = read_order_history_by_appid(app_id, real)
                if data_list == None:
                    trade_control_tx_q.put_nowait(get_msg_for_q(TR_ORDER_NAK, app_id, src_block,[way_dic, CMN_ERR_DB_SELECT_ERROR, 0, 0, cur]))
                    continue

                orderId = data_list[3]
                result = cancel_order_all_api(orderId, cur, real)

                if result.get('result') == 'success':
                    result = delete_pro_list(app_id)
                    qty = get_approc_val(float(data_list[6]))
                    price = float(data_list[7])
                    trade_amount = round(qty * price)
                    balance_info_update(KRW, trade_amount, 0, real)
                    update_used_balance(-trade_amount, real)
                    update_order_history_by_appid(app_id, [VOTE], [STR_VOTE[VOTE_CANCEL]], real)

                    if result == True:
                        trade_control_tx_q.put_nowait(get_msg_for_q(TR_ORDER_ACK, app_id, src_block,[way_dic, None]))
                    else:
                        trade_control_tx_q.put_nowait(get_msg_for_q(TR_ORDER_NAK, app_id, src_block,[way_dic, CMN_ERR_RESULT_FAIL, 0, 0, cur]))
                        mlog('Cancel Order Success But ther are no Timer', TR)
                else:
                    trade_control_tx_q.put_nowait(get_msg_for_q(TR_ORDER_NAK, app_id, src_block, [way_dic, CMN_ERR_RESULT_FAIL, 0, 0, cur]))
                    mlog('Cancel Order Fail', TR)

        except NQueue.Empty:
            pass
        gevent.sleep(0)
        #sema_api.release()



def balance_update(limit_balance, stop_flag):
    balanc_dic = request_balance_api()
    if balanc_dic.pop("result") == "success":
        for coin_key in list(balanc_dic.keys()):
            if len(coin_key) < 5:
                balance_info_inital(str(coin_key), float(balanc_dic[coin_key]["avail"]), float(balanc_dic[coin_key]["balance"]))
        krw_wr_init_balance(limit_balance)



if __name__ == "__main__":
    update_order_history_by_appid(4, ['orderid'], ['test343434'], TR_WAY_TEST)
    '''
    json_dat = get_response_val_by_key(ORDER_BOOK, 'GET', params=currency_params)

    print "가장 높은 판매가격 "+ str(json_dat["ask"][-1])
    print "가장 낮은 판매가격 "+ str(json_dat["ask"][0])
    print "가장 높은 구매가격 " + str(json_dat[BID][0])
    print "가장 낮은 구매가격 " + str(json_dat[BID][-1])
    print json_dat["result"]
    '''
    #json_dat = get_response_val_by_key(TICKER, 'GET', params=get_currency_params('iota'))
    #print json_dat

    #test_order_history_insert('3', 1, 'btc',0.1, 12000000, 0.05)
    #json_dat = get_response_val_by_key(TICKER, 'GET', params=get_currency_params('iota'))
    #print json_dat

    #print request_private_api(BALANCE,BALANCE_PAYLOAD)

    #print request_private_api(ORDERINFO, COMORDER_PAYLOAD)
    #json_dat =  get_response_val_by_key(CURRENT_PRICE, 'GET', params=currency_params)
    #print request_buy_api(5000,0.1,"btc")
    #balance_update(1,200000.0)
    #print b["krw"]


