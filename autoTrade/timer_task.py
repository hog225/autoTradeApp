import threading
import time
from .common_func import *
import gevent
from .db_lib import *


_TIMEOUT = 1
_TIMERLS = 2
_RLEASED = 0
TIMER_COUNT_1S = 0

TIMER_RLS_COUNT = 5
ORDER_FILL_CHK_TIME = 30

class eventTimer(threading.Thread):

    def __init__(self, TimeOut, function, *args):
        threading.Thread.__init__(self)
        self.event = threading.Event()
        self.thread_event = threading.Event()
        self.timeout = TimeOut
        self.timer_m1_count = 60/TimeOut
        self.timer_m1_triger = 0
        self.timer_age = 0
        self.function = function

        self.args = tuple(value for _, value in enumerate(args))
        self.args = self.args
        #self.start_time = time.time()
        # disconnect_sock


    def run(self):
        print('start timer 1 Minute timer %d' % self.timer_m1_count)
        while self.thread_event.isSet() == False:

            #self.start_time = time.time()
            self.event.wait(self.timeout)

            if self.timer_m1_count != 0:
                self.timer_m1_triger += 1
                if self.timer_m1_triger == self.timer_m1_count:
                    self.timer_age += 1
                    self.function(*self.args + (self.timer_age,1))
                    self.timer_m1_triger=0
                    if self.timer_age >= 1440:
                        self.timer_age = 0
                else:
                    self.function(*self.args + (self.timer_age,0))
            else:
                self.function(*self.args + (self.timer_age, 0))

            self.event.clear()


    # def start(self):
    #    print 'start thread'

    def stop(self):
        print('stop thread')
        self.event.set()

    def kill(self):
        print('kill timer')
        self.thread_event.set()


class tradeTimer(threading.Thread):

    def __init__(self, TimeOut, function, *args):
        threading.Thread.__init__(self)
        self.event = threading.Event()
        self.thread_event = threading.Event()
        self.timeout = TimeOut
        self.function = function
        self.timer_rls_cnt = TIMER_RLS_COUNT
        self.stop_flag = 0
        self.args = tuple(value for _, value in enumerate(args))
        self.args = self.args


    def run(self):
        print('start timer')
        while self.thread_event.isSet() == False:

            # self.start_time = time.time()
            if self.timer_rls_cnt == 0:
                self.stop_flag = 1

            if self.function(self.stop_flag, *self.args) == 0:
                return

            self.timer_rls_cnt -= 1




            self.event.wait(self.timeout)

            self.event.clear()

    # def start(self):
    #    print 'start thread'

    def stop(self):
        print('tradeTimer STOP')
        self.event.set()

    def kill(self):
        print('tradeTimer KILL')
        self.thread_event.set()





#def candle_call_timer(candle_val, stop_flag):
#    candle_q.put_nowait(candle_val)



if __name__ == "__main__":
    tmlist = []
    def helloTimer(stop, a, b, c):
        strs = ','.join((a,b,c))
        print('Function Called ' + strs + '   '+str(stop))




    tmr = tradeTimer(10, helloTimer, 'hi', 'func', 'power')
    tmr.start()
    tmr.setName(32)
    tmlist.append(tmr)

    while True:
        try:
            chat = input('\ninput : ')
            print(chat)
            if chat == 'set':
                tmr.stop()
            elif chat == 'kill':
                tmr.kill()
                #tmr.stop()
            elif chat == 'name':
                for i in tmlist:
                    print(i.getName())
                    print(i.isAlive())
                    print(threading.enumerate())
        except (KeyboardInterrupt, SystemExit):
            tmr.kill()
            exit()



