# -*- coding: utf-8 -*-
from autoTradeApp.common_func import *
from autoTradeApp.timer_task import *
from autoTradeApp.mqtt_task import *
from autoTradeApp.db_chart_lib import *
from autoTradeApp.pandas_func import *
import gevent
import ast

def initit():
    # 초기 Initalize Process
    # 금일 살만한 주식을 찾는다 .. 등등등 나중에 필요 할 듯
    # 모니터링 하고있는 코인들의 차트를 웹이든 어디든 불러온뒤 기술적 분석을 하여 매수 매도 포인트를 찾음
    mon_list = selectChartMonList()
    for mon_col in mon_list:
        currency = mon_col[2]
        pd = getCoinChartFromWeb(currency, 100) # 현제로부터 100일 사이의 데이터 를 가져옴
    pass

def my_strategy():
    print('\x1b[1;34m**STRATEGY START**\x1b[1;m')

    while True:
        gevent.sleep(0)
        try:

            data = stra_q.get_nowait()
            option, para1, para2, data_list = data
            # m1_trigger, timer_age = data_list
            print('\x1b[1;34m get Coin chart %s, %s, %s,  \x1b[1;m' % (str(option), str(para1), str(para2)))
            if para1 == MQB:
                str_app_id, str_data_dic = data_list.split(',', 1)
                # str_app_id 쓸려면 String 으로 디코드 해야 함 (App_id=0 이런식으로 옴 )
                data_dic = ast.literal_eval(str_data_dic.strip())
                if option == MQ_CHART_NOTI_TICK: # 10Sec timer
                    pass
                    print('Ticker From Chart Thread CURRENCY = %s ' % (data_dic['currency']))
                    #print data_list['last']

                elif option == MQ_CHART_NOTI_1MIN:
                    pass



            elif EVENT_SIGNAL:
                #st_mqtt_q.put_nowait(get_msg_for_q(BUY))
                #print 'Signal'
                pass

        except NQueue.Empty:
            pass

def give_timer_signal(timer_age, m1_triger):
    stra_q.put_nowait(get_msg_for_q(EVENT_SIGNAL))
    print('\x1b[1;33m10 Sec Timer\x1b[1;m')


