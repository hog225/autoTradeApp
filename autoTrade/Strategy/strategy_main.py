# -*- coding: utf-8 -*-

from autoTradeApp.mqtt_task import *
from .strategy_func import *
from gevent.pool import Pool
import gevent.monkey
gevent.monkey.patch_all()


if __name__ == "__main__":
    #Currency_monitor DB 에 있는 거 선택하여 모니터링
    print('\x1b[1;35m------Strategy Task Start------\x1b[1;m')

    #tmr = eventTimer(10, give_timer_signal)
    #tmr_name = 'signal_timer'
    #tmr.setName(tmr_name)
    #tmr.start()

    while True:
        try:
            gevent.joinall([
                gevent.spawn(my_strategy),
                gevent.spawn(mqtt_subs_thread, MSG_URL_STRATEGY),
                gevent.spawn(mqtt_pub_thread)
            ])

        except (KeyboardInterrupt, SystemExit):
            print('Service End')
            exit()
