# coding=utf-8
import mysql.connector as mariadb
from mysql.connector import errorcode
import datetime
from .finance_equation import *
from .common_func import *

from .pandasLib import *

DATABASE = 'chart_dat'
USER = 'yghong'
PASSWORD = 'ghddudrl'


def check_table_exist(table_name):
    tup_list = []
    #sema_db.acquire()
    mariadb_connection = mariadb.connect(user=USER, password=PASSWORD, database=DATABASE)
    cursor = mariadb_connection.cursor()

    query = "show tables like '%s'" % table_name
    cursor.execute(query)

    for db_tupe in cursor:
        tup_list.append(db_tupe)

    mariadb_connection.close()
    #sema_db.release()
    return tup_list[0]


def db_select(table,col,key):
    #print "%0.5f" % time.time()
    return_DB = []
    #sema_db.acquire()

    mariadb_connection = mariadb.connect(user=USER, password=PASSWORD, database=DATABASE)
    cursor = mariadb_connection.cursor()

    if type(key) == int:
        query = "SELECT * FROM " + table + " WHERE " + col + "=" + str(key)
    else:
        query = "SELECT * FROM "+table+" WHERE "+col+"="+'"'+key+'"'
    try:
        cursor.execute(query)
    except:
        print('Query Fail')
        pass

    for db_tuple in cursor:
        return_DB.append(db_tuple)

    mariadb_connection.close()
    #print "%0.5f" % time.time()
    #sema_db.release()

    return return_DB


def select_for_candle(table,count):
    #print "%0.5f" % time.time()
    return_DB = []
    #sema_db.acquire()

    mariadb_connection = mariadb.connect(user=USER, password=PASSWORD, database=DATABASE)
    cursor = mariadb_connection.cursor()


    query = "SELECT * FROM " + table + " ORDER BY ID DESC LIMIT " + str(count)

    try:
        cursor.execute(query)
    except:
        print('Query Fail')
        pass

    for db_tuple in cursor:
        return_DB.append(db_tuple)

    mariadb_connection.close()
    # print "%0.5f" % time.time()
    #sema_db.release()

    return return_DB


def db_select_join_by_id(table1, table2,table2_forign_col,id,want_col_str=0):

    return_DB = []
    #sema_db.acquire()

    mariadb_connection = mariadb.connect(user=USER, password=PASSWORD, database=DATABASE)
    cursor = mariadb_connection.cursor()

    query = "SELECT " + "*" + \
        " FROM " + str(table1) + " JOIN " + str(table2) + " ON " +\
        str(table1)+'.'+'id'+'='+str(table2_forign_col) + \
        " WHERE " + str(table1)+'.'+'id' + "=" + str(id)

    try:
        cursor.execute(query)
    except:
        print('Query Fail')
        pass

    field_name = [i[0] for i in cursor.description] #print colum of selec# t
    # print field_name

    try:
        # 문제점 : col 리스트에 중복 데이터가 들어갈 수 있다 id 가 두개 들어간다 던지 나중에 처리 필요
        want_index = field_name.index(want_col_str)
        for db_tuple in cursor:
            return_DB.append(db_tuple[want_index])

    except:
        for db_tuple in cursor:
            return_DB.append(db_tuple)

    for i in return_DB:
        print(i)

    mariadb_connection.close()
    #sema_db.release()

    return return_DB



def db_select_inner_join(table1, table2, key1, key2, com_key_value, output_table_flag=2):
    #output_table 0 output both table
    #output_table 1 output table1
    #output_table 2 output table2
    return_DB = []
    #sema_db.acquire()

    mariadb_connection = mariadb.connect(user=USER, password=PASSWORD, database=DATABASE)
    cursor = mariadb_connection.cursor()


    query = "SELECT " + get_select_list_string(table1, table2, output_table_flag) + \
        " FROM " + str(table1) + " INNER JOIN " + str(table2) + " ON " +\
        str(table1) + '.'+ str(key1)+ '=' + str(table2) + '.'+ str(key2) + ' and ' + \
        str(table1) + '.' + str(key1) + '=' + get_db_string_by_type(com_key_value)

    try:
        cursor.execute(query)
    except:
        print('Query Fail')
        pass


    for db_tuple in cursor:
        return_DB.append(db_tuple)

    mariadb_connection.close()

    #sema_db.release()

    return return_DB


def db_select_all(table):
    return_DB = []
    #sema_db.acquire()

    mariadb_connection = mariadb.connect(user=USER, password=PASSWORD, database=DATABASE)
    cursor = mariadb_connection.cursor()

    query = "SELECT * FROM "+table

    try:
        cursor.execute(query)
    except:
        print('Query Fail')
        pass

    for db_tuple in cursor:
        return_DB.append(db_tuple)

    mariadb_connection.close()

    #sema_db.release()

    return return_DB

def db_select_all_to_pd(table):
    # sema_db.acquire()
    df = 0
    mariadb_connection = mariadb.connect(user=USER, password=PASSWORD, database=DATABASE)
    cursor = mariadb_connection.cursor()

    query = "SELECT * FROM " + table

    try:
        df = pd.read_sql(query, mariadb_connection)
    except:
        mariadb_connection.close()
        return 0

    mariadb_connection.close()

    # sema_db.release()

    return df


def db_select_forign(table, forn_table, key_col ,key, forn_key_col):
    return_DB = []
    #sema_db.acquire()

    mariadb_connection = mariadb.connect(user=USER, password=PASSWORD, database=DATABASE)
    cursor = mariadb_connection.cursor()
    #get_db_string_by_type


    query = "SELECT * FROM "+table+\
            " LEFT OUTER JOIN "+forn_table+\
            " ON " + table +'.id'+' = '+ forn_table +'.'+forn_key_col+\
            " WHERE "+key_col+"="+get_db_string_by_type(key)

    try:
        cursor.execute(query)
    except:
        print('Query Fail')
        pass

    for db_tuple in cursor:
        print(db_tuple)
        return_DB.append(db_tuple)

    mariadb_connection.close()
    #print "%0.5f" % time.time()
    #sema_db.release()

    return return_DB


def db_insert(table, col_name_tuple, value_tuple):

    insert_id = 0
    #sema_db.acquire()

    mariadb_connection = mariadb.connect(user=USER, password=PASSWORD, database=DATABASE)
    cursor = mariadb_connection.cursor()

    #insert_query = "INSERT INTO %s %s VALUES %s" % (table, str(col_name_tuple), str(value_tuple))
    #insert_query = "INSERT INTO %s (device_id, binding_method, create_date, modify_date) VALUES %s" \
    #               % (table , str(value_tuple))
    insert_query = "INSERT INTO " + table + " ("+ ','.join(col_name_tuple) +") " + "VALUES " + str(value_tuple)

    #print_query( insert_query)
    db_str  = ' DB Insert %s ' % table
    mlog(db_str, DBB)

    try:
        cursor.execute(insert_query)
        insert_id = cursor.lastrowid

        mariadb_connection.commit()

    except mariadb.Error as err:
            print((err.msg))
    else:
        pass

    mariadb_connection.close()
    #print "%0.5f" % time.time()
    #sema_db.release()

    return insert_id

def db_update_single(table, col_name,value,key,key_val):

    update_id = 0
    #sema_db.acquire()

    mariadb_connection = mariadb.connect(user=USER, password=PASSWORD, database=DATABASE)
    cursor = mariadb_connection.cursor()

    update_query = \
        "update " + table + " "\
        "SET "+ col_name+"="+get_db_string_by_type(value) + " " + \
        "WHERE " + str(key) +'='+get_db_string_by_type(key_val)

    #print_query( update_query)

    db_str  = ' DB Update Single %s ' % table
    mlog(db_str, DBB)

    try:
        cursor.execute(update_query)
        update_id = cursor.lastrowid

        mariadb_connection.commit()
    except mariadb.IntegrityError as e:
        if not e[0] == 1062:
            raise
        else:
            print("MY ERROR 1062: " + e[1])
        return False


    mariadb_connection.close()
    #print "%0.5f" % time.time()
    #sema_db.release()

    return update_id

def db_update(table, col_name_tuple,value_tuple,key,key_val):

    update_id = 0
    #sema_db.acquire()

    mariadb_connection = mariadb.connect(user=USER, password=PASSWORD, database=DATABASE)
    cursor = mariadb_connection.cursor()

    assignment_list = []
    for index, col_name in generator_list(col_name_tuple):
        assignment_list.append(col_name + '=' + get_db_string_by_type(value_tuple[index]))
    #print assignment_list

    update_query = \
        "update " + table + " "\
        "SET "+ ','.join(assignment_list) + " " + \
        "WHERE " + str(key) +'='+get_db_string_by_type(key_val)

   # print_query( update_query)
    db_str  = ' DB Update %s ' % table
    mlog(db_str, DBB)

    try:
        cursor.execute(update_query)
        update_id = cursor.lastrowid

        mariadb_connection.commit()
    except mariadb.IntegrityError as e:
        if not e[0] == 1062:
            raise
        else:
            print("MY ERROR 1062: " + e[1])
        return False


    mariadb_connection.close()
    #print "%0.5f" % time.time()
    #sema_db.release()

    return update_id



def insertOrderBook(json_str):
    fail_val = 0
    try:
        if json_str["result"] == "success":
            int_ts = int(json_str["timestamp"])
            date_str = datetime.fromtimestamp(int_ts )
            times = '%s' % date_str
            #print json_dat["ask"][:5]
            #ts = 'FROM_UNIXTIME' + '('+json_dat["timestamp"]+')'
        else:
            mlog("json result is not success ", DBB)
            fail_val = 1
    except:
        mlog("invalid json_str !",DBB)
        mlog(str(json_str), DBB)
        fail_val = 1

    if fail_val == 1:
        tmp_list = select_for_candle(ASKL, 1)
        ask_low_val_tup = tmp_list[0][2:]
        tmp_list = select_for_candle(ASKH, 1)
        ask_high_val_tup = tmp_list[0][2:]
        tmp_list = select_for_candle(BIDL, 1)
        bid_low_val_tup = tmp_list[0][2:]
        tmp_list = select_for_candle(BIDH, 1)
        bid_high_val_tup = tmp_list[0][2:]

        db_insert('askLow',PRICE_TUP_NO_TS,ask_low_val_tup)
        db_insert('askHigh', PRICE_TUP_NO_TS, ask_high_val_tup)
        db_insert('bidHigh', PRICE_TUP_NO_TS, bid_high_val_tup)
        db_insert('bidLow', PRICE_TUP_NO_TS, bid_low_val_tup)
    else:
        ask_low_val_tup = tuple( [times] +  getPriceListFromJsonList(json_str[ASK][-5:]) )
        ask_high_val_tup = tuple([times] + getPriceListFromJsonList(json_str[ASK][:5]))
        bid_low_val_tup = tuple([times] + getPriceListFromJsonList(json_str[BID][-5:]))
        bid_high_val_tup = tuple([times] + getPriceListFromJsonList(json_str[BID][:5]))

        db_insert('askLow',PRICE_LOW_TUP,ask_low_val_tup)
        db_insert('askHigh', PRICE_LOW_TUP, ask_high_val_tup)
        db_insert('bidHigh', PRICE_LOW_TUP, bid_high_val_tup)
        db_insert('bidLow', PRICE_LOW_TUP, bid_low_val_tup)

def check_data_validation(ts_data_list, time_interval):
    for ts in range(len(ts_data_list)-1):
        if ts_data_list[ts] - timedelta(seconds=time_interval) != ts_data_list[ts+1]:
            return False
    return True


def getCandleDataFromDB():
    #함수 이름은 소문자 인게 좋데
    print(getCandleDataFromDB.__name__)
    #db_select()


def db_to_csv(db_name):
    #'btc_1M_CANDLE'
    # //minute 이냐 daily 냐 정할 필요 있을 듯
    # linux 에서 실행 해야 함
    file_name= db_name + '.csv'
    file_path = os.path.join(os.getcwd(), 'toolEX', 'csv_file', 'minute', file_name)
    dir_path = os.path.join(os.getcwd(), 'toolEX', 'csv_file', 'minute')
    print(file_path)
    if not os.path.isdir(dir_path):
        os.mkdir(dir_path)

    df = db_select_all_to_pd(db_name)
    df.index = df['ts'].apply(pd.to_datetime)
    df.index.names = ['Date']

    tmp_df = df.loc[:, 'open':]

    #tmp_df = tmp_df.resample('1Min', how='last')
    tmp_df = tmp_df.tz_localize("UTC")
    df = tmp_df

    pickle_name = db_name
    DfToPickle(df, pickle_name)

    print(df.info())

    df.to_csv(file_path)
    #print (df.info(), end='')

if __name__=="__main__":
    #create_init_db()
    #pass
    #db_select_inner_join(DB_TBL_CLNDEV, DB_TBL_CLNDEV_ITEM, 'id', DB_CLN_ID, 12)
    #candle_write(M_1_CANDLE * 60 / 10)
    #candle_write_using_candle_db(M1_C,M3_C,3,60)
    #getCandleDataFromDB()
    #db_insert(DB_TBL_CLNDEV_ITEM,(DB_ITEM,DB_CLN_ID),(ITEM_TYPE_LED,10))

    #insertOrderBook('dfdfd')
    #candle_write(M_1_CANDLE*60/10)
    #order_write(BUY_TBL,'8a82c561-40b4-4cb3-9bc0-9ac9ffc1d63b','btc','34343.23','0.0003','filled')
    #avr_margine_write(100,10,BTC)
    #test_avr_margine_update(12000000,BTC)
    #test_avr_margine_write(12,-0.3702,'btc')
    #balance_info_list = db_select_all(BALANCE_INFO_TBL)
    #print balance_info_list
    #avr_margine_write(3434, 3434, 'btc', TR_WAY_TEST)
    #check_table_exist('M_10_CANDLE')
    db_to_csv('btc_1M_CANDLE')
    #df = db_select_all_to_pd('btc_ticker')
    #print df.info()