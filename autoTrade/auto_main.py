import gevent.monkey
gevent.monkey.patch_all()
from .api_task import*
from .timer_task import *
from .trading_controler import *
from .mqtt_task import *
from .trading_proc import *



LIMIT_BALANCE= 200000

if __name__ == "__main__":

    # balance_update(1, LIMIT_BALANCE)
    # balance_tmr = eventTimer(60*60*6, balance_update, LIMIT_BALANCE)
    # balance_tmr.start()


    delete_trade_db()
    create_init_db()

    #balance_info_inital(KRW, 200000000, 200000000, TR_WAY_TEST)
    #krw_wr_init_balance(400000000, TR_WAY_TEST)
    balance_info_inital(KRW, 200000, 200000, TR_WAY_TEST)
    krw_wr_init_balance(400000000, TR_WAY_TEST)
    print(check_balance(KRW, TR_WAY_TEST))
    while True:
        try:
            gevent.joinall([
                gevent.spawn(task_api),
                gevent.spawn(task_trading_proc),
                gevent.spawn(task_trading_controller_rx),
                gevent.spawn(task_trading_controller_tx),
                gevent.spawn(mqtt_subs_thread, MSG_URL_TRADING),
                gevent.spawn(mqtt_pub_thread)
            ])

        except (KeyboardInterrupt, SystemExit):
            print('Service End')
            #balance_tmr.kill()
            exit()

'''
#test process
if __name__ == "__main__":
	p = Process(target=task_process_test,name='order_id', args=('hi',proc_queue))
	p.daemon = True
	p.start()
	print p.name
	print p.is_alive()

	while True:
		try:
			gevent.joinall([
				gevent.spawn(task_api_test),
			])

		except (KeyboardInterrupt, SystemExit):
			print 'Service End'
			exit()
'''