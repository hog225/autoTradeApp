import paho.mqtt.client as mqtt
import gevent
import os
from .common_func import *

MQ_SERVER = '127.0.0.1'
MQ_PORT = 1883
MQ_KEEPALIVE = 60


MSG_URL_ROOT ='autotrade/'
MSG_ROOT = 'autotrade'
MSG_URL_TRADING = 'trading'
MSG_URL_STRATEGY = 'strategy'
MSG_URL_CHART = 'chart'
MSG_URL_CLIENT = 'client'



#mqtt_code
#---------------------------------------------------------------------------------------------
def addKeyToMqttKeyInfo(mqtt_key_list, _key):
    for key in mqtt_key_list:
        if key == _key:
            return False

    mqtt_key_list.append(_key)
    return True

def delKeyToMqttKeyInfo(mqtt_key_list, _key):
    try:
        mqtt_key_list.remove(_key)
        return True
    except:
        return False


def on_connect(client, userdata, flags, rc):
    print(("Connencted with result code "+str(rc)))
    client.subscribe(MSG_URL_ROOT+userdata+'/#')

def MQTT_publish(key, msg_url, pub_str):
    mqttc = mqtt.Client()
    mqttc.connect("127.0.0.1", 1883)

    clnt_path = os.path.join(MSG_URL_ROOT+msg_url, key)
    print(clnt_path)
    mqttc.publish(clnt_path, pub_str)

def on_message(client, userdata, msg):
    tmp_path = msg.topic
    root_path = tmp_path.split('/')
    if root_path[0] == MSG_ROOT and len(root_path) >= 4:
        #msg_name = os.path.basename(msg.topic)
        thread_name = root_path[1]
        mq_from = root_path[2]
        key = root_path[3]

        strings = msg.topic + ":" + str(msg.payload)
        #mlog(strings, MQB)


        sema_mqtt_sub.acquire()
        if thread_name == MSG_URL_CHART and msg.payload != '':
            chart_control_q.put_nowait(get_msg_for_q(key, para1 = MQB, para2=mq_from, data_list=msg.payload))
        elif thread_name == MSG_URL_TRADING and msg.payload != '':
            trade_control_rx_q.put_nowait(get_msg_for_q(key, para1 = MQB, para2=mq_from, data_list=msg.payload))
        elif thread_name == MSG_URL_STRATEGY and msg.payload != '':
            stra_q.put_nowait(get_msg_for_q(key, para1 = MQB, para2=mq_from, data_list=msg.payload))
        elif thread_name == MSG_URL_CLIENT and msg.payload != '':
            pass
        else:
            mlog('Invalid MQ URL or no payload', MQB)
        sema_mqtt_sub.release()




def mqtt_subs_thread(my_msg_url):
    print(C_BOLD + '**MQTT SUB START**' + C_END)

    client = mqtt.Client(userdata=my_msg_url)

    client.on_connect = on_connect
    client.on_message = on_message

    client.connect(MQ_SERVER, MQ_PORT, MQ_KEEPALIVE)
    client.loop_start()
    while True:
        gevent.sleep(0)


def mqtt_pub_thread():
    print(C_BOLD + '**MQTT PUB START**' + C_END)
    while True:
        try:
            data = mqtt_q.get_nowait()
            mlog('------------MQTT PUB -----------', MQB)
            mq_key, para1, msg_url, msg = data #para1 is app_id,
            if msg == []:
                msg = None

            MQTT_publish(mq_key, msg_url, 'App_id='+str(para1) +', ' +str(msg))


        except NQueue.Empty:

            pass
        gevent.sleep(0)