# -*- coding: utf-8 -*-
import requests
import json
import base64
import hashlib
import hmac
import httplib2
import time
from .common_func import *
import random

#API info-------------COIN ONE ---------------------------
API_HOST = 'https://api.coinone.co.kr'
API_NAME = 'yeonggiAPI'
API_ACC_TOKEN = 'dc58e134-1181-43d1-a1fd-17eac4443451'
API_PRI_KEY = '7628c044-032d-4eed-b876-e741a5cb510a'
API_VERSION = 'v2'

#path
CURRENT_PRICE = '/trades/'
ORDER_BOOK = '/orderbook/'
TICKER = '/ticker/'

#balance check
BALANCE = API_HOST+'/'+API_VERSION +'/account/balance/'
D_BALANCE = API_HOST+'/'+API_VERSION +'/account/daily_balance/'
BALANCE_PAYLOAD = {
  "access_token": API_ACC_TOKEN,
  #"currency": "iota"
}

#complete order
COMORDER = API_HOST+'/'+API_VERSION +'/order/complete_orders/'
COMLIMITORDER = API_HOST+'/'+API_VERSION +'/order/limit_orders/'
ORDERINFO = API_HOST+'/'+API_VERSION +'/order/order_info/'
COMORDER_PAYLOAD = {
  "access_token": API_ACC_TOKEN,
}

#buy order
BUYURL = API_HOST+'/'+API_VERSION +'/order/limit_buy/'
SELLURL = API_HOST+'/'+API_VERSION +'/order/limit_sell/'
CNACELORDER = API_HOST+'/'+API_VERSION +'/order/cancel/'
CNACELORDERALL = API_HOST+'/'+API_VERSION +'/order/cancel_all/'

BUY_PAYLOAD = {
  "access_token": API_ACC_TOKEN,
  "price": 500000,
  "qty": 0.1,
  "currency": "btc"
}

#response key
#param
currency_params = {'currency':'btc'}
currency_period_params = {'currency':'btc', 'period':'hour'}
#---------------------------------------------COINONE---

#orderbook
'''
"currency": "btc",
"errorCode": "0",
"result": "success",
"timestamp": "1512571446"
'''
def get_currency_params(currency):
    return {'currency': '%s'% currency}

def req(path, method, params={}):
    url = API_HOST + path
    print(('HTTP Method: %s' % method))
    start_time = time.time()
    if 'GET' == method:
        try:
            res = requests.get(url, params=params, timeout = 8)
        except:
            res = 0
    #elif 'POST' == method:
    #    req = urllib2.Request(API_HOST + path, data)
    #req.add_header('Authorization', APP_KEY)
    tstr = (time.time() - start_time)
    strs = 'Request URL: %s   %0.5f' % (url,tstr)

    if tstr > 8:
        mlog(strs,API)
    else:
        print(strs)
    return res

def get_response_val_by_key(path, method, keys = None,params={}):

    res = req(path, method, params=params)
    if res==0:
        mlog('chart data request fail no json data',API)
        return {}

    if res.status_code == 200:
        if keys == None:
            #print json.dumps( res.json(), sort_keys=True, indent=4, separators=(',', ': '))
            try:
                json_str = res.json()
                return json_str
            except:
                mlog('chart data request fail no json data',API)
                return {}
        else:
            try:
                json_s = res.json()
                return json_s[keys]
            except:
                mlog('chart data request fail no json data',API)
                return {}

    else:
        strings = 'chart data request fail error code %d ' % res.status_code
        mlog(strings,API)
        return {}


def get_encoded_payload(payload):
  payload['nonce'] = int(time.time()*1000)

  dumped_json = json.dumps(payload)
  encoded_json = base64.b64encode(dumped_json)
  return encoded_json

def get_signature(encoded_payload, secret_key):
  signature = hmac.new(str(secret_key).upper(), str(encoded_payload), hashlib.sha512);
  return signature.hexdigest()

def get_response(url, payload):
  encoded_payload = get_encoded_payload(payload)
  start_time = time.time()
  headers = {
    'Content-type': 'application/json',
    'X-COINONE-PAYLOAD': encoded_payload,
    'X-COINONE-SIGNATURE': get_signature(encoded_payload, API_PRI_KEY)
  }
  http = httplib2.Http()
  response, content = http.request(url, 'POST', headers=headers, body=encoded_payload)
  end_time = time.time() - start_time
  strs = 'Request Private URL %s response %s time: %0.5f' % (url,response["status"],end_time)
  mlog(strs,API)
  return content

def request_private_api(url, payload):
  content = get_response(url, payload)
  content = json.loads(content)

  return content



def request_buy_api(price,qty,currency):
    BUY_PAYLOAD = {
        "access_token": API_ACC_TOKEN,
        "price": price,
        "qty": qty,
        "currency": currency
    }
    #print BUY_PAYLOAD
    return request_private_api(BUYURL,BUY_PAYLOAD)

def request_sell_api(price,qty,currency):
    payload = {
        "access_token": API_ACC_TOKEN,
        "price": price,
        "qty": qty,
        "currency": currency
    }
    #print BUY_PAYLOAD
    return request_private_api(SELLURL,payload)

def request_comorder_api(currency):
    comorder_payload = {
        "access_token": API_ACC_TOKEN,
        "currency": currency
    }
    #print comorder_payload
    return request_private_api(COMORDER,comorder_payload)

def request_balance_api():
    payload = {
        "access_token": API_ACC_TOKEN,
    }
    #print comorder_payload
    return request_private_api(BALANCE,payload)


def request_orderinfo_api(orderid, currency):
    payload = {
        "access_token": API_ACC_TOKEN,
        "order_id": orderid,
        "currency": currency,
    }
    #print comorder_payload
    return request_private_api(ORDERINFO,payload)

def request_cancelorder_api(orderid, currency,price,qty,is_ask):
    payload = {
        "access_token": API_ACC_TOKEN,
        "order_id": orderid,
        "price": price,
        "qty": qty,
        "is_ask": is_ask,
        "currency": currency
    }
    #print comorder_payload
    content = request_private_api(CNACELORDER, payload)
    try:
        if content['result'] == 'success':
            return True
        else:
            return False
    except:
        return False

def request_cancelorder_all_api(orderid, currency):
    payload = {
        "access_token": API_ACC_TOKEN,
        "order_id": orderid,
        "currency": currency
    }
    #print comorder_payload
    content = request_private_api(CNACELORDERALL, payload)
    try:
        if content['result'] == 'success':
            return True
        else:
            return False
    except:
        return False

'''
    HTTP/1.1 200 OK
{
  "errorCode": "0",
  "result": "success",
  "successCanceledOrders": "4",
  "overallOrders": "4"
}
'''


def cancel_order_all_api(orderid, currency, real):
    if TR_WAY_REAL:
        return request_cancelorder_all_api(orderid, currency)
    elif TR_WAY_TEST:
        return {'result': 'success'}



def apiGetLastPrice(cur):
    json_dat = get_response_val_by_key(TICKER, 'GET', params=get_currency_params(cur))
    err_code = CMN_SUCCESS

    if json_dat != {} and json_dat['result'] == 'success':
        if json_dat.get('currency') == cur:
            mlog('get last data Success !', API)
            return int(json_dat['last']), err_code # 한화로 표시되니까 int 여도 될듯 ??
        err_code = CMN_ERR_INVALID_COIN
    else:
        err_code = CMN_ERR_REQ_FAIL
    return False, err_code

def request_orderinfomation_api(orderid, currency, real, order_price, order_qty):
    if real == TR_WAY_TEST:
        '''
        # TODO TEST------------- 아래 테스트 코드 제거
        result = random.randrange(1, 10)
        if result < 7:
            str_result = 'success'
        else:
            str_result = 'fail'
        # --------------------------------
        '''
        str_result = 'success'

        return {
            'result':str_result,
            #'result': 'fail',
            'status':'filled',
            'price': order_price,
            'qty': order_qty
        }

    else:
        return request_orderinfo_api(orderid, currency)




def request_order_api(price,qty,currency, way, real):

    if real == TR_WAY_TEST:
        order_id = random.randrange(1, 0xFFFFFFFF)
        order_id = 'test'+str(order_id)
        return {'result':'success','orderId':order_id }

    if way == TR_WAY_BUY:
        return request_buy_api(price,qty,currency)
    elif way == TR_WAY_SELL:
        return request_sell_api(price,qty,currency)
    else:
        return {'result':'Fail'}

if __name__ == "__main__":
    currency_params = {'currency': 'btc'}
    json_dat = get_response_val_by_key(TICKER, 'GET', params=currency_params)
    print('result' in json_dat)
